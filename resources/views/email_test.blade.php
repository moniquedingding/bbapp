<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
</head>
<body><?php $customer = $customers_invoices; ?>
<div style="font-size:15px; background: #fff; color: #2A3F54; font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;">
  <div class="wrapper" style="width:870px; margin: 0 auto;">
    <div class="header" style="margin-bottom: 50px;">
      <img src="{{ asset('/images/bblogo_resize.jpg') }}" alt="BBApp Logo" style="float:left;">
      <div style="float: right; margin-right: 110px; color: #00894A;">
        <h1 style="font-size: 20px; margin: 13px 0 3px; font-weight: 500; color: #0F432C;">{{ $app_name }}</h1>
        <?php echo nl2br($customer->address);?>
      </div>
      <br style="clear:both;">
    </div>
    <div class="start-section" style="line-height:1.3;">
      <h3 style="margin: 0 0 15px;">Invoice to:</h3>
      <div class="customer-info" style="float: left; width: 62%;">
        <strong>{{ $customer->firstname }} {{ $customer->lastname}}</strong><br>
        {{ $customer->address_1 }} {{ $customer->address_2}}<br>
        {{ $customer->city }}, {{ $customer->state}}<br>
        {{ $customer->country }}<br>
        {{ $customer->postcode}}<br>        
        <br style="clear:both;">
      </div>
      <div class="invoice-info" style="float: right; width: 38%;">
        <table class="invoice-info-tb" style="border-spacing: 0;">
            <tr><td><strong>Page:</strong></td><td style="padding-left: 45px;">1</td></tr>
            <tr><td><strong>Invoice Month:</strong></td><td style="padding-left: 45px;">{{ date('F', $customer->start) }}</td></tr>
            <tr><td><strong>Invoice Date:</strong></td><td style="padding-left: 45px;">{{ date('m/d/Y', $customer->start) }}</td></tr>
            <tr><td><strong>Invoice No:</strong></td><td style="padding-left: 45px;">{{ sprintf('%07d', $invoice_id) }}</td></tr>
        </table>
      </div><br style="clear:both;">
    </div><br style="clear:both;">
    <div class="invoice-section" style="margin-bottom: 20px; line-height:1.5;">
      <table class="invoice-details-tb" style="border-spacing: 0; border-top: 1px solid; border-bottom: 2px solid; width: 100%;">
        <thead>
            <tr>
                <th colspan="3" style="text-align: left; width: 62%;">Description</th>
                <th style="text-align: left; width: 38%;">Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4">
                    <strong>{{ ucwords($customer->title) }} for {{ ucwords($customer->name) }}</strong> <br>

                    @if ($customer->frequency == "ALL")
                        Everyday, 
                    @else
                        <?php
                            $freq = explode(",", $customer->frequency);
                            $temp = "";
                            foreach ($freq as $f) {
                                $temp .= strtoupper(substr($f, 0, 1));
                            }
                        ?> 
                        {{ $temp }}, 
                    @endif
                    {{ date("M j", $customer->start_date) }} - {{ date("M j, Y", $customer->end_date) }} <br>
                    
                    
                    
                    {{ date("h:i A", $customer->time_start) }} to {{ date("h:i A", strtotime("+".$customer->duration." minutes", $customer->time_start)) }} ({{ ucwords($customer->duration) }} mins)

                </td>
            </tr>

            <?php

                $frequency = $customer->frequency;

                if ($frequency == "ALL") {
                    $frequency = "monday,tuesday,wednesday,thursday,friday,saturday,sunday,";
                }

                $frequency = explode(",", $frequency);
                $frequency = array_filter($frequency);

                $start = $customer->start_date;
                $start = strtotime("+ 1 day", $start);

                $end = $customer->end_date;

                $week = 0;
                
                while($start <= $end) {
                    $week++;
                    $first = "yes";

                    $sunday = strtotime("this sunday", $start);

                    echo "<tr>";
                    echo "<td style='color: white'>Week ".$week."</td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "</tr>";


                    //GET NUMBER OF DAYS ENROLLED IN A WEEK
                    $temp_start = $start;

                    $count = 0;
                    while(($temp_start <= $sunday) && ($start <= $end)) {
                        $day = strtolower(date('l', $temp_start));
                        $res = array_search($day, $frequency);

                        if (is_numeric($res)) {
                            $count++;
                        }

                        $temp_start = strtotime("+1 day", $temp_start);
                    }

                    while(($start <= $sunday) && ($start <= $end)) {

                        $day = strtolower(date('l', $start));
                        $res = array_search($day, $frequency);

                        if (is_numeric($res)) {
                            echo "<tr>";
                            echo "<td style='width: 17%;'>".date('l', $start)."</td>";
                            echo "<td style='width: 15%'>".date('jS', $start)."</td>";
                            echo "<td>".date('F', $start)."</td>";

                            if ($customer->is_custom_price == "yes") {

                                foreach($centres as $cen) {
                                    if($cen->id == $customer->centre_id) {
                                        echo "<td>£ ".number_format($cen->custom_price, 2, '.', '')."</td>";
                                    }
                                }
                            } else {
                                foreach($centres as $cen) {
                                    if($cen->id == $customer->centre_id) {

                                        if ($cen->name == "Hythe") {
                                            if ($count == 1) {
                                                echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                            } else {
                                                echo "<td>£ ".number_format($cen->discounted_price, 2, '.', '')."</td>";
                                            }
                                        } else {

                                            if ($cen->name == "Sevenoaks") {
                                                if ($count == 1) {
                                                    echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                } else {
                                                    if ($first == "yes") {
                                                        echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                        $first = "no";
                                                    } else {
                                                        echo "<td>£ ".number_format($cen->discounted_price, 2, '.', '')."</td>";

                                                    }
                                                }
                                            } else {
                                                if ($count == 1) {
                                                    echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                } else {
                                                    if ($first == "yes") {
                                                        echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                        $first = "no";
                                                    } else {
                                                        if ($customer->is_custom_price == "yes") {
                                                            echo "<td>£ ".number_format($cen->custom_price, 2, '.', '')."</td>";
                                                        } else {
                                                            echo "<td>£ ".number_format($cen->discounted_price, 2, '.', '')."</td>";
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                            echo "</tr>";
                        }

                        $start = strtotime("+1 day", $start);


                    }
                } 
            ?>
            <tr><td colspan="4">&nbsp;</td></tr>
            <tr>
                <td style="border-top: 1px solid;" colspan="3"><strong>Invoice Total:</strong></td>
                <td style="border-top: 1px solid;"><strong>&pound; {{ number_format($customer->amount_due, 2, '.', '') }}</strong></td>
            </tr>
            
        </tbody>
      </table>
    </div>
    <div>
      <?php echo $customer->email_footnote; ?>
    </div>
  </div>
</div>
</body>
</html>