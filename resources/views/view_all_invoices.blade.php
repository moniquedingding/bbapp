@extends('admin.admin_layout')
@section('title', $title)

@section('additional_head')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
 <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
 <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    @if (isset($is_all_invoice) && $is_all_invoice == TRUE)
                    <h2>Paid Invoices List</h2>
                    @elseif ($title == "Past Invoices")
                    <h2>Invoice History</h2>
                    @elseif ($title == "Future Invoices")
                    <h2>Future Invoices</h2>
                    @elseif ($title == "Unpaid & Partial Invoices")
                    <h2>Unpaid, Partial and Overdue Invoices</h2>
                    @else
                    <h2>Invoices as of {{ date('M Y') }}</h2>
                    @endif

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    @if (!isset($is_all_invoice_customer))
                        <ul class="nav nav-tabs bar_tabs">
                            <li role="presentation"<?php if($is_active_link == '1') echo 'class="active" '; ?> > <a href="/invoice">Current Invoices</a></li>
                            <li role="presentation"<?php if($is_active_link == '2') echo 'class="active" '; ?> > <a href="/invoice/view-all">View Paid Invoices</a></li>
                            <li role="presentation"<?php if($is_active_link == '3') echo 'class="active" '; ?> > <a href="/invoice/view-unpaid">View Unpaid Invoices</a></li>
                            <li role="presentation"<?php if($is_active_link == '4') echo 'class="active" '; ?> > <a href="/invoice/view-past">Past Invoices</a></li>
                            <li role="presentation"<?php if($is_active_link == '5') echo 'class="active" '; ?> > <a href="/invoice/view-future">Future Invoices</a></li>
                        </ul>
                    @else
                    <a href="/invoice/" class="btn btn-primary">Go back to invoices list</a>
                    @endif
                    <!-- <a href="/invoice/add" class="btn btn-warning"><i class="fa fa-plus"></i> Add Manual Invoice</a> -->
                    @if (isset($message))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif
                    <table id="example" class="table table-striped responsive-utilities jambo_table table-highlight-links" style="font-size: 11px;">
                        <thead>
                            <tr class="headings">
                                <th>Invoice # </th>
                                <th>Date Issued</th>
                                <th>Date Due</th>
                                <th>Date Sent</th>
                                <th>Customer</th>
                                <!-- <th>Payment for Class</th> -->
                                <th>Centre</th>
                                <!-- <th># of Days</th> -->
                                <th>Amount Due</th>
                                <th>Amount Paid</th>
                                <th>Payment</th>
                                <th>Status</th>
                                <th>Date Paid</th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                $due_sum = 0;
                                $paid_sum = 0;
                            ?>
                            @foreach ($customers_invoices as $row)
                            
                                @if ($count % 2 == 0)
                                    <tr class="even pointer">
                                @else
                                    <tr class="odd pointer">
                                @endif

                                    <td><a href="/invoice/view/{{ $row->id }}">{{ sprintf( '%07d', $row->id) }}</a></td>
                                    <td style="white-space:nowrap;">{{ date("M j, Y", $row->start) }}</td>
                                    <?php 
                                        $near = strtotime("- 5 days", $row->due_date);
                                    ?>
                                    @if (($row->amount_due != "0.00") && ($row->status == "new") && (time() >= $near) && (time() <= $row->due_date))
                                    <td style="white-space:nowrap; color: #F0AD4E">
                                    @elseif ($row->due_date <= time())
                                    <td style="white-space:nowrap; color: red">
                                    @else 
                                    <td style="white-space:nowrap;">
                                    @endif
                                        {{ date("M j, Y", $row->due_date) }}</td>

                                    @if ($row->sent_date == "0")
                                    <td></td>
                                    @else
                                    <td style="white-space:nowrap;" >{{ date("M j, Y", $row->sent_date) }}</td>
                                    @endif
 

                                    @foreach($customers as $c) 
                                        @if ($row->customer_id == $c->id)
                                            <td>
                                                <a href="/invoice/view-customer/{{ $row->customer_id }}">{{ ucfirst($c->firstname)." ".ucfirst($c->lastname) }}</a>
                                                <br> {{ $row->name }}
                                            </td>
                                            <!-- <td>{{ $row->title }}</td> -->

                                            @foreach($centres as $ce) 
                                                @if ($c->centre_id == $ce->id)
                                                    <td>{{ ucfirst($ce->name) }}</td>
                                                @endif
                                            @endforeach

                                        @endif
                                    @endforeach
                                    
                                    <?php
                                        $due_sum += $row->amount_due;
                                        $paid_sum += $row->amount_paid;
                                    ?>
                                    <!-- <td>{{ $row->days_enrolled }}</td> -->
                                    <td>£ {{ number_format($row->amount_due, 2, '.', '') }}</td>
                                    <td>£ {{ number_format($row->amount_paid, 2, '.', '') }}</td>
                                    <td>{{ ucfirst($row->payment_method) }}</td>
                                        @if ($row->status == "paid")
                                        <td style="color: #26B99A">
                                        @elseif ($row->status == "partial")
                                        <td style="color: #F0AD4E">
                                        @else 
                                        <td style="color: #1A82C3">
                                        @endif
                                            {{ strtolower($row->status) }}
                                    </td>
                                    <td style="white-space: nowrap;">                                       
                                        <?php
                                            if ($row->status == "paid")
                                                echo date("M j, Y", $row->paid_date);
                                            else
                                                echo "";
                                        ?>
                                    </td>
                                    <td style="white-space: nowrap;"><a href="/invoice/view/{{ $row->id }}">View</a> <br>
                                        <!-- <a href="">Update</a> <br>  -->
                                        <span><a href="/invoice/resend-email/{{ $row->id }}" class="resend">Resend e-mail</a></span> <br> 
                                        @if ($row->status == "new" || $row->status == "partial")
                                        <a href="/receipt/add-payment/{{ $row->id }}">Add payment</a> <br>
                                        @else
                                        <p class="disabled">Paid in full</p>
                                        @endif
                                    </td>
                                </tr>
                                
                                <?php $count++; ?>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12">
                <hr>
                    <div class="pull-right">
                         <h4>
                            Total amount due: <strong>£ {{ number_format($due_sum, 2, '.', '') }}</strong> &nbsp;&nbsp;|&nbsp;&nbsp;
                            Total amount paid: <strong>£ {{ number_format($paid_sum, 2, '.', '') }}</strong>

                         </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('additional_script')

    <script type="text/javascript">
        /*$(".description").click(function() {
            var desc = $('.description').attr('data-desc'); 
            var id = $('.description').attr('data-id'); 

            $(".desc-td"+id).html(desc);
        });*/   

        $(document).ready(function () {
                $('.resend').on('click', function(){
                    $(this).parent().html("<span>Sending...</span>");
                })
            });
         $(document).ready(function() {
              // Configure/customize these variables.
              var showChar = 60;  // How many characters are shown by default
              var ellipsestext = "...";
              var moretext = "More <i class='fa fa-angle-double-right'></i>";
              var lesstext = "<i class='fa fa-angle-double-left'></i> Less";
            

            $('.class-description').each(function() {
                var content = $(this).html();
         
                if(content.length > showChar) {
         
                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);
         
                    var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
         
                    $(this).html(html);
                }
         
            });
         
            $(".morelink").click(function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>

    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
        ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "{{ asset('js/datatables/tools/swf/copy_csv_xls_pdf.swf') }}"
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop