<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $for_all_view['app_name'] }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('fonts/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        <div id="wrapper">
            <div class="text-center">
                <img src="{{ asset('images/bestbehaviourLOGO.JPG') }}" border="0" title="BBApp">
            </div>
            <div class="clearfix"></div>
            <div id="login" class="animate form">
                <section class="login_content">
                    <form action="/login/process" enctype="multipart/form-data" method="POST">
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <h1>Sign In</h1>
                        <div class="login_body">
                            @if (isset($message))
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                {{ $message }}
                            </div>
                            @endif
                            <div>
                                <input name="username" type="text" class="form-control" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input name="password" type="password" class="form-control" placeholder="Password" required="" />
                            </div>
                        </div>
                        <div class="login_footer">
                            <div>
                                <button type="submit" class="btn btn-default submit login-btn">Log in</button>
                                <a class="reset_pass" href="#"><i class="fa fa-question-circle fa-lg"></i> Forgot password</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

 <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>

</body>

</html>