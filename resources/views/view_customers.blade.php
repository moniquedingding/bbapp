@extends('admin.admin_layout')
@section('title', $title)

@section('additional_head')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
 <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
 <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Active Customers</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <a href="/customers/add" class="btn btn-success"><i class="fa fa-plus"></i> Add a Customer</a>
                    <a href="/customers/view-all" class="btn btn-primary"><i class="fa fa-users"></i> View All Customers</a>
                    @if (isset($message))
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                            <br><a href="/customers"><strong>Go back to customers list</strong></a>
                        </div>
                    @endif

                    <table id="example" class="table table-striped responsive-utilities jambo_table table-highlight-links" style="font-size: 11px;">
                        <thead>
                            <tr class="headings">
                                <th># </th>
                                <th>Name </th>
                                <th>Centre </th>
                                <th>Address </th>
                                <th>Phone </th>
                                <th>Mobile </th>
                                <th>Email </th>
                                <th>Dogs</th>
                                <th class=" no-link last" style="white-space: nowrap;"><span class="nobr">Action</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

    </div>
</div>
@stop

@section('additional_script')
        <!-- Datatables -->
        <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
        <script>
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "processing": true,
                    "serverSide": true,


                    "ajax": {
                        url :"/customers/ajax-view",
                        type: "post",
                        data: {
                            _token: "{{ csrf_token() }}"
                        }

                    },



                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "{{ asset('js/datatables/tools/swf/copy_csv_xls_pdf.swf') }}"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>
@stop