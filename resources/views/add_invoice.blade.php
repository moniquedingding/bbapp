@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Invoice Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    <form id="invoice-form" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Invoice # 
                                    </label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input id="invoice-number" class="form-control" type="text" disabled value="0001">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Customer Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <select class="form-control" name="customer_id">
                                            <option value="">John Doe</option>
                                            <option value="">Sam Smith</option>
                                            <option value="">Anne Mendoza</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Created Date <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input id="created-date" class="date-picker form-control" required="required" type="text" value="{{ date('m/d/Y') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Due Date <span class="required">*</span>
                                    </label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                        <input id="due-date" class="date-picker form-control" required="required" type="text" value="{{ date('m/d/Y') }}">
                                    </div>
                                </div>
                            </div>
                        </div><!--/.row-->
                        <div class="row">
                            <div class="col-md-9 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Date Range <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-9 col-xs-11">
                                        <input id="date-range" class="form-control" type="text" value="">
                                    </div>
                                    <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="These are the Classes Dates included in this invoice."></i>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <a href="" class="btn btn-default"><i class="fa fa-refresh"></i> Generate Invoice Items</a>
                            </div>
                        </div>
                        <br>
                        <span class="section">Invoice Items</span>
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12 table">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Dog Name</th>
                                            <th>Class Description</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Barky</td>
                                            <td>Barking Class - 12pm-1pm - 1hr.</td>
                                            <td>08/01/2015</td>
                                            <td>£ 20.00</td>
                                        </tr>
                                        <tr>
                                            <td>Barky</td>
                                            <td>Something - 9am-10am - 1hr.</td>
                                            <td>8/01/2015</td>
                                            <td>£ 15.00</td>
                                        </tr>
                                        <tr>
                                            <td>Slater</td>
                                            <td>Something - 9am-10am - 1hr.</td>
                                            <td>08/01/2015</td>
                                            <td>£ 20.00</td>
                                        </tr>
                                        <tr>
                                            <td>Slater</td>
                                            <td>Barking Class - 12pm-1pm - 1hr</td>
                                            <td>08/01/2015</td>
                                            <td>£ 15.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-xs-6">
                                <p class="lead">Payment Methods:</p>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="flat"> Paypal
                                    </label>

                                    <label>
                                        <input type="checkbox" class="flat"> Cheque
                                    </label>
                                    <label>
                                        <input type="checkbox" class="flat"> Bank Transfer
                                    </label>                                    
                                    <label>
                                        <input type="checkbox" class="flat"> Cash
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th style="width:50%">Subtotal:</th>
                                                <td>£ 100.00</td>
                                            </tr>
                                            <tr>
                                                <th>Tax (10%)</th>
                                                <td>£ 10.00</td>
                                            </tr>
                                            <tr>
                                                <th>Total:</th>
                                                <td>£ 110.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Cancel</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                    </form><!--/#invoice-form-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#invoice-form .btn').on('click', function () {
        $('#invoice-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#invoice-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.date-picker').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#date-range').daterangepicker({
        calender_style: "picker_2",
        "startDate": moment().startOf('month'),
        "endDate": moment().endOf('month')
    });
}); 
</script>
@stop