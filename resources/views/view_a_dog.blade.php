
@extends('admin.admin_layout')
@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Dog Information <small><a href="/dogs">Go back to dogs list</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><strong>Image</strong></td>
                                @if(empty($dog['image']))
                                <td><img src="{{ asset('/images/pets/no-image.jpg') }}" alt="" class="img-responsive" width="100px"></td>
                                @else
                                <td><img src="{{ asset('/images/pets/'.$dog['image']) }}" alt="" class="img-responsive" width="100px"></td>
                                @endif
                            </tr>
                            <tr>
                                <td><strong>Owner</strong></td>
                                @foreach ($customers as $c)
                                    @if ($c->id == $dog['customer_id'])
                                        <td class=" ">{{ ucfirst($c->firstname)." ".ucfirst($c->lastname) }} <a href="/customers/view/{{ $c->id }}"><span class="label label-info">View Profile</span> </a></td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{{ ucwords($dog['name']) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Age</strong></td>
                                <td>{{ $dog['age'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Breed</strong></td>
                                <td>{{ ucwords($dog['breed']) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Color</strong></td>
                                <td>{{ ucwords($dog['color']) }}</td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

    </div>
</div>
@stop