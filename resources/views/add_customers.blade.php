@extends('admin.admin_layout')

@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Customer Information <small><a href="/customers">Go back to customers list</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if (empty($id))
                    <form class="form-horizontal form-label-left" method="POST" action="/customers/add" novalidate>
                    @else
                    <form class="form-horizontal form-label-left" method="POST" action="/customers/edit/{{ $id }}" novalidate>
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                        @if (isset($message))
                            <div class="alert alert-success" role="alert">
                                {{ $message }}
                                <br><a href="/customers"><strong>Go back to customers list</strong></a>
                            </div>
                        @endif

                        <p>Please fill out the necessary details marked with an asterisk (*) below.</p>                

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="1" name="firstname" required="required" type="text" value="{{ $customer['firstname'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Middle Name
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="2" name="middlename" type="text" value="{{ $customer['middlename'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="1" name="lastname" required="required" type="text" value="{{ $customer['lastname'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Address 1 <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="textarea" name="address_1" required="required" class="form-control col-md-7 col-xs-12">{{ $customer['address_1'] }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Address 2
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="textarea" name="address_2" class="form-control col-md-7 col-xs-12">{{ $customer['address_2'] }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">City/Town <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="email" name="city" required="required" class="form-control col-md-7 col-xs-12" value="{{ $customer['city'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">State
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="email" name="state" class="form-control col-md-7 col-xs-12" value="{{ $customer['state'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Country <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="email" name="country" required="required" class="form-control col-md-7 col-xs-12" value="{{ $customer['country'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Post Code <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="email" name="postcode" required="required" class="form-control col-md-7 col-xs-12" value="{{ $customer['postcode'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $customer['email'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Telephone Number
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="number" name="phone" class="form-control col-md-7 col-xs-12" value="{{ $customer['phone'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Mobile Number <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="number" name="mobile" required="required" class="form-control col-md-7 col-xs-12" value="{{ $customer['mobile'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Fax Number
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="number" name="fax" class="form-control col-md-7 col-xs-12" value="{{ $customer['fax'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Occupation
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="occupation" type="text" name="occupation" class="form-control col-md-7 col-xs-12" value="{{ $customer['occupation'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Additional Notes
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="textarea" name="notes" class="form-control col-md-7 col-xs-12">{{ $customer['notes'] }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Discounted Price?
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="radio" name="is_patron" value="yes" <?php if($customer['is_patron'] == "yes") echo "checked"; ?>> Yes &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="is_patron" value="no" <?php if($customer['is_patron'] == "no") echo "checked"; ?>> No
                            </div>
                        </div>
                        <br>
                        <span class="section">Other Information</span>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Centre <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="centre_id" name="centre_id" class="form-control col-md-7 col-xs-12">
                                    @foreach($centres as $row)
                                        <option value="{{ $row->id }}" <?php if($customer['centre_id'] == $row->id) echo "selected"; ?>>{{ ucfirst($row->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="item form-group" id="manager_incharge_div">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Manager in-charge 
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="manager_id" name="manager_id" class="form-control col-md-7 col-xs-12">
                                    @foreach($manager as $row)
                                        <option value="{{ $row->id }}" data-centre-id="{{ $row->centre_id }}" <?php if($customer['manager_id'] == $row->id) echo "selected"; ?> style="display:none;">{{ ucfirst($row->firstname)." ".ucfirst($row->lastname)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="ln_solid"></div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a href="/customers" class="btn btn-primary">Cancel</a>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script src="{{ asset('js/validator/validator.js') }}"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);


//On document.ready of Centre_id
var cSel = $('#centre_id').val();
$("#manager_id > option").each(function() {
    var dci = $(this).attr('data-centre-id');
    if(cSel == dci ){
        $(this).prop('selected', true);
        $(this).show();
    } else {
        $(this).hide();
    }
console.log(dci);
});


//on changes of centre_id
$('#centre_id').on('change', function(){
    var cSel = $('#centre_id').val();

    $("#manager_id > option").each(function() {
        var dci = $(this).attr('data-centre-id');
        if(cSel == dci ){
            $(this).prop('selected', true);
            $(this).show();
        } else {
            $(this).hide();
        }
    });
});

</script>
@stop