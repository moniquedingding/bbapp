@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Invoice Details - #{{ sprintf('%07d', $invoice_id) }} <small> <a href="/invoice">Go back to invoice list</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    <form id="invoice-form" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="row">
                            <div class="col-md-6">
                                <style type="text/css">
                                .strong-title {
                                    font-weight: bold;
                                }
                                .invitems-date {
                                    display: inline-block;
                                }
                                .invitems-word-day, .invitems-month { width: 150px; }
                                .invitems-word-num { width: 100px;}
                                </style>

                                <?php
                                    $customer = $customers_invoices[0];
                                ?>

                                <table class="table table-bordered">

                                    <tr>
                                        <td width="30%" class="strong-title">Customer Name</td>
                                        <td>{{ ucfirst($customer->firstname)." ".ucfirst($customer->lastname) }}</td>
                                    </tr> 

                                    <tr>
                                        <td width="30%" class="strong-title">Dog Enrolled</td>
                                        <td>{{ ucwords($customer->name) }}</td>
                                    </tr>

                                    <tr>
                                        <td width="30%" class="strong-title">Centre</td>
                                        <td> 
                                            @if ($customer->centre_id == 1)
                                                Hythe
                                            @elseif ($customer->centre_id == 2)
                                                Sevenoaks
                                            @else
                                                Tonbridge
                                            @endif
                                            <br>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td width="30%" class="strong-title">Address</td>
                                        <td>
                                            {{ ucfirst($customer->address_1) }} <br>
                                            
                                            @if (!empty($customer->address_2))
                                                {{ ucfirst($customer->address_1) }} <br>
                                            @endif

                                            {{ ucfirst($customer->city) }}, {{ ucfirst($customer->state) }}<br>
                                            {{ ucfirst($customer->country) }}<br>
                                            {{ ucfirst($customer->postcode) }}

                                        </td>
                                    </tr>  
                                </table>

                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered">

                                    <tr>
                                        <td width="30%" class="strong-title">Invoice #</td>
                                        <td>{{ sprintf('%07d', $invoice_id) }}</td>
                                    </tr>

                                    <tr>
                                        <td width="30%" class="strong-title">Invoice Date</td>
                                        <td>{{ date("M j, Y", $invoice_date) }}</td>
                                    </tr>


                                    <tr>
                                        <td width="30%" class="strong-title">Invoice Details</td>
                                        <td>Issued invoice for {{ date("M d", $invoice_start) }} to {{ date("M d, Y", $invoice_end) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="strong-title">Payment Status</td>
                                        @if ($customer->status == "new")
                                        <td>
                                        @else
                                        <td style="color: #26B99A">
                                        @endif
                                            <strong>{{ $customer->status }}</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div><!--/.row-->
                       <hr>
                        <div class="row">
                            <div class="col-md-12 table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th> </th>
                                            <th>Schedule</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <strong>{{ ucwords($customer->title) }}</strong> <br>
                                                <p>{{ ucwords($customer->description) }}</p>
                                                
                                                
                                            </td>
                                        
                                            <td>
                                            @if ($customer->frequency == "ALL")
                                                    Everyday, 
                                                @else
                                                    <?php
                                                        $freq = explode(",", $customer->frequency);
                                                        $temp = "";
                                                        foreach ($freq as $f) {
                                                            $temp .= ucfirst(substr($f, 0, 3));
                                                        }
                                                    ?> 
                                                    {{ $temp }}<br>
                                                @endif

                                                {{ date("M j", $customer->start_date) }} - {{ date("M j, Y", $end_sched) }} <br>
                                                
                                                
                                                
                                                {{ date("h:i A", $customer->time_start) }} to {{ date("h:i A", strtotime("+".$customer->duration." minutes", $customer->time_start)) }} ({{ ucwords($customer->duration) }} mins)
                                                <!-- {{ $end_sched }} -->
                                            </td>
                                        </tr>

                                        <?php

                                            $frequency = $customers_invoices[0]->frequency;

                                            if ($frequency == "ALL") {
                                                $frequency = "monday,tuesday,wednesday,thursday,friday,saturday,sunday,";
                                            }

                                            $frequency = explode(",", $frequency);
                                            $frequency = array_filter($frequency);

                                            $start = $customers_invoices[0]->start;

                                            $end = $customers_invoices[0]->end;

                                            // echo date('Y-m-d', $start);
                                            // echo date('Y-m-d', $end);

                                            $week = 0;

                                            // if ($customers_invoices[0]->start > )

                                            while($start <= $end) {
                                                $week++;
                                                $first = "yes";

                                                $sunday = strtotime("this sunday", $start);

                                                if ($sunday > $end) {
                                                    $sunday = $end;
                                                }

                                                echo "<tr>";
                                                echo "<td style='color: white'> ".$week."</td>";
                                                echo "<td></td>";
                                                echo "<td></td>";
                                                echo "<td></td>";
                                                echo "</tr>";


                                                //GET NUMBER OF DAYS ENROLLED IN A WEEK
                                                $temp_start = $start;

                                                $count = 0;
                                                while(($temp_start <= $sunday) && ($temp_start <= $end)) {
                                                    $day = strtolower(date('l', $temp_start));
                                                    $res = array_search($day, $frequency);

                                                    if (is_numeric($res) && ($temp_start <= $end)) {
                                                        ++$count;
                                                    }

                                                    $temp_start = strtotime("+1 day", $temp_start);
                                                }

                                                while(($start <= $sunday) && ($start <= $end)) {
                                                    $day = strtolower(date('l', $start));
                                                    $res = array_search($day, $frequency);

                                                    if (is_numeric($res)) {

                                                        echo "<tr>";

                                                        if (!empty($days_absent)) {
                                                            foreach($days_absent as $dc) {
                                                                if ($dc == date('M d, Y', $start)) {
                                                                    echo "<td style='width: 20%; color: #D9534F;'>".date('l', $start)."</td>";
                                                                    echo "<td style='width: 18%; color: #D9534F;'>".date('jS', $start)."</td>";
                                                                    echo "<td style='width: 40%; color: #D9534F;'>".date('F', $start)."</td>";
                                                                } else {

                                                                    if (!empty($days_cancelled)) {

                                                                        foreach($days_cancelled as $da) {
                                                                            if ($da != date('M d, Y', $start)) {
                                                                                echo "<td style='width: 20%'>".date('l', $start)."</td>";
                                                                                echo "<td style='width: 18%'>".date('jS', $start)."</td>";
                                                                                echo "<td style='width: 40%'>".date('F', $start)."</td>";
                                                                            }
                                                                        }

                                                                    } else {
                                                                        echo "<td style='width: 20%'>".date('l', $start)."</td>";
                                                                        echo "<td style='width: 18%'>".date('jS', $start)."</td>";
                                                                        echo "<td style='width: 40%'>".date('F', $start)."</td>";

                                                                    }
                                                                    
                                                                }
                                                            }   
                                                        } else {
                                                            echo "<td style='width: 20%'>".date('l', $start)."</td>";
                                                            echo "<td style='width: 18%'>".date('jS', $start)."</td>";
                                                            echo "<td style='width: 40%'>".date('F', $start)."</td>";
                                                        }
                                                        

                                                        if ($customers_invoices[0]->is_custom_price == "yes") {

                                                            foreach($centres as $cen) {
                                                                if($cen->id == $customers_invoices[0]->centre_id) {
                                                                    echo "<td>£ ".number_format($cen->custom_price, 2, '.', '')."</td>";
                                                                }
                                                            }
                                                        } else {
                                                            foreach($centres as $cen) {
                                                                if($cen->id == $customers_invoices[0]->centre_id) {

                                                                    if ($cen->name == "Hythe") {
                                                                        if ($count == 1) {
                                                                            echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                                        } else {
                                                                            echo "<td>£ ".number_format($cen->discounted_price, 2, '.', '')."</td>";
                                                                        }
                                                                    } else {

                                                                        if ($cen->name == "Sevenoaks") {
                                                                            if ($count == 1) {
                                                                                echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                                            } else {
                                                                                if ($first == "yes") {
                                                                                    echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                                                    $first = "no";
                                                                                } else {
                                                                                    echo "<td>£ ".number_format($cen->discounted_price, 2, '.', '')."</td>";

                                                                                }
                                                                            }
                                                                        } else {
                                                                            if ($count == 1) {
                                                                                echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                                            } else {
                                                                                if ($first == "yes") {
                                                                                    echo "<td>£ ".number_format($cen->price, 2, '.', '')."</td>";
                                                                                    $first = "no";
                                                                                } else {
                                                                                    if ($customers_invoices[0]->is_custom_price == "yes") {
                                                                                        echo "<td>£ ".number_format($cen->custom_price, 2, '.', '')."</td>";
                                                                                    } else {
                                                                                        echo "<td>£ ".number_format($cen->discounted_price, 2, '.', '')."</td>";
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        }

                                                        echo "</tr>";
                                                    }

                                                    $start = strtotime("+1 day", $start);


                                                }
                                            }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-xs-6">
                                <p class="lead">Payment Methods:</p>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="flat" <?php if ($customer->payment_method == "paypal") echo "checked "; else echo " disabled"; ?>> Paypal
                                    </label>

                                    <label>
                                        <input type="checkbox" class="flat" <?php if ($customer->payment_method == "cheque") echo "checked "; else echo " disabled"; ?>> Cheque
                                    </label>
                                    <label>
                                        <input type="checkbox" class="flat" <?php if ($customer->payment_method == "bank transfer") echo "checked "; else echo " disabled"; ?>> Bank Transfer
                                    </label>                                    
                                    <label>
                                        <input type="checkbox" class="flat" <?php if ($customer->payment_method == "cash") echo "checked "; else echo " disabled"; ?>> Cash
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>Invoice Total:</th>
                                                <td>£ {{ number_format($customer->amount_due, 2, '.', '') }}</td>
                                            </tr>
                                            <tr>
                                                <th>Amount Paid:</th>
                                                <td>£ {{ number_format($customer->amount_paid, 2, '.', '') }}</td>
                                            </tr>
                                            <tr>
                                                <th>Remaining Balance:</th>
                                                <td>£ {{ number_format($customer->amount_rem, 2, '.', '') }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
<!-- 
                        <div class="clearfix"></div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Cancel</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div> -->
                    </form><!--/#invoice-form-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#invoice-form .btn').on('click', function () {
        $('#invoice-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#invoice-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.date-picker').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#date-range').daterangepicker({
        calender_style: "picker_2",
        "startDate": moment().startOf('month'),
        "endDate": moment().endOf('month')
    });
}); 
</script>
@stop