@extends('admin.admin_layout')

@section('title', 'Calendar')

@section('additional_head')
 <link href="{{ asset('css/calendar/fullcalendar.css') }}" rel="stylesheet">
 <link href="{{ asset('css/calendar/fullcalendar.print.css') }}" rel="stylesheet" media="print">
@stop


@section('content')
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Class Schedule</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Calendar</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        @if (isset($message))
                                            @if (isset($is_delete)) 
                                                <div class="alert alert-danger" role="alert">
                                            @else
                                                <div class="alert alert-success" role="alert">
                                            @endif

                                                {{ $message }}
                                                <br><a href="/"><strong>Go back to home</strong></a>
                                            </div>
                                        @endif
                                        <div class="col-sm-6">
                                            <p> <strong>To create a schedule, click on any of the grey spaces after the current date.</strong></p>
                                        </div>
                                        <div class="color-legend col-sm-6 hidden-xs">
                                            <div class="pull-right">
                                                <label>Color Legend:</label>
                                                <p><i class="fa fa-square" style="color:#D54F18;"></i> Hythe</p>
                                                <p><i class="fa fa-square" style="color:#1ABB9C;"></i> Sevenoaks</p>
                                                <p><i class="fa fa-square" style="color:#3498DB;"></i> Tonbridge</p>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div id='calendar'></div>
                                        <style>
                                            #calendar .fc-time-grid-container{
                                                height:auto !important;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>

            <!-- Start Calendar modal -->
            <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <form class="form-horizontal calender" method="POST" action="/classes/add" role="form">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myModalLabel">New Class</h4>
                            </div>
                            <div class="modal-body">
                                <div class="error-content">
                                </div>
                                <div id="testmodal" class="form-content" style="padding: 5px 20px;">
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="form-control col-md-7 col-xs-12 start-date" data-validate-length-range="1" required="required" name="start_date" type="text" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Time <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="start-time form-control col-md-7 col-xs-12 start-date" data-validate-length-range="1" required="required" name="time_start" type="text" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Duration <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <select name="duration" class="form-control">
                                                    <option value="30">30 mins</option>
                                                    <option value="60">1 hr </option>
                                                    <option value="90">1 hr 30 mins</option>
                                                    <option value="120">2 hrs</option>
                                                    <option value="150">2 hrs 30 mins</option>
                                                    <option value="180">3 hrs</option>
                                                    <option value="210">3 hrs 30 mins</option>
                                                    <option value="240">4 hrs</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Repeat
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <table class="table" style="margin-bottom: 0px;">
                                                    <tr>
                                                        <td><input type="radio" class="is-repeating" name="is_repeating" value="certain_date"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Until Specified </td>
                                                        <td colspan="2"><input type="radio" class="is-forever" name="is_repeating" checked value="forever" <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Forever </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group certain-date"  style="display: none;">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dateend">End Date
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="dateend" class="date-picker form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="2" name="end_date" type="text" value="">
                                                <label class="error-msg" style="color: #D9534F"></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Frequency 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <table class="table" style="margin-bottom: 0px;">
                                                    <tr>
                                                        <td colspan="2"><input class="select-days" type="radio" name="frequency" value="select"> Select days</td>
                                                        <td><input class="everyday" type="radio" name="frequency" value="ALL" checked> Everyday</td>
                                                    </tr>
                                                    <tr class="freq1">
                                                        <td><input type="checkbox" name="frequency[]" value="monday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Mon</td>
                                                        <td><input type="checkbox" name="frequency[]" value="tuesday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Tues</td>
                                                        <td><input type="checkbox" name="frequency[]" value="wednesday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Wed</td>
                                                    </tr>
                                                    <tr class="freq2">
                                                        <td><input type="checkbox" name="frequency[]" value="thursday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Thurs</td>
                                                        <td><input type="checkbox" name="frequency[]" value="friday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Fri</td>
                                                        <td><input type="checkbox" name="frequency[]" value="saturday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Sat</td>
                                                    </tr>
                                                    <tr class="freq3">
                                                        <td><input type="checkbox" name="frequency[]" value="sunday"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Sun</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group" >
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Centre <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                @foreach($centres as $row)
                                                    <input type="radio" name="centre_id[]" value="{{ $row->id }}" checked <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> {{ ucfirst($row->name) }} &nbsp;&nbsp;
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="1" name="title" required="required" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea id="name" class="form-control col-md-7 col-xs-12" required="required" name="description"></textarea>
                                            </div>
                                        </div>

                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary save-changes" name="save" style="margin-top: -7px;" value="Save Changes">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel2">View Schedule</h4>
                        </div>
                        <div class="modal-body">

                            <div id="testmodal2" style="padding: 5px 20px;">
                                <p class="delete-msg">
                                    Would you like to <strong>delete the selected class only</strong>, <strong>the entire class</strong> or <strong>all the future schedules of this class</strong>?
                                </p>
                                <center>
                                <div class="row delete-msg" aria-label="...">
                                    <a href="#" class="btn btn-danger sel">Selected class only</a>
                                    <a href="#" class="btn btn-danger ent">Entire class</a>
                                    <a href="#" class="btn btn-danger fut">Future schedules</a>
                                </div>
                                </center>

                                <table class="table sched-deets">
                                    <tr>
                                        <td style="white-space: nowrap"><strong>Class Name</strong></td>
                                        <td id="title"></td>
                                    </tr>

                                    <tr>
                                        <td style="white-space: nowrap"><strong>Description</strong></td>
                                        <td id="descr"></td>
                                    </tr>

                                    <tr>
                                        <td style="white-space: nowrap"><strong>Schedule</strong></td>
                                        <td id="sched"></td>
                                    </tr>

                                    <tr>
                                        <td style="white-space: nowrap"><strong>Dogs Enrolled</strong></td>
                                        <td id="dogs_enrolled" data-selectdate=""></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger pull-left delete-class" data-id="" data-time="" data-selectdate="">Delete this class</button>
                            <button type="button" id="myClose" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
            <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>


            <!-- Error Delete Class Modal Dialog -->
            <div class="modal" id="modalErrorDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: #F56954;"><i class="fa fa-exclamation-triangle"></i> Warning</h4>
                  </div>
                  <div class="modal-body">
                    <p style="font-weight:600;">You cannot delete schedules of invoices that are already paid.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <button id="modalErrorDelete-button" data-toggle="modal" data-target="#modalErrorDelete" style="display:none;"></button>


@stop

@section('additional_script')
    <!-- form validation -->
    <script src="{{ asset('js/validator/validator.js') }}"></script>

    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/calendar/fullcalendar.min.js') }}"></script>

    <script type="text/javascript">
        //Modal to not delete payed class
        var show = "<?php echo $error_delete_modal; ?>";
        if((show != "")){
            $('#modalErrorDelete-button').click();
        }


        $('.freq1').hide();
        $('.freq2').hide();
        $('.freq3').hide();
        $('.delete-msg').hide();

        $("body").on("click", ".delete-class", function(e){
            $('.modal-title').html("Delete Class");
            $('.delete-msg').show();
            $('.delete-class').hide();
            $('.sched-deets').hide();

            var id = $('.delete-class').attr('data-id');
            var timestamp = $('.delete-class').attr('data-selectdate');

            $("a.sel").attr("href", "/classes/delete-schedules/selected/"+id+ "/" + timestamp);
            $("a.ent").attr("href", "/classes/delete-schedules/entire/"+id+ "/" + timestamp);
            $("a.fut").attr("href", "/classes/delete-schedules/future/"+id+ "/" + timestamp);

        });


        $("body").on("click", ".is-repeating", function(e){
            $(".certain-date").show();            
        });
        
        $("body").on("click", ".is-forever", function(e){
            $(".certain-date").hide();
        });

        $("body").on("click", ".select-days", function(e){
            $('.freq1').show();
            $('.freq2').show();
            $('.freq3').show();
        });

        $("body").on("click", ".everyday", function(e){
            $('.freq1').hide();
            $('.freq2').hide();
            $('.freq3').hide();
        });

        $("body").on("click", ".fc-time-grid-event", function(e){
            var title = $('#title').text();
            var descr = $('#descr').text();

            $.ajax({
                async:false,
                method: "POST",
                url: "/classes/get-details",
                data: {
                    title: title,
                    description: descr,
                    _token: "{{ csrf_token() }}"
                }
            })
            .done(function(result) {
                var data = JSON.parse(result);
                var class_deets = data['class_details'];
                var enrolled_dogs = data['enrolled_dogs'];

                $('#title').html(class_deets[0]['title']);
                $('#descr').html(class_deets[0]['description']);

                var endtime = class_deets[0]['time_start'] + (class_deets[0]['duration'] * 60);
                
                if (class_deets[0]['frequency'] == "ALL") {
                    var freq_days = "Monday to Friday";
                } else {
                    var temp = class_deets[0]['frequency'].split(",");
                    var freq_days = "";

                    $.each(temp, function(index, value){
                        var temp2 = value.charAt(0).toUpperCase() + value.slice(1);
                        freq_days += temp2 + "-";
                    });

                    freq_days = freq_days.substr(0, freq_days.length-2);
                }


                $('.delete-class').attr('data-id', class_deets[0]['id']);
                
                var schedule = "<p>" + moment(class_deets[0]['start_date'] * 1000).zone("+0:00").format('MMM D, YYYY') + " to " + moment(class_deets[0]['end_date'] * 1000).zone("+0:00").format('MMM D, YYYY') + "<br>";
                schedule +=  moment($('.delete-class').attr('data-selectdate') * 1000).zone("+0:00").format('h:mm A') + " to "  + moment($('.delete-class').attr('data-time') * 1000).zone("+0:00").format('h:mm A') + " (" + class_deets[0]['duration'] + " mins) <br>";
                schedule += freq_days + "</p>";
                $('#sched').html(schedule);
                
                var dogs = "<p>";
                if (enrolled_dogs == "") {
                    dogs += "None";
                } else {
                    dogs += "<tr>";
                    var absent_dogs = <?php echo $absent_dogs ?>;

                    var flag = true;
                    $.each(enrolled_dogs, function(index, value){
                        for(i=0; i<absent_dogs.length; i++) {
                            console.log(absent_dogs[i]['schedule']);
                            console.log($('#dogs_enrolled').attr('data-selectdate'));

                            val = absent_dogs[i]['schedule'];
                            val_dog = absent_dogs[i]['dog_id'];
                            val_class = absent_dogs[i]['class_id'];

                            if (val == $('#dogs_enrolled').attr('data-selectdate') && val_dog == value['id'] && val_class == class_deets[0]['id']) {
                                flag = false;
                            } else flag = true;

                            if (flag == false) {
                                i = absent_dogs.length;
                            }
                        }

                        if (flag == true) {                      
                            dogs += "<td>";
                            dogs += value['name'].charAt(0).toUpperCase() + value['name'].slice(1) +  "&nbsp;&nbsp;<a href='/dogs/mark-as-absent/" + value['id'] + "/" + class_deets[0]['id'] + "/" + $('#dogs_enrolled').attr('data-selectdate') +"'>";
                            dogs += "</td>";
                            dogs += "<td>";
                            dogs += "<span style='color: #D9534F'>Mark as Absent</span></a><br>";
                            dogs += "</td>";
                        }
                    });
                    dogs += "</tr>";
                }
                dogs += "</p>";

                $('.modal-title').html("View Schedule");
                $('#dogs_enrolled').html(dogs);
                $('.delete-msg').hide();
                
                $('.delete-class').show();

                $('.sched-deets').show();

                $('#CalenderModalEdit').click();
            });
        });



    </script>

    <script>


        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.required' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit) 
                this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

        var start_date;

        /*Full Calendar*/
        $(window).load(function () {

                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();
                var categoryClass;
                var started;

                var calendar = $('#calendar').fullCalendar({

                    defaultView: 'agendaWeek',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''
                    },
                    contentHeight: 600,
                    slotDuration: '00:30:00',
                    snapDuration:"00:30",
                    contentHeight: 750,
                    minTime: "07:00:00",
                    maxTime: "20:00:00",
                    selectable: true,
                    selectHelper: true,
                    allDaySlot: false,
                    editable: false,
                    slotEventOverlap: false,
                    ignoreTimezone: true,
                    select: function (start, end, allDay) {

                        started = start;
                        ended = end;

                        start_date = start.format('MM/DD/YYYY');
                        var start_time = start.format('h:mm a');
                        var start_timestamp = Date.parse(start) * 0.001;

                        var curr_date = <?php echo time(); ?>;

                        /* daterangepicker */

                        $('#dateend').daterangepicker({
                            singleDatePicker: true, 
                            calender_style: "picker_4",
                            startDate: start_date,
                        }, function (start, end, label) {
                            var dr_date = new Date($("#dateend").val()).getTime() * 0.001;
                            var sr_date = new Date($(".start-date").val()).getTime() * 0.001;
                            // var c_mmddyy = new Date(curr_date).getTime();
                            if (dr_date < sr_date) {
                                $(".error-msg").html("Please choose a day after your selected start date.");
                                $(".save-changes").attr('disabled', 'disabled');
                            } else {
                                $(".save-changes").removeAttr('disabled');
                                $(".error-msg").html("");
                            }

                        });

                        if (start_timestamp < curr_date) {
                            $('.modal-title').html("Error");
                            $('.error-content').show();
                            $('.error-content').html("<p> You cannot add a schedule on a day that has already passed. </p>");
                            $('.form-content').hide();
                            $('.modal-footer').hide();

                            $('#fc_create').click();

                        } else {
                            $('.modal-title').html("New Class Schedule");
                            $('.form-content').show();
                            $('.error-content').hide();
                            $('.modal-footer').show();

                            $('.start-date').val(start_date);
                            $('.start-time').val(start_time);
                            $('#fc_create').click();
                        }


                    },
                    eventClick: function (calEvent, jsEvent, view) {

                        // 

                        $('#fc_edit').click();
                        $('#title').html(calEvent.title);
                        $('#descr').html(calEvent.description);
                        $('#start').html(calEvent._start.format("h:mm a"));
                        // var strdate = new Date(calEvent._start.format("YYYY-MM-DD"));
                        var strdate = calEvent._start._d;
                        // alert();
                        var strtime = calEvent._end._d;

                        $('.delete-class').attr('data-selectdate', strdate.getTime() * 0.001);
                        $('.delete-class').attr('data-time', strtime.getTime() * 0.001);
                        $('#dogs_enrolled').attr('data-selectdate', strdate.getTime() * 0.001);

                        categoryClass = $("#event_type").val();

                        $(".antosubmit2").on("click", function () {
                            calEvent.title = $("#title").text();
                            calEvent.description = $("#descr").text();
                            calEvent.start = $("#start").text();

                            calendar.fullCalendar('updateEvent', calEvent);
                            $('.antoclose2').click();
                        });
                        calendar.fullCalendar('unselect');
                    },
                    editable: false,
                    events: '/calendar/get-schedule',
                    
                    viewRender: function(view,element){
                        var now = moment().subtract(1, 'month').format("M/YYYY");
                        var end = moment().add(4, 'month').format("M/YYYY");
                        var cal_date_string = view.start.format("M")+'/'+view.start.format("YYYY");


                            if(cal_date_string == now) { jQuery('.fc-prev-button').addClass("fc-state-disabled"); }
                            else { jQuery('.fc-prev-button').removeClass("fc-state-disabled"); }

                            if(end == cal_date_string) { jQuery('.fc-next-button').addClass("fc-state-disabled"); }
                            else { jQuery('.fc-next-button').removeClass("fc-state-disabled"); }
                    },
                });

            });

$(".modal").on("hidden.bs.modal", function(){
    $("#title").html("");
    $("#descr").html("");
    $("#sched").html("");
    $("#dogs_enrolled").html("");
});


    </script>

    <script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/moment-timezone.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>

@stop