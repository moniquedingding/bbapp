@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
 <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
 <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <h2></h2>
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                  </ul>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  @if (isset($message))
                      <div class="alert alert-success" role="alert">
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                          {{ $message }}
                      </div>
                  @endif
                  <div class="" role="tabpanel" data-example-id="settings-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          @if($for_all_view['current_usertype'] == "1")
                          <li role="presentation" class="active"><a href="#general_config" id="general-tab" role="tab" data-toggle="tab" aria-expanded="true">General Configurations</a>
                          </li>
                          <li role="presentation" class=""><a href="#centre_info" id="centre_info_tab" role="tab" data-toggle="tab" aria-expanded="false">Centre Information</a>
                          </li>
                          <li role="presentation" class=""><a href="#email_setting" id="email-tab" role="tab" data-toggle="tab" aria-expanded="false">Email Setting</a>
                          </li>
                          @else
                          <li role="presentation" class="active"><a href="#centre_info" id="centre_info_tab" role="tab" data-toggle="tab" aria-expanded="false">Centre Information</a>
                          </li>
                          <li role="presentation" class=""><a href="#email_setting" id="email-tab" role="tab" data-toggle="tab" aria-expanded="false">Email Setting</a>
                          </li>
                          @endif
                      </ul>
                      <div id="myTabContent" class="tab-content">
                          @if($for_all_view['current_usertype'] == "1")
                          <!--Content for General Configuration-->
                          <div role="tabpanel" class="tab-pane fade active in" id="general_config" aria-labelledby="general-tab">
                            <form id="general-setting-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="" enctype="multipart/form-data">
                              <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Application Name
                                  </label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                      <input id="app_name" name="app_name" class="date-picker form-control" required="required" type="text" value="{{ $settings['app_name'] }}">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Application Logo
                                  </label>
                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                    @if(!empty($settings['app_logo']))
                                    <div id="imageName" style="padding-top:8px;"><a href="/settings/remove-image/{{ $settings['id'] }}"><i class="fa fa-times red" style="font-size:15px;"></i></a> {{ $settings['app_logo'] }}</div>
                                    @else
                                    <input type="file" name="image" id="image" onchange="imageUpload()" accept="image/*" class="hidden">
                                    <label for="image" class="btn btn-default">Choose File</label>
                                    <span id="imageName"></span>
                                    @endif
                                  </div>
                              </div>
                              <div class="clearfix"></div>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-success">Update Settings</button>
                                      </div>
                                  </div>
                            </form><!--/#general-setting-form-->
                          </div>
                          <!--end of General Configuration-->
                          <!--Content for Centre Information-->
                          <div role="tabpanel" class="tab-pane fade" id="centre_info" aria-labelledby="centre_info_tab">
                          @else
                          <!--Content for Centre Information-->
                          <div role="tabpanel" class="tab-pane fade active in" id="centre_info" aria-labelledby="centre_info_tab">
                          @endif
                            <table id="centre_info_table" class="table table-striped responsive-utilities jambo_table table-highlight-links table-setting" style="font-size: 11px;">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>E-mail</th>
                                        <th>Phone</th>
                                        <th>Mobile</th>
                                        <th>Fax</th>
                                        <th>Pricing</th>
                                        <th class=" no-link last"><span class="nobr">Action</span>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php $count = 0; ?>
                                  @foreach ($centres as $row)
                                      @if ($count % 2 == 0)   
                                      <tr class="even pointer">  
                                      @else                     
                                      <tr class="odd pointer">
                                      @endif
                                          <td>{{ ++$count }}</td>
                                          <td>{{ $row->name }}</td>
                                          <td style="width: 15%"><?php echo nl2br($row->address); ?></td>
                                          <td>{{ $row->email }}</td>
                                          <td>{{ $row->phone }}</td>
                                          <td>{{ $row->mobile }}</td>
                                          <td>{{ $row->fax }}</td>
                                          <td style="white-space: nowrap;">
                                            £ {{ number_format($row->price, 2, '.', '') }} - original <br>
                                            £ {{ number_format($row->discounted_price, 2, '.', '') }} - discounted <br>
                                            £ {{ number_format($row->custom_price, 2, '.', '') }} - custom
                                          </td>
                                          <td style="white-space: nowrap;"><a href="/settings/edit-centre/{{ $row->id }}">Update info</a></td>
                                      </tr>
                                  @endforeach
                                </tbody>
                            </table>
                          </div>
                          <!--end of Centre Information-->
                          <!--Content for Email Setting-->
                          <div role="tabpanel" class="tab-pane fade" id="email_setting" aria-labelledby="email-tab">  
                            <table id="email_setting_table" class="table table-striped responsive-utilities jambo_table table-highlight-links table-setting" style="font-size: 11px;">
                                <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th class=" no-link last"><span class="nobr">Action</span>
                                    </tr>
                                <tbody>
                                  <?php $count = 0; ?>
                                  @foreach ($centres as $row)
                                      @if ($count % 2 == 0)   
                                      <tr class="even pointer">  
                                      @else                     
                                      <tr class="odd pointer">
                                      @endif
                                          <td>{{ ++$count }}</td>
                                          <td>{{ $row->name }}</td>
                                          <td>{{ $row->email }}</td>
                                          <td style="white-space: nowrap;"><a href="/settings/edit-email/{{ $row->id }}">Edit</a></td>
                                      </tr>
                                  @endforeach
                                </tbody>
                                </thead>
                            </table>
                          </div>
                          <!--End of Email Setting-->
                      </div>
                  </div>

              </div>
          </div>
      </div>
    </div>
</div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#invoice-form .btn').on('click', function () {
        $('#invoice-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#invoice-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});

//image upload
function imageUpload(){
    var valName = document.getElementById("image").files[0].name;
    $('#imageName').html(valName);
}
</script>

<!-- Datatables -->
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('.table-setting').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
    ],
            'iDisplayLength': 12,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "{{ asset('js/datatables/tools/swf/copy_csv_xls_pdf.swf') }}"
            }
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });

  //Go to specific tab on Page Reload
  $(document).ready(function(){
    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
    } 

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
  });
</script>
@stop