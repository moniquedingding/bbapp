<!-- footer content -->
<footer>
 <div class="">
  <p class="pull-right">
   <span class="lead"> <i class="fa fa-paw"></i> {{ $for_all_view['app_name'] }}</span>
  </p>
 </div>
 <div class="clearfix"></div>
</footer>
<!-- /footer content -->