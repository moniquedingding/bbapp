
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="/" class="site_title"><i class="fa fa-paw"></i> <span><small>{{ $for_all_view['app_name'] }}</small></span></a>
                    </div>
                    <div class="clearfix"></div>


                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            @foreach($for_all_view['current_user_info'] as $row)
                                @if(!empty($row->image))
                                <img src="{{ asset('/images/users/'.$row->image) }}" alt="..." class="img-circle profile_img">
                                @else
                                <img src="{{ asset('/images/users/no-image.jpg') }}" alt="..." class="img-circle profile_img">
                                @endif
                            @endforeach
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            @foreach($for_all_view['current_user_info'] as $row)
                            <h2>{{ $row->firstname }} {{ $row->lastname }}</h2>
                            @endforeach
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            @if($for_all_view['current_usertype']=="1")
                            <h3>Admin</h3>
                            @else
                            <h3>Manager</h3>
                            @endif
                            <ul class="nav side-menu">
                                <li><a href="/"><i class="fa fa-home"></i> Dashboard </a>
                                </li>
                                @if($for_all_view['current_usertype']=="1")
                                <li><a href="/users"><i class="fa fa-group"></i> Users </a>
                                </li>
                                @endif
                                <li><a><i class="fa fa-tasks"></i> Management <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/calendar">Calendar</a></li>
                                        <li><a href="/customers">Customers</a></li>
                                        <li><a href="/dogs">Dogs</a></li>
                                        <li><a href="/classes">Classes</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-calculator"></i> Finance <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/invoice">Invoices</a></li>
                                        <li><a href="/receipt">Receipts</a></li>
                                        <li><a href="/credits">Credits</a></li>
                                    </ul>
                                </li>
                                <li><a href="/settings"><i class="fa fa-cog"></i> Settings </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a href="/settings" data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a href="/logout" data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
