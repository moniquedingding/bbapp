
            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    @foreach($for_all_view['current_user_info'] as $row)
                                        @if(!empty($row->image))
                                        <img src="{{ asset('/images/users/'.$row->image) }}" alt="">
                                        @else
                                        <img src="{{ asset('/images/users/no-image.jpg') }}" alt="">
                                        @endif
                                    {{ $row->firstname }} {{ $row->lastname }} 
                                    @endforeach
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="/profile">  Profile</a>
                                    </li>
                                    @if($for_all_view['current_usertype']=="1")
                                    <li class="divider"></li>
                                    <li class="menu-label-only"><a>View As:</a></li>
                                        <li class="switch-user-topbar"><a href="/view-as/0">Admin</a></li>
                                        @foreach($for_all_view['centres_info_for_all'] as $cforall)
                                        <li class="switch-user-topbar"><a href="/view-as/{{$cforall->id}}">{{ $cforall->name }}</a></li>
                                            @if($cforall->id == $for_all_view['current_centre'])
                                                <?php $topbar_centre_name = $cforall->name; ?>
                                            @endif
                                        @endforeach
                                    @endif
                                    <li class="divider"></li>
                                    <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                            @if($for_all_view['view_as'])
                            <li role="presentation">
                                <a href="/view-as/0" class="dropdown-toggle info-number" data-toggle="tooltip" data-placement="bottom" title="Click to view as Admin" aria-expanded="false" style="color: #6B4004 !important;">
                                    <i class="fa fa-info-circle" style="font-size: 17px;"></i> Currently viewing {{$topbar_centre_name}} Centre
                                </a>
                            </li>
                            @endif
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->