@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Payment Details <small><a href="/receipt"> Go back to receipts list </a> </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                                $customer = $customers_invoices[0];
                            ?>

                            <table class="table table-bordered">

                                <style type="text/css">
                                    .thirty{
                                        width: 30%;
                                        font-weight: bold;
                                    }
                                </style>
                                
                                <tr>
                                    <td class="thirty">Date</td>
                                    <td>{{ date("D, M j, Y", time()) }}</td>
                                </tr>
                                <tr>
                                    <td class="thirty">Receipt # </td>
                                    <td>{{ sprintf( '%07d', $receipt_id) }}</td>
                                </tr>
                                <tr>
                                    <td class="thirty">Customer Name</td>
                                    <td>{{ ucfirst($customer->firstname)." ".ucfirst($customer->lastname) }}</td>
                                </tr>
                                <tr>
                                    <td class="thirty">Description</td>
                                    <td>
                                        Payment for invoice # {{ sprintf( '%07d', $invoice_id) }}<br>
                                        {{ ucfirst($customer->name) }} enrolled in class {{ ucfirst($customer->title) }}
                                    </td>
                                </tr>
                                
                            </table>

                            <table class="table table-bordered">
                                <tr>
                                    <td class="thirty">Invoice Amount Due</td>
                                    <td>£ {{ number_format($customer->amount_due, 2, '.', '') }}</td>
                                </tr>

                                <tr>
                                    <td class="thirty">Tax</td>
                                    <td>£ 0.00</td>
                                </tr>

                                <tr>
                                    <td class="thirty" style="color: #26B99A">Amount Paid</td>
                                    <td>£ {{ number_format($receipts->amount_paid, 2, '.', '') }}</td>
                                </tr>

                                <tr>
                                    <td class="thirty" style="color: #26B99A">Total Amount Paid</td>
                                    <td>£ {{ number_format($customer->amount_paid, 2, '.', '') }}</td>
                                </tr>
                            </table>

                            <table class="table table-bordered">
                                <tr>
                                    <td class="thirty">
                                        <span>Payment Method </span> <br>
                                    </td>
                                    <td>

                                        {{ ucfirst($customer->payment_method) }}
                                    </td>
                                </tr>

                            </table>

                        </div>
                    </div><!--/.row-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>

        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Payment History</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Receipt #</th>
                                    <th>Date Paid</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($payment_history as $row)
                                <tr>
                                    <td><a href="/receipt/view/{{ $row->id }}">{{ sprintf( '%07d', $row->id) }}</a></td>
                                    <td>{{ date("D, M j, Y", $row->date_paid) }}</td>
                                    <td>£ {{ number_format($row->amount_paid, 2, '.', '') }}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div><!--/.row-->
                        
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#invoice-form .btn').on('click', function () {
        $('#invoice-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#invoice-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.date-picker').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#date-range').daterangepicker({
        calender_style: "picker_2",
        "startDate": moment().startOf('month'),
        "endDate": moment().endOf('month')
    });
}); 
</script>
@stop