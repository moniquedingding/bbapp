@extends('admin.admin_layout')

@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }} @if($edit == TRUE)<span style="font-size: 12px;"><a href="/users">Back to Users List</a></span>@endif
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 animated fadeInDown">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Profile Preview</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if (isset($message))
                        <div class="alert alert-success" role="alert">
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            {{ $message }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <center>
                                <div class="avatar-view">
                                    @if(!empty($user['image']))
                                    <img src="{{ asset('/images/users/'.$user['image']) }}" alt="Avatar">
                                    @else
                                    <img src="{{ asset('/images/users/no-image.jpg') }}" alt="Avatar">
                                    @endif
                                </div>
                            </center>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <h3>{{ $user['firstname'] }} {{ $user['lastname'] }}</h3>
                            <ul class="list-unstyled user_data">
                                <li><i class="fa fa-map-marker user-profile-icon"></i> {{ $user['address'] }}</li>
                                <li><i class="fa fa-envelope user-profile-icon"></i> {{ $user['email'] }}</li>
                                @if(!empty($user['phone']))
                                    <li><i class="fa fa-phone fa-lg user-profile-icon"></i> {{ $user['phone'] }}</li>
                                @endif
                                <li><i class="fa fa-mobile fa-lg user-profile-icon"></i> {{ $user['mobile'] }}</li>
                            </ul>
                            <button id="btn-profile-edit" class="btn btn-success pull-right"><span class="span-hide" style="display:none;">Hide</span><span class="span-show">Show</span> Edit Form &nbsp;&nbsp;<i class="glyphicon glyphicon-chevron-down"></i>  </button>
                        </div>
                    </div>
                </div><!--/x_content-->
            </div><!--/x_panel-->
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <a class="collapse-link title-collapse-link"><h2 class="closed-label-collapse">Edit Profile Information</h2></a>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="display:none;">
                    @if($edit == "TRUE")
                    <form class="form-horizontal form-label-left" method="POST" action="/users/edit/{{ $user['id'] }}" enctype="multipart/form-data" novalidate>
                    @else
                    <form class="form-horizontal form-label-left" method="POST" action="" enctype="multipart/form-data" novalidate>
                    @endif
                        <p>Please fill out the necessary details marked with an asterisk (*) below.</p>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="1" name="firstname" required="required" type="text" value="{{ $user['firstname'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="2" data-validate-words="1" name="lastname" required="required" type="text" value="{{ $user['lastname'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="textarea" name="address" required="required" class="form-control col-md-7 col-xs-12">{{ $user['address'] }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $user['email'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Telephone Number
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="number" name="phone" class="form-control col-md-7 col-xs-12" value="{{ $user['phone'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Mobile Number <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="number" name="mobile" required="required" class="form-control col-md-7 col-xs-12" value="{{ $user['mobile'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Image
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                @if(!empty($user['image']))
                                <div id="imageName" style="padding-top:8px;"><a href="/profile/remove-image/{{ $user['id'] }}"><i class="fa fa-times red" style="font-size:15px;"></i></a> {{ $user['image'] }}</div>
                                @else
                                <input type="file" name="image" id="image" onchange="imageUpload()" accept="image/*" class="hidden">
                                <label for="image" class="btn btn-default">Choose File</label>
                                <span id="imageName"></span>
                                @endif
                            <!--<p class="help-block">Leave blank if not Changed</p>-->
                            </div>
                        </div>
                        @if($edit == TRUE)
                            <br>
                            <span class="section">Other Information</span>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">User Type <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="usertype" name="usertype" class="form-control">
                                      <option value="1" <?php if($user['usertype'] == "1") echo "selected"; ?> >Admin</option>
                                      <option value="2" <?php if($user['usertype'] == "2") echo "selected"; ?> >Manager</option>
                                    </select>
                                </div>
                            </div>
                            @if($user['usertype'] == "1")
                            <div class="form-group" id="centre-group" style="<?php if($user['usertype'] == "1") echo "display:none;"; ?>">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="centre">Centre <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    @foreach($centres as $row)
                                        <input type="radio" id="centre" name="centre" value="{{ $row->id }}" <?php if($row->id == 1) echo "checked"; ?> > {{ $row->name }} &nbsp;&nbsp;
                                    @endforeach
                                </div>
                            </div>
                            @else
                            <div class="form-group" id="centre-group" style="<?php if($user['usertype'] == "1") echo "display:none;"; ?>">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="centre">Centre <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    @foreach($centres as $row)
                                        <input type="radio" id="centre" name="centre" value="{{ $row->id }}" <?php if($user['centre_id'] == $row->id) echo "checked"; ?> > {{ $row->name }} &nbsp;&nbsp;
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        @endif
                        <div class="ln_solid"></div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="username" name="username" class="form-control col-md-7 col-xs-12" value="{{ $user['username'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12" placeholder="Leave blank if not changed" value="">
                                <input type="hidden" id="cpassword" name="cpassword" value="{{ $user['password'] }}">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a href="javascript:history.back()" class="btn btn-primary">Cancel</a>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script src="{{ asset('js/validator/validator.js') }}"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);


$('.title-collapse-link').click(function() {
    $(".span-hide").toggle();
    $(".span-show").toggle();
    var buttonClick = $(this).siblings('.panel_toolbox').find('li a i');
    buttonClick.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
});

//image upload
function imageUpload(){
    var valName = document.getElementById("image").files[0].name;
    $('#imageName').html(valName);
}

$('#btn-profile-edit').on('click', function(){
    $('.title-collapse-link').trigger('click');
});

//Usertype on Change
$('#usertype').on('change', function(){
    if($(this).val()==="2"){
        $("#centre-group").fadeIn();
    } else {
        $("#centre-group").fadeOut();
    }
});
</script>
@stop