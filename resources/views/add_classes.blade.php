
@extends('admin.admin_layout')
@section('title', $title)

@section('additional_head')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.css" rel="stylesheet">
@stop

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Class Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if (empty($id))
                    <form class="form-horizontal form-label-left" method="POST" action="/classes/add" novalidate>
                    @else
                    <form class="form-horizontal form-label-left" method="POST" action="/classes/edit/{{ $id }}" novalidate>
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                        <p>Please fill out the necessary details marked with an asterisk (*) below.</p>

                        @if (isset($message))
                            <div class="alert alert-success" role="alert">
                                {{ $message }}
                                <br><a href="/classes"><strong>Go back to classes list</strong></a>
                            </div>
                        @endif

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Centre <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php
                                    if($for_all_view['current_usertype'] != "1" || $for_all_view['view_as'])
                                        $dataC = $centre_per_centre;
                                    else
                                        $dataC = $centres;
                                 ?>
                                @foreach($dataC as $row)
                                    <input type="radio" name="centre_id[]" value="{{ $row->id }}" required="required" <?php if(!empty($included_centres) && $included_centres[0]->centre_id == $row->id) echo "checked"; ?>> {{ ucfirst($row->name) }} &nbsp;&nbsp;&nbsp;
                                @endforeach
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="1" name="title" required="required" type="text" value="{{ $classes['title'] }}">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="name" class="form-control col-md-7 col-xs-12" required="required" name="description">{{ $classes['description'] }}</textarea>
                            </div>
                        </div>

                        @if (!isset($is_edit))
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Duration in Minutes <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="duration" class="form-control">
                                    <option value="30" <?php if ($classes['duration'] == 30) echo "selected"; ?>>30 mins</option>
                                    <option value="60"  <?php if ($classes['duration'] == 60) echo "selected"; ?>>1 hr </option>
                                    <option value="90"  <?php if ($classes['duration'] == 90) echo "selected"; ?>>1 hr 30 mins</option>
                                    <option value="120"  <?php if ($classes['duration'] == 120) echo "selected"; ?>>2 hrs</option>
                                    <option value="150"  <?php if ($classes['duration'] == 150) echo "selected"; ?>>2 hrs 30 mins</option>
                                    <option value="180"  <?php if ($classes['duration'] == 180) echo "selected"; ?>>3 hrs</option>
                                    <option value="210"  <?php if ($classes['duration'] == 210) echo "selected"; ?>>3 hrs 30 mins</option>
                                    <option value="240"  <?php if ($classes['duration'] == 240) echo "selected"; ?>>4 hrs</option>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Start Date <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="datestart" class="date-picker form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="2" required="required" name="start_date" type="text" value="<?php if (!empty($classes['start_date'])) echo date('m/d/Y', $classes['start_date']); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Repeat
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <table class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <td><input type="radio" class="is-repeating" name="is_repeating" value="certain_date"  <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Until Specified </td>
                                        <td><input type="radio" class="is-forever" name="is_repeating" checked value="forever" <?php //if($classes['centre_id'] == $row->id) echo "checked"; ?>> Forever </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="item form-group end-date">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="name">End Date <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="dateend" class="date-picker form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="2" required="required" name="end_date" type="text" value="<?php if (!empty($classes['end_date'])) echo date('m/d/Y', $classes['end_date']); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Frequency 
                            </label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <table class="table" style="margin-bottom: 0px;">
                                    <tr>
                                        <td><input class="select-days" type="radio" name="frequency" value="" <?php if (isset($classes['frequency']) && ($classes['frequency'] != "ALL")) { echo "checked"; } ?>> Select days</td>
                                        <td><input class="everyday" type="radio" name="frequency" value="ALL" <?php if (isset($classes['frequency']) && ($classes['frequency'] == "ALL")) { echo "checked"; } ?>> Everyday</td>
                                    </tr>
                                    <?php
                                        if (isset($classes['frequency'])) {
                                            $str = explode(",", $classes['frequency']);
                                        }
                                    ?>
                                    <tr class="freq1">
                                    <td><input type="checkbox" name="frequency[]" value="monday"  <?php $res = array_search("monday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Monday</td>
                                    <td><input type="checkbox" name="frequency[]" value="tuesday"  <?php $res = array_search("tuesday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Tuesday</td>
                                    </tr>
                                    <tr class="freq2">
                                    <td><input type="checkbox" name="frequency[]" value="wednesday"  <?php $res = array_search("wednesday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Wednesday</td>
                                    <td><input type="checkbox" name="frequency[]" value="thursday"  <?php $res = array_search("thursday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Thursday</td>
                                    
                                    <tr class="freq3">
                                    <td><input type="checkbox" name="frequency[]" value="friday"  <?php $res = array_search("friday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Friday</td>
                                    <td><input type="checkbox" name="frequency[]" value="saturday"  <?php $res = array_search("saturday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Saturday</td>
                                    </tr>
                                    <tr class="freq4">
                                    <td><input type="checkbox" name="frequency[]" value="sunday"  <?php $res = array_search("sunday", $str); if(is_numeric($res)) echo "checked"; ?>> &nbsp;Sunday</td>
                                    <td></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Time Start <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type='text' name='time_start' id='' class='timepicker form-control' value="{{ date('h:i A', $classes['time_start'])  }}" /> 
                            </div>
                        </div>
                        @endif
                        <div class="ln_solid"></div>

                        @if (isset($is_edit) && isset($classes['id']))
                            <input type="hidden" name="id" value="{{ $classes['id'] }}">
                            <input type="hidden" name="is_edit" value="{{ $is_edit }}">
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a href="/classes" class="btn btn-primary">Cancel</a>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>   
            </div>   
        </div>
    </div>
</div>
@stop

@section('additional_script')
<!-- Timepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>

<!-- daterangepicker -->
<script type="text/javascript">
    $('.timepicker').timepicker({
        'timeFormat': 'h:i A',
        'minTime': '7:00 AM',
        'maxTime': '7:00 PM',
        'forceRoundTime': true
    });

    if ($(".select-days").attr('checked') != "checked") {
        $(".freq1").hide();
        $(".freq2").hide();
        $(".freq3").hide();
        $(".freq4").hide();
    }

    $(".end-date").hide();
    
    $("body").on("click", ".is-repeating", function(e){
        $(".end-date").show();
    });
    
    $("body").on("click", ".is-forever", function(e){
        $(".end-date").hide();
    });


    $("body").on("click", ".select-days", function(e){
        $(".freq1").show();
        $(".freq2").show();
        $(".freq3").show();
        $(".freq4").show();
    });

    $("body").on("click", ".everyday", function(e){
        $(".freq1").hide();
        $(".freq2").hide();
        $(".freq3").hide();
        $(".freq4").hide();
    });


    $(document).ready(function () {
        $('#datestart').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#dateend').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

    });
</script>
@stop
