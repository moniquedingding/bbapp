@extends('admin.admin_layout')
@section('title', $title)

@section('additional_head')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
 <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
 <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if($for_all_view['current_usertype'] != "1")
            <div class="alert alert-danger" role="alert">
                You are not allowed to access this page.
            </div>
        @else
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <a href="/users/add" class="btn btn-success"><i class="fa fa-plus"></i> Add New User</a>
                    @if (isset($message))
                        <div class="alert alert-success" role="alert">
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            {{ $message }}
                        </div>
                    @endif
                    <table id="example" class="table table-striped responsive-utilities jambo_table table-highlight-links" style="font-size: 11px;">
                        <thead>
                            <tr class="headings">
                                <th>#</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>E-mail</th>
                                <th>Centre</th>
                                <th>User Type</th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 0; ?>
                            @foreach ($users as $row)
                                @if ($count % 2 == 0)   
                                <tr class="even pointer">  
                                @else                     
                                <tr class="odd pointer">
                                @endif
                                    <td>{{ ++$count }}</td>
                                    <td>{{ $row->firstname }} {{ $row->lastname }}</td>
                                    <td>{{ $row->mobile }}</td>
                                    <td>{{ $row->email }}</td>
                                    @if($row->centre_id == 0)
                                        <td>All</td>
                                    @else
                                        @foreach ($centres as $c)
                                            @if($c->id == $row->centre_id)
                                            <td>{{ $c->name }}</td>
                                            <?php break; ?>
                                            @endif
                                        @endforeach
                                    @endif

                                    @if($row->usertype == '1')
                                        <td>Admin</td>
                                    @else
                                        <td>Manager</td>
                                    @endif
                                    <td><a href="/users/edit/{{ $row->id }}">View/Update</a>

                                        @if ($current_centre == 0)
                                        |
                                        <a href="/users/delete/{{ $row->id }}" onclick="return confirm('Permanently delete <?php echo e(ucwords($row->firstname)); ?> <?php echo e(ucwords($row->lastname)); ?>? This action is NOT reversible once completed.');">Delete</a> 
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

@section('additional_script')
    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
        ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "{{ asset('js/datatables/tools/swf/copy_csv_xls_pdf.swf') }}"
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop