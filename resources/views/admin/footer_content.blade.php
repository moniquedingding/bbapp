<!-- footer content -->
<footer>
    <div class="">
        <p class="pull-right">Made for Best Behaviour School for Dogs by Futuristech Studios, Inc. Copyright 2015. |
            <span class="lead"> <i class="fa fa-paw"></i> Best Behaviour App Admin</span>
        </p>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->