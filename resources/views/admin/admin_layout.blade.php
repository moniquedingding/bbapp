<!DOCTYPE html>
<html lang="en">

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <!-- Meta, title, CSS, favicons, etc. -->
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <title>@yield('title') - {{ $for_all_view['app_name'] }} - @if($for_all_view['current_usertype']=="1") Admin @else Manager @endif</title>

 <!-- Bootstrap core CSS -->
 <link href="{{ asset('images/users/'.$for_all_view['app_logo']) }} " rel="shortcut icon">
 <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

 <link href="{{ asset('fonts/css/font-awesome.min.css') }}" rel="stylesheet">
 <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

 <!-- Custom styling plus plugins -->
 <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

 <!-- Additional Declaration to Head -->
@yield('additional_head')

</head>

<body class="nav-md">
 <div class="container body">
  <div class="main_container">

   @include('includes.sidebar')
   @include('includes.topnav')

   <!-- page content -->
   <div class="right_col" role="main">

    @yield('content')

    @include('includes.footer_content')

   </div>
   <!-- /page content -->
  </div>
 </div>
 <div id="custom_notifications" class="custom-notifications dsp_none">
   <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
   </ul>
   <div class="clearfix"></div>
   <div id="notif-group" class="tabbed_notifications"></div>
 </div>

 <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
 <script src="{{ asset('js/bootstrap.min.js') }}"></script>
 <script src="{{ asset('js/nprogress.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>

 <!-- chart js -->
 <script src="{{ asset('js/chartjs/chart.min.js') }}"></script>
 <!-- bootstrap progress js -->
 <script src="{{ asset('js/progressbar/bootstrap-progressbar.min.js') }}"></script>
 <script src="{{ asset('js/nicescroll/jquery.nicescroll.min.js') }}"></script>
 <!-- icheck -->
 <script src="{{ asset('js/icheck/icheck.min.js') }}"></script>

 <script src="{{ asset('js/custom.js') }}"></script>

 @yield('additional_script')

</body>

</html>