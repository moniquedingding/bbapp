<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }} - Best Behaviour School for Dogs - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('fonts/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
    <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
    <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
    <link href="{{ asset('css/calendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/calendar/fullcalendar.print.css') }}" rel="stylesheet" media="print">

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <style type="text/css">

    a.clear-anchor-style {
        text-decoration: none;
    }

    .danger-color {
        color: #F56954 !important;
    }

    .success-color {
        color: #26B99A !important;
    }

    .warning-color {
        color: #F0AD4E !important;
    }

    .info-color {
        color: #5BC0DE !important;
    }

    .smaller-font {
        font-size: 10px;
    }

    </style>

</head>
