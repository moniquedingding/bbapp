
@extends('admin.admin_layout')
@section('title', $title)

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>

    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Basic Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if (empty($id))
                    <form class="form-horizontal form-label-left" method="POST" action="/dogs/add" enctype="multipart/form-data" novalidate>
                    @else
                    <form class="form-horizontal form-label-left" method="POST" action="/dogs/edit/{{ $id }}" enctype="multipart/form-data" novalidate>
                    <input type="hidden" name="id" value="{{ $id }}">
                    @endif
                        @if (isset($message))
                            <div class="alert alert-danger" role="alert">
                                {{ $message }}
                            </div>
                        @endif
                        <p>Please fill out the necessary details marked with an asterisk (*) below.</p>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Owner <span class="required">*</span>
                            </label>

                            <div class="col-md-1 col-sm-1 col-xs-6">
                                
                                <select class="form-control letter" required>
                                    <option value="dash">--</option>

                                    @for ($i = 65; $i < 91; $i++)
                                        <option value="{{ chr($i) }}">{{ chr($i) }}</option>
                                    @endfor
                                </select>
                            </div>

                           <div class="col-md-5 col-sm-5 col-xs-6">
                                    @if (isset($dogs['customer_id']))
                                        @foreach($customers as $row)
                                            @if ($dogs['customer_id'] == $row->id)
                                                <input type="hidden" name="customer_id" value="{{ $row->id }}">
                                                <input type="text" class="form-control" value="{{ ucfirst($row->firstname)." ".ucfirst($row->lastname) }}" readonly>
                                            @endif
                                        @endforeach
                                    @else
                                        <select class="form-control customers-dropdown" name="customer_id">
                                            <option> None </option>
                                        </select>
                                    @endif
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="1" name="name" required="required" type="text" value="{{ $dogs['name'] }}">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Breed <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="1" name="breed" required="required" type="text" value="{{ $dogs['breed'] }}">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Sex <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="radio" name="sex" value="male"> Male &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="sex" value="female"> Female
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Color <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="1" data-validate-words="1" name="color" required="required" type="text" value="{{ $dogs['color'] }}">
                            </div>
                        </div>
                        
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Age <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="1" placeholder="e.g., 3 years and 9 months" name="age" required="required" type="text" value="{{ $dogs['age'] }}">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Image
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                @if(!empty($dogs['image']))
                                <div id="imageName" style="padding-top:8px;"><a href="/dogs/remove-image/{{ $id }}"><i class="fa fa-times red" style="font-size:15px;"></i></a> {{ $dogs['image'] }}</div>
                                @else
                                <input type="file" name="image" id="image" onchange="imageUpload()" accept="image/*" class="hidden">
                                <label for="image" class="btn btn-default">Choose File</label>
                                <span id="imageName"></span>
                                @endif
                            </div>
                        </div>

                        <div class="item form-group">
                            <br>                        
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Pricing <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="radio" name="price_type" value="existing" class="existing-price"> Existing &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="price_type" value="new" class="new-price"> New &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="price_type" value="custom" class="custom-price"> Custom
                            </div>
                        </div>

                        <div class="item form-group custom-price-input">
                            <br>                        
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customp">Custom Price <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" id="customp" name="new_custom_price">
                            </div>
                        </div>
                        
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a href="/dogs" class="btn btn-primary">Cancel</a>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>   
            </div>   
        </div>
    </div>
</div>
@stop

@section('additional_script')
<script type="text/javascript">
    
    $('.custom-price-input').hide();   

    $(document).ready(function() {
        $('.custom-price').on('click', function() {
            $('.custom-price-input').show();
        });

        $('.existing-price').on('click', function() {
            $('.custom-price-input').hide();
        });

        $('.new-price').on('click', function() {
            $('.custom-price-input').hide();
        });
        

    });

    var html_string = "";
    
    $('.customers-dropdown').attr('disabled', 'true');
    
    $(document).ready(function() {
            
        $('.form-control.letter').on('change', function() {
            var html_string = "";
            var letter = $('.letter').val();
            var customers = <?php echo json_encode($customers); ?>;
            
            if (letter == "dash") {
                html_string += '<option> None </option>';
                $('.customers-dropdown').attr('disabled');
            } else {

            }

            $.each(customers, function(index, value) {
                if (letter == value['firstname'].substr(0, 1)) {
                    html_string += '<option value="' + value['id'] + '">' + value['firstname'] + ' '+ value['lastname'] + '</option>';
                }
            })

            $('.customers-dropdown').html(html_string);
            $('.customers-dropdown').attr('disabled', false);

        });
    });
</script>

<!-- daterangepicker -->
<script type="text/javascript">

    $(document).ready(function () {
        $('#datestart').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#dateend').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

    });
</script>

<!-- form validation -->
<script src="{{ asset('js/validator/validator.js') }}"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);

    //image upload
    function imageUpload(){
        var valName = document.getElementById("image").files[0].name;
        $('#imageName').html(valName);
    }
</script>

<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
@stop
