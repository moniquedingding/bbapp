
@extends('admin.admin_layout')
@section('title', $title)

@section('additional_head')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.css" rel="stylesheet">
@stop

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>

        <div class="title_right">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input name="search-input" id="search-input" type="text" class="form-control" placeholder="Search Dog or Owner Name">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go</button>
                    </span>
                </div>
            </div>
        </div>
    </div><!--/.page-ttile-->
    <div class="clearfix"></div>

    <div class="row">
        @if (empty($id))
        <form class="form-horizontal form-label-left" method="POST" action="/classes/enroll" novalidate>
        @else
        <form class="form-horizontal form-label-left" method="POST" action="/classes/edit/{{ $id }}" novalidate>
        <input type="hidden" name="id" value="{{ $id }}">
        @endif
            
            <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">
                <div class="x_panel class-detail">
                    <div class="x_title">
                        <h2><i class="fa fa-list-ul"></i> Class Details</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content class-detail-content">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td><strong>Class:</strong></td>
                                    <td>{{ $classes['title'] }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Description:</strong></td>
                                    <td>{{ $classes['description'] }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Date:</strong></td>
                                    <td>From {{ date("M j", $classes['start_date']) }} to {{ date("M j", $classes['end_date']) }} at {{ date("g:i A", $classes['time_start']) }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Duration:</strong></td>
                                    <td>{{ $classes['duration'] }} minutes</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-sm-8 col-xs-12 animated fadeInDown">
                <div class="x_panel select-dog">
                    <div class="x_title">
                        <h2><i class="fa fa-paw"></i> Select a Dog to enroll</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="well">
                            IMPORTANT NOTE:<br>
                            Please <strong>properly specify the end date </strong> as it is used to calculate the duration of the dog's enrollment to the class.
                            Ideally, the date must be between the start and the end date of the class.
                        </div>

                        <div class="list-dogs">
                            @foreach($dogs as $row)
                                @foreach($customers as $c)
                                    @if ($row->customer_id == $c->id && ($c->centre_id == $centre_id))
                                    
                                    <div data-filter="{{ $row->name }}" class="col-md-6 col-sm-6 col-xs-12 animated fadeInDown dogs-block">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h2><a href="/dogs/view/{{ $row->id }}">{{ $row->name }}</a></h2>
                                                <div class="row">
                                                    <div class="left col-xs-7 left-dog-enroll">
                                                        <small>
                                                        <p class="dog-owner-name"><strong>Owner: </strong><a href="/customers/view/{{ $c->id }}" style="color: #16418A;">{{ ucfirst($c->firstname)." ".ucfirst($c->lastname) }}</a></p>

                                                            @if(strlen($row->breed) >= 20)
                                                                <p><strong>Breed: </strong>{{ substr(ucwords($row->breed), 0, 20) }}...</a></p>
                                                            @else
                                                                <p><strong>Breed: </strong>{{ ucwords($row->breed) }}</a></p>
                                                            @endif

                                                        <p><strong>Age: </strong>{{ strtolower($row->age) }}</p>
                                                        <br><br>
                                                        </small>
                                                    </div>

                                                    <div class="right col-xs-5">
                                                        @if(empty($row->image))
                                                        <img src="{{ asset('/images/pets/no-image.jpg') }}" alt="" class="img-circle img-responsive" width="90" style="min-height:80px; max-height:83px; min-width:75px;">
                                                        @else
                                                        <img src="{{ asset('/images/pets/'.$row->image) }}" alt="" class="img-circle img-responsive" width="90" style="min-height:80px; max-height:83px; min-width:75px;">
                                                        @endif
                                                    </div>
                                                </div>

                                                <?php $flag = FALSE; ?>
                                                @foreach($enrolled_dogs as $e)
                                                    @if ($row->id == $e->dog_id)
                                                        <?php $flag = TRUE; ?>

                                                        <p>Schedule ends on: {{ date('M d, Y', $e->end_sched) }}</p>
                                                    @endif
                                                @endforeach

                                                @if (!$flag)
                                                        <div class="item form-group end-date">
                                                            <label class="control-label col-md-6 col-sm-6 col-xs-12 " for="name">End Date <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input id="dateend{{ $row->id }}" class="date-picker form-control col-md-7 col-xs-12 end_sched" data-validate-length-range="1" data-validate-words="2" required="required" name="end_date" type="text" value="<?php echo date('m/d/Y', strtotime("+1 day", $classes['start_date'])); ?>">
                                                            </div>
                                                        </div>
                                                @endif
                                            </div>


                                            @if (empty($enrolled_dogs))
                                                <div class="col-xs-12 bottom text-center">
                                                        <button type="button" id="enrollClass{{ $row->id }}" class="btn btn-warning btn-xs pull-right enroll" data-classid="{{ $class_id }}" data-dogid="{{ $row->id }}">
                                                            <i class="fa fa-user"></i> Enroll to the Class </button>
                                                </div>
                                            @else
                                                <?php $flag = FALSE; ?>
                                                @foreach($enrolled_dogs as $e)
                                                    @if ($row->id == $e->dog_id)
                                                        <div class="col-xs-12 bottom text-center">
                                                                <button type="button" id="enrollClass{{ $row->id }}" class="btn btn-success btn-xs pull-right end-sched" disabled>
                                                                    <i class="fa fa-check"></i> Enrolled to this Class </button>
                                                        </div>
                                                        <?php $flag = TRUE; ?>
                                                    @endif
                                                @endforeach

                                                @if (!$flag)
                                                    <div class="col-xs-12 bottom text-center">
                                                            <button type="button" id="enrollClass{{ $row->id }}" class="btn btn-warning btn-xs pull-right enroll" data-classid="{{ $class_id }}" data-dogid="{{ $row->id }}">
                                                                <i class="fa fa-user"></i> Enroll to the Class </button>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                        <div class="col-xs-12">
                            <hr>
                            <a href="/classes" class="btn btn-primary">Cancel</a>
                            <a href="/calendar" class="btn btn-success pull-right"><i class="fa fa-check-square-o"></i> OK, I'm done</a>
                        </div>
                    </div><!--/x_content-->
                </div><!--/x_panel-->
            </div>
        </form>
    </div><!--/.row-->
</div><!--/""-->
@stop


@section('additional_script')

<!-- Timepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function () {

        initCalendar();

    });

    function initCalendar(){
        $('.date-picker').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    }




/**Enroll Dogs Button**/
$("body").on("click", ".enroll", function(e){
        e.preventDefault();
        var token = "{{ csrf_token() }}";
        var class_id = $(this).attr("data-classid");
        var dog_id = $(this).attr("data-dogid");
        var id_name = "#dateend" + dog_id;

        $.ajax({
            method: "POST",
            url: "/dogs/ajax-enroll-dog",
            dataType: "json",
            data: {
                class_id: class_id,
                dog_id: dog_id,
                end_sched: $(id_name).val(),
                end_date: "{{ date('m/d/Y', $classes['end_date']) }}",
                start_date: "{{ date('m/d/Y', $classes['start_date']) }}",
                _token: token
            }
        })
        .done(function(result) {
            if (result == false) {
                alert("Error! Please specify a date between the start and end date of this class.");

            } else {

                var id = "#enrollClass" + dog_id;
                var id_name = "#dateend" + dog_id;

                $(id).removeClass("btn-warning");
                $(id).addClass("btn-success");
                $(id).attr("disabled", true);
                $(id_name).attr("disabled", true);
                $(id).html('<i class="fa fa-check"></i> Enrolled to this Class');
            }
        });
    });

    $(document).ready(function () {
        /** Search Box **/
        $('#search-input').keyup(function(e) {
            e.preventDefault();
            var token = "{{ csrf_token() }}";
            var class_id = "{{ $class_id }}";

            $.ajax({
                method: "POST",
                url: "/dogs/ajax-search-dog",
                dataType: "json",
                data: {
                    keyword: $('#search-input').val(),
                    class_id: class_id,
                    _token: token
                }
            })
            .done(function(result) {
                var html_string = "";
                var customers = result['customers'];
                var enrolled_dogs = result['enrolled_dogs'];
                var cal_id  = "";

                $('.dogs-block').html(html_string);
                $.each(result['dogs'], function(index, value) {

                    html_string += '<div data-filter="' + value['name'] + '" class="col-md-6 col-sm-6 col-xs-12 animated fadeInDown dogs-block">';
                        html_string += '<div class="well profile_view">';
                            html_string += '<div class="col-sm-12">';
                                html_string += '<h2><a href="/dogs/view/' + value['id'] + '">' + value['name'] + '</a></h2>';

                                html_string += '<div class="row">';
                                    html_string += '<div class="left col-xs-7 left-dog-enroll">';
                                        html_string += '<small>';
                                        html_string+='<p class="dog-owner-name"><strong>Owner: </strong><a href="/customers/view/' + value['customer_id'] + '" style="color: #16418A;">' + customers[value['customer_id']] + '</a></p>';
                                        html_string += '<p><strong>Breed: </strong>' + value['breed'] + '</a></p>';
                                        html_string += '<p><strong>Age: </strong>' + value['age'] + '</a></p>';
                                        html_string += '<br><br>';
                                        html_string += '</small>';
                                    html_string += '</div>';
                                    

                                    html_string += '<div class="right col-xs-5">';
                                        if (value['image'] == "") {
                                            html_string += '<img src="/images/pets/no-image.jpg" alt="" class="img-circle img-responsive" width="90" style="min-height:80px; max-height:83px; min-width:75px;">';
                                        } else {
                                            html_string += '<img src="/images/pets/' + value['image'] + '" alt="" class="img-circle img-responsive" width="90" style="min-height:80px; max-height:83px; min-width:75px;">';
                                        }
                                    html_string += '</div>';
                                html_string += '</div>';

                                if (enrolled_dogs == "") {
                                    html_string += '<div class="item form-group end-date">';
                                        html_string += '<label class="control-label col-md-6 col-sm-6 col-xs-12 " for="name">End Date <span class="required">*</span></label>';
                                        html_string += '<div class="col-md-6 col-sm-6 col-xs-12">';
                                            dateval = "{{ date('m/d/Y', strtotime('+1 day', $classes['start_date'])) }}";
                                            html_string += '<input id="dateend' + value['id'] + '" class="date-picker form-control col-md-7 col-xs-12 end_sched" data-validate-length-range="1" data-validate-words="2" required="required" name="end_date" type="text" value="'+ dateval +'">';
                                        html_string += '</div>';
                                    html_string += '</div>';
                                } else {


                                    $.each(enrolled_dogs, function(i, val) {
                                        if (val['id'] == value['id']) {
                                            var d = new Date(val["end_sched"] * 1000);

                                            var dateval = d.getMonth() + 1 + '/' + d.getDate() + '/' + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                                            html_string += '<p>Schedule ends on: ' + dateval + '</p>';
                                        } else {
                                            html_string += '<div class="item form-group end-date">';
                                                html_string += '<label class="control-label col-md-6 col-sm-6 col-xs-12 " for="name">End Date <span class="required">*</span></label>';
                                                html_string += '<div class="col-md-6 col-sm-6 col-xs-12">';
                                                    dateval = "{{ date('m/d/Y', strtotime('+1 day', $classes['start_date'])) }}";
                                                    html_string += '<input id="dateend' + value['id'] + '" class="date-picker form-control col-md-7 col-xs-12 end_sched" data-validate-length-range="1" data-validate-words="2" required="required" name="end_date" type="text" value="'+ dateval +'">';
                                                html_string += '</div>';
                                            html_string += '</div>';
                                        }
                                    });
                                }
                            html_string += '</div>';

                            html_string += '<div class="col-xs-12 bottom text-center">';
                                html_string += '<button type="button" id="enrollClass' + value['id'] + '" class="btn btn-warning btn-xs pull-right enroll" data-classid="{{ $class_id }}" data-dogid="' + value['id'] + '">';
                                if (enrolled_dogs == "") {
                                    html_string += '<i class="fa fa-user"></i> Enroll to the Class </button>';
                                } else {
                                    $.each(enrolled_dogs, function(i, val) {
                                        if (val['id'] == value['id']) {
                                            html_string += '<i class="fa fa-check"></i> Enrolled to this Class </button>';
                                        } else {
                                            html_string += '<i class="fa fa-user"></i> Enroll to the Class </button>';
                                        }
                                    });

                                }
                            html_string += '</div>';

                        html_string += '</div>';
                    html_string += '</div>';
                    

                });

                $('.list-dogs').html(html_string);
                initCalendar();
            });

            // var sval = $(this).val().toUpperCase();
            // var regexp = new RegExp(sval, 'i');
            // var dogsBlock = document.getElementsByClassName('dogs-block');
            // for (var i = 0; i < dogsBlock.length; i++) {
            //     var dogname = dogsBlock[i].getElementsByTagName("h2")[0].innerHTML;
            //     var ownername = dogsBlock[i].getElementsByClassName("dog-owner-name")[0].innerHTML;
            //     if ((dogname.toUpperCase().search(regexp) != -1) || (ownername.toUpperCase().search(regexp) != -1))    
            //         dogsBlock[i].style.display = 'block';
            //     else
            //         dogsBlock[i].style.display = 'none';
            // }
        });
    });
</script>



@stop
