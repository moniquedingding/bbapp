@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if($for_all_view['current_usertype'] != "1")
            <div class="alert alert-danger" role="alert">
                You are not allowed to access this page.
            </div>
        @else
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    @if (isset($message))
                        <div class="alert alert-danger" role="alert">
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            {{ $message }}
                        </div>
                    @endif
                    <form id="user-add-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="/users/add" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="firstname" name="firstname" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="lastname" name="lastname" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="address" name="address" required="required" class="form-control col-md-7 col-xs-12"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Telephone Number
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="phone" name="phone" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile Number <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mobile" name="mobile" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usertype">User Type <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="usertype" name="usertype" class="form-control">
                                  <option value="1">Admin</option>
                                  <option value="2">Manager</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="centre-group" style="display:none;">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="centre">Centre <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                @foreach($centres as $row)
                                    <input type="radio" id="centre" name="centre" value="{{ $row->id }}" <?php if($row->id == "1") echo "checked"; ?> > {{ $row->name }} &nbsp;&nbsp;
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Image
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" name="image" id="image" onchange="imageUpload()" accept="image/*" class="hidden">
                                <label for="image" class="btn btn-default">Choose File</label>
                                <span id="imageName"></span>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a id="cancel" href="/users" class="btn btn-primary">Cancel</a>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form><!--/#user-add-form-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        @endif
        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#user-add-form .btn').on('click', function () {
        $('#user-add-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#user-add-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});

//image upload
function imageUpload(){
    var valName = document.getElementById("image").files[0].name;
    $('#imageName').html(valName);
}

//Usertype on Change
$('#usertype').on('change', function(){
    if($(this).val()==="2"){
        $("#centre-group").fadeIn();
    } else {
        $("#centre-group").fadeOut();
    }
});

$('#cancel').on('click', function(){
    $('#user-add-form').hide();
});
</script>
@stop