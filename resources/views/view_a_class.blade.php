
@extends('admin.admin_layout')
@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Class Information <small><a href="/classes">Go back to class list</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{{ $class['title'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Description</strong></td>
                                <td>{{ $class['description'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Duration</strong></td>
                                <td>
                                    <?php
                                        $hrs = floor($class['duration']/60);
                                        $mins = $class['duration'] % 60;

                                        if ($mins <= 0) 
                                            echo $hrs." hour/s";
                                        else echo $hrs." hour/s ".$mins." minute/s";
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Schedule</strong></td>

                                <td>
                                <?php
                                    $str = explode(",", $class['frequency']);

                                    foreach($str as $s) {
                                        echo ucfirst(substr($s, 0, 1));
                                    }

                                    echo "<br>";

                                ?>

                                @if (date("M j, Y", $class['start_date']) == date("M j, Y", $class['end_date']))
                                    {{ date("M j, Y", $class['start_date']) }}
                                @else
                                    {{ date("M j, Y", $class['start_date']) }} to {{ date("M j, Y", $class['end_date']) }}
                                @endif
                                    <br>
                                    {{ date("h:i A", $class['time_start']) }} to {{ date("h:i A", strtotime("+ ".$class['duration']." minutes", $class['time_start'])) }}
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Centre</strong></td>
                                <td>
                                    @foreach($centres as $row)
                                        {{ ucfirst($row->name) }}
                                    @endforeach
                                </td>
                            </tr>
                        
                        </tbody>

                    </table>
                </div>

                <div class="x_title">
                    <h2>Dogs Enrolled <small><a href="#">Go back to top</a></small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-highlight-links">
                        <tbody>
                            @foreach($enrolled_dogs as $row)
                            <tr>
                                <td>
                                @if(empty($row->image))
                                    <img src="{{ asset('/images/pets/no-image.jpg') }}" alt="" class="img-circle img-responsive" width="90" style="min-height:80px; max-height:83px; min-width:75px;">
                                @else
                                    <img src="{{ asset('/images/pets/'.$row->image) }}" alt="" class="img-circle img-responsive" width="90" style="min-height:80px; max-height:83px; min-width:75px;">
                                @endif
                                </td>
                                <td width="5%">
                                    <p><strong>Name</strong>
                                    <p><strong>Age</strong>
                                    <p><strong>Breed</strong>
                                    <p><strong>Color</strong>
                                </td>
                                <td>
                                    <p><a href="/dogs/view/{{ $row->id }}">{{ ucwords($row->name) }}</a></p>
                                    <p>{{ strtolower($row->age) }} </p>
                                    <p>{{ ucwords($row->breed) }} </p>
                                    <p>{{ ucfirst($row->color) }} </p>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

    </div>
</div>
@stop

@section('additional_script')


@stop