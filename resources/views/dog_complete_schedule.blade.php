@extends('admin.admin_layout')

@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>{{ $title }}</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Scheduled Dates <a href="/dogs"><small>Back to list</small></a></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    @if (isset($message))
                        @if (isset($is_delete)) 
                            <div class="alert alert-danger" role="alert">
                        @else
                            <div class="alert alert-success" role="alert">
                        @endif

                            {{ $message }}
                            <br><a href="/"><strong>Go back to home</strong></a>
                        </div>
                    @endif
                    <div>
                        <table class="table">
                            <tr>
                            <td><strong>Title</strong></td>
                            <td><a href="/classes/view/{{ $schedule[0]->id }}">{{ $schedule[0]->title }}</a></td>
                            </tr>
                            <tr>
                            <td><strong>Description</strong></td>
                            <td>{{ $schedule[0]->description }}</td>
                            </tr>
                            <tr>
                            <td><strong>Time Schedule</strong></td>
                            <td>
                                <p></p>
                                <p>{{ date("h:i A", $schedule[0]->time_start) }} to {{ date("h:i A", strtotime("+".$schedule[0]->duration." minutes", $schedule[0]->time_start)) }}</p>
                            </td>
                            </tr>
                        </table>
                    </div>

                    <table class="table table-striped table-highlight-link">
                        <thead>
                            <tr>
                                <th>#</th>
                                <!-- <th>Title</th> -->
                                <th>Class Sessions</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                if (isset($timestamp)) {
                                    $start_date = $timestamp;
                                } else {
                                    $start_date = $schedule[0]->start_date;
                                }

                                $count = 0;

                                if ($schedule[0]->frequency == "ALL") {
                                    $frequency = "monday,tuesday,wednesday,thursday,friday,";
                                } else {
                                    $frequency = $schedule[0]->frequency;
                                }
                            ?>

                            @while(($start_date <= $schedule[0]->end_date) && ($count < $limit))
                                <?php 
                                    $temp = strtolower(date('l', $start_date));
                                    $temp2 = explode(",", $frequency);

                                    $res = array_search($temp, $temp2);
                                ?>

                                @if (is_numeric($res) && $res >= 0)
                                    <?php 
                                        $flag = TRUE;

                                        foreach($dogs_days_cancelled as $dc) {
                                            if (date("l, M j, Y", $dc->schedule) == date("l, M j, Y", $start_date)) {
                                                $flag = FALSE;
                                            }

                                            if (!$flag) {
                                                break;
                                            }
                                        }  

                                    ?>

                                    @if ($flag)
                                    <tr>
                                        @if (isset($page_count))
                                            <td>{{ ++$page_count }}</td>
                                            <?php ++$count; ?>
                                        @else
                                            <td>{{ ++$count }}</td>
                                        @endif
                                        <!-- <td></td> -->
                                        <td>
                                            <p>{{ date("l, M j, Y", $start_date) }}</p>
                                        </td>

                                        @if ($status == "new")
                                        <td>
                                            <?php
                                                $temp = date("l, M j, Y", $start_date);
                                                $temp2 = date("h:i A", $schedule[0]->time_start);
                                                $new_date = strtotime($temp." ".$temp2);
                                            ?>
                                            <a href="/dogs/remove-selected/{{ $dog_id }}/{{ $class_id }}/{{ $new_date }}">Cancel schedule</a>
                                        </td>
                                        @else
                                        <td>

                                            <?php
                                                $temp = date("l, M j, Y", $start_date);
                                                $temp2 = date("h:i A", $schedule[0]->time_start);
                                                $new_date = strtotime($temp." ".$temp2);
                                                

                                                if (!empty($dogs_days_absent)) {
                                                    $temp3 = array_search($new_date, $dogs_days_absent);
                                                } else $temp3 = NULL;

                                            ?>

                                            @if ($temp3 == NULL)
                                                <a href="/dogs/mark-as-absent/{{ $dog_id }}/{{ $class_id }}/{{ $new_date }}" style="color: #D9534F">Mark as absent</a>
                                            @else
                                                <span>Absent</span>
                                            @endif

                                        </td>
                                        @endif
                                    </tr>
                                    @endif
                                @endif
                                
                                <?php
                                    $start_date = strtotime("+1 day", $start_date);
                                ?>
                            @endwhile

                        </tbody>

                    </table>



                    @if (isset($timestamp))
                    <?php Session::put('count', 0); ?>
                    <div class="pull-left">
                        <a href="/dogs/complete-schedule/{{ $dog_id }}/{{ $class_id }}/" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> View previous schedules</a>
                    </div>
                    @endif

                    @if (($count >= $limit) && ($start_date <= $schedule[0]->end_date))
                    <?php Session::put('count', $count); ?>
                    <div class="pull-right">
                        <a href="/dogs/complete-schedule/{{ $dog_id }}/{{ $class_id }}/{{ $start_date }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i> View next schedules</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

    </div>
</div>

@stop

@section('additional_script')
        
@stop