
@extends('admin.admin_layout')
@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                View Customer Profile
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Contact Information <small><a href="/customers">Go back to customers list</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{{ ucfirst($customer['firstname'])." ".ucfirst($customer['lastname']) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Centre</strong></td>
                                @foreach ($centres as $c)
                                    @if ($c->id == $customer['centre_id'])
                                        <td class=" ">{{ ucfirst($c->name) }}</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <td><strong>Manager in-charge</td>
                                @foreach ($manager as $m)
                                    @if ($m->id == $customer['manager_id'])
                                        <td class=" ">{{ ucfirst($m->firstname)." ".ucfirst($m->lastname) }}</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <td><strong>Address</strong></td>
                                <td>{{ $customer['address_1']." ".$customer['address_2'].", ".$customer['city']." ".$customer['state']." ".$customer['Country'].", ".$customer['postcode'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Phone</strong></td>
                                <td>{{ $customer['phone'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Mobile</strong></td>
                                <td>{{ $customer['mobile'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Fax</strong></td>
                                <td>{{ $customer['fax'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>E-mail Address</strong></td>
                                <td>{{ $customer['email'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Occupation</strong></td>
                                <td>{{ $customer['occupation'] }}</td>
                            </tr>
                            <tr>
                                <td><strong>Notes</strong></td>
                                <td>{{ $customer['notes'] }}</td>
                            </tr>
                            <tr>
                                <td><strong># of dogs enrolled</strong></td>

                                <?php $num_dogs = count($dogs); ?>
                                <td>{{ count($dogs) }}</td>
                            </tr>

                            @if ($num_dogs > 0)
                            <tr>
                                <td><strong>Dogs Enrolled</strong></td>

                                <td>
                                @foreach($dogs as $d)
                                    {{ $d->name }} <a href="/dogs/view/{{ $d->id }}"><span class="label label-info">View Profile</span></a> <br>
                                @endforeach
                                </td>
                            </tr>
                            @endif
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

    </div>
</div>
@stop

@section('additional_script')
        <!-- Datatables -->
        <script src="{{ asset('js/datatables/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('js/datatables/tools/js/dataTables.tableTools.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

            var asInitVals = new Array();
            $(document).ready(function () {
                var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 12,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip',
                    "tableTools": {
                        "sSwfPath": "<?php echo URL::to('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                    }
                });
                $("tfoot input").keyup(function () {
                    /* Filter on the column based on the index of this element's parent <th> */
                    oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
                });
                $("tfoot input").each(function (i) {
                    asInitVals[i] = this.value;
                });
                $("tfoot input").focus(function () {
                    if (this.className == "search_init") {
                        this.className = "";
                        this.value = "";
                    }
                });
                $("tfoot input").blur(function (i) {
                    if (this.value == "") {
                        this.className = "search_init";
                        this.value = asInitVals[$("tfoot input").index(this)];
                    }
                });
            });
        </script>

@stop