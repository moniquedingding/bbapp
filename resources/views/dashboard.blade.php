@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
 <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
 <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
 <style type="text/css">

 a.clear-anchor-style {
     text-decoration: none;
 }

 .danger-color {
     color: #F56954 !important;
 }

 .success-color {
     color: #26B99A !important;
 }

 .warning-color {
     color: #F0AD4E !important;
 }

 .info-color {
     color: #5BC0DE !important;
 }

 .smaller-font {
     font-size: 10px;
 }

 .centre-name {
    font-weight: bold;
 }

 .pantone-yellow {
    background-color: #FBDC43;
 }

 .pantone-pms122 {
    background-color: #FCED6E;
 }

 </style>
@stop

@section('content')
                <div class="">

                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-sort-amount-desc danger-color"></i>
                                </div>
                                <div class="count">{{ $count_invoices }}</div>

                                <a href="/invoice/view-unpaid" class="clear-anchor-style">
                                    <span class="danger-color">
                                        <hr>
                                        <h3 class="danger-color">Unpaid Invoices</h3>
                                        <p><span class="label label-danger">VIEW INVOICES</span></p>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-calendar info-color"></i>
                                </div>
                                <div class="count">{{ $count_classes }}</div>

                                <a href="/calendar" class="clear-anchor-style">
                                <span class="info-color">
                                    <hr>
                                    <h3 class="info-color">Calendar</h3>
                                    <p><span class="label label-info">VIEW CALENDAR</span></p>
                                </span>
                                </a>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users warning-color"></i>
                                </div>
                                <div class="count">{{ $count_customers }}</div>

                                <a href="/customers" class="clear-anchor-style">
                                <span class="warning-color">
                                    <hr>
                                    <h3 class="warning-color">Customers</h3>
                                    <p><span class="label label-warning">VIEW CUSTOMERS</span></p>
                                </span>
                                </a>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-paw success-color"></i>
                                </div>
                                <div class="count">{{ $count_dogs }}</div>

                                <a href="dogs" class="clear-anchor-style">
                                <span class="success-color">
                                    <hr>
                                    <h3 class="success-color">Dogs</h3>
                                    <p><span class="label label-success">VIEW DOGS</span></p>
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>

                    @if ($current_centre == 0)

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Monthly Progress &nbsp;&nbsp; {{ date('Y') }} <small><strong>x = month, y = revenue in £  </strong></small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-4">
                                        <center>
                                            <canvas id="canvas1"></canvas>
                                            <h4>Hythe Centre</h4>
                                        </center>
                                    </div>
                                    <div class="col-md-4">
                                        <center>
                                            <canvas id="canvas2"></canvas>
                                            <h4>Sevenoaks Centre</h4>
                                        </center>
                                    </div>
                                    <div class="col-md-4">
                                        <center>
                                            <canvas id="canvas3"></canvas>
                                            <h4>Tonbridge Centre</h4>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Invoice Report <small>Monthly Progress</small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-4 tile">
                                            
                                            <style type="text/css">
                                                .total {
                                                    font-weight: bold;
                                                }
                                            </style>

                                            <?php
                                                $temp = date('m')-2;
                                                $prev_month_first = strtotime(date('Y')."-".$temp."-01");
                                                $prev_month_last = strtotime(date('Y-m-t', $prev_month_first));
                                            ?>
                                            <h2 class="warning-color">{{ date('M j', $prev_month_first) }} - {{ date('M j', $prev_month_last) }}, {{ date('Y') }}</h2>
                                            <table class="table table-bordered table-striped smaller-font">
                                                <thead class="pantone-yellow">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Centre</th>
                                                        <th>Revenue</th>
                                                        <th>Rate</th>
                                                        <th>Commission</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $count = 0;
                                                        $amount_total = 0;
                                                        $comm_total = 0;
                                                    ?>

                                                    @if (empty($receipts_prev2))
                                                        @foreach($centres as $c)
                                                        <tr>
                                                            <td>{{ ++$count }}</td>
                                                            <td class="centre-name">{{ ucfirst($c->name) }}</td>
                                                            <td>£ 0.00</td>
                                                            <td>{{ $c->rate }}%</td>
                                                            <td>£ 0.00</td>
                                                        </tr>
                                                        @endforeach
                                                    @else

                                                    <?php
                                                        $data[1]['revenue'] = 0;
                                                        $data[2]['revenue'] = 0;
                                                        $data[3]['revenue'] = 0;
                                                        $data[1]['commission'] = 0;
                                                        $data[2]['commission'] = 0;
                                                        $data[3]['commission'] = 0;
                                                        $data[1]['count'] = 0;
                                                        $data[2]['count'] = 0;
                                                        $data[3]['count'] = 0;
                                                    ?>

                                                    @foreach($receipts_prev2 as $r)

                                                        @foreach($centres as $c)

                                                            @if ($c->id == $r->centre_id)
                                                            <?php

                                                                $data[$c->id]['name'] = ucfirst($c->name);
                                                                $data[$c->id]['revenue'] += $r->revenue;
                                                                $data[$c->id]['rate'] = $c->rate;
                                                                $data[$c->id]['commission'] += $r->commission;
                                                                $data[$c->id]['count'] += 1;
                                                            ?>

                                                            @else 
                                                            <?php
                                                                $data[$c->id]['name'] = ucfirst($c->name);
                                                                $data[$c->id]['revenue'] = 0;
                                                                $data[$c->id]['rate'] = $c->rate;
                                                                $data[$c->id]['commission'] += 0;
                                                            ?>
                                                            @endif                                     

                                                        @endforeach

                                                            <?php
                                                                $amount_total += $r->revenue;
                                                                $comm_total += $r->commission;
                                                            ?>
                                                    @endforeach

                                                    @foreach($centres as $c)
                                                        <tr>
                                                        <td scope="row">{{ ++$count }}</td>
                                                        <td class="centre-name">{{ $data[$c->id]['name'] }}</td>
                                                        <td>£ {{ number_format($data[$c->id]['revenue'], 2, '.', '') }}</td> 

                                                        @if ($data[$c->id]['count'] > 1)                  
                                                        <td>{{ $data[$c->id]['rate'] }}% <strong class="success-color">*</strong></td>
                                                        @else                   
                                                        <td>{{ $data[$c->id]['rate'] }}%</td>
                                                        @endif                   
                                                        <td>£ {{ number_format($data[$c->id]['commission'], 2, '.', '') }}</td>  
                                                        </tr>

                                                    @endforeach
                                                    @endif

                                                    <tr class="total pantone-pms122">
                                                        <td></td>
                                                        <td>TOTAL</td>
                                                        <td>£ {{ number_format($amount_total, 2, '.', '') }}</td>
                                                        <td></td>
                                                        <td>£ {{ number_format($comm_total, 2, '.', '') }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4 tile">
                                            <?php
                                                $temp = date('m')-1;
                                                $prev_month_first = strtotime(date('Y')."-".$temp."-01");
                                                $prev_month_last = strtotime(date('Y-m-t', $prev_month_first));
                                            ?>
                                            <h2 class="warning-color">{{ date('M j', $prev_month_first) }} - {{ date('M j', $prev_month_last) }}, {{ date('Y') }}</h2>
                                            <table class="table table-bordered table-striped smaller-font">
                                                <thead class="pantone-yellow">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Centre</th>
                                                        <th>Revenue</th>
                                                        <th>Rate</th>
                                                        <th>Commission</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                        $count = 0;
                                                        $amount_total = 0;
                                                        $comm_total = 0;
                                                    ?>

                                                    @if (empty($receipts_prev))
                                                        @foreach($centres as $c)
                                                        <tr>
                                                            <td>{{ ++$count }}</td>
                                                            <td class="centre-name">{{ ucfirst($c->name) }}</td>
                                                            <td>£ 0.00</td>
                                                            <td>{{ $c->rate }}%</td>
                                                            <td>£ 0.00</td>
                                                        </tr>
                                                        @endforeach
                                                    @else

                                                    <?php
                                                        $data[1]['revenue'] = 0;
                                                        $data[2]['revenue'] = 0;
                                                        $data[3]['revenue'] = 0;
                                                        $data[1]['commission'] = 0;
                                                        $data[2]['commission'] = 0;
                                                        $data[3]['commission'] = 0;
                                                        $data[1]['count'] = 0;
                                                        $data[2]['count'] = 0;
                                                        $data[3]['count'] = 0;
                                                    ?>

                                                    @foreach($receipts_prev as $r)

                                                        @foreach($centres as $c)

                                                            @if ($c->id == $r->centre_id)
                                                            <?php

                                                                $data[$c->id]['name'] = ucfirst($c->name);
                                                                $data[$c->id]['revenue'] += $r->revenue;
                                                                $data[$c->id]['rate'] = $c->rate;
                                                                $data[$c->id]['commission'] += $r->commission;
                                                                $data[$c->id]['count'] += 1;
                                                            ?>

                                                            @else 
                                                            <?php
                                                                $data[$c->id]['name'] = ucfirst($c->name);
                                                                $data[$c->id]['revenue'] = 0;
                                                                $data[$c->id]['rate'] = $c->rate;
                                                                $data[$c->id]['commission'] += 0;
                                                            ?>
                                                            @endif                                     

                                                        @endforeach

                                                            <?php
                                                                $amount_total += $r->revenue;
                                                                $comm_total += $r->commission;
                                                            ?>
                                                    @endforeach

                                                    @foreach($centres as $c)
                                                        <tr>
                                                        <td scope="row">{{ ++$count }}</td>
                                                        <td class="centre-name">{{ $data[$c->id]['name'] }}</td>
                                                        <td>£ {{ number_format($data[$c->id]['revenue'], 2, '.', '') }}</td> 

                                                        @if ($data[$c->id]['count'] > 1)                  
                                                        <td>{{ $data[$c->id]['rate'] }}% <strong class="success-color">*</strong></td>
                                                        @else                   
                                                        <td>{{ $data[$c->id]['rate'] }}%</td>
                                                        @endif                   
                                                        <td>£ {{ number_format($data[$c->id]['commission'], 2, '.', '') }}</td>  
                                                        </tr>

                                                    @endforeach
                                                    @endif

                                                    <tr class="total pantone-pms122">
                                                        <td></td>
                                                        <td>TOTAL</td>
                                                        <td>£ {{ number_format($amount_total, 2, '.', '') }}</td>
                                                        <td></td>
                                                        <td>£ {{ number_format($comm_total, 2, '.', '') }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4 tile">
                                            <?php
                                                $this_month_first = strtotime(date('Y')."-".date('m')."-01");
                                                $this_month_last = strtotime(date('Y-m-t', $this_month_first));
                                            ?>
                                            <h2 class="warning-color">{{ date('M j', $this_month_first) }} - {{ date('M j', $this_month_last) }}, {{ date('Y') }}</h2>
                                            <table class="table table-bordered table-striped smaller-font">
                                                <thead class="pantone-yellow">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Centre</th>
                                                        <th>Revenue</th>
                                                        <th>Rate</th>
                                                        <th>Commission</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    <?php
                                                        $count = 0;
                                                        $amount_total = 0;
                                                        $comm_total = 0;
                                                    ?>

                                                    @if (empty($receipts_this))
                                                        @foreach($centres as $c)
                                                        <tr>
                                                            <td>{{ ++$count }}</td>
                                                            <td class="centre-name">{{ ucfirst($c->name) }}</td>
                                                            <td>£ 0.00</td>
                                                            <td>{{ $c->rate }}%</td>
                                                            <td>£ 0.00</td>
                                                        </tr>
                                                        @endforeach
                                                    @else

                                                    <?php
                                                        $data[1]['revenue'] = 0;
                                                        $data[2]['revenue'] = 0;
                                                        $data[3]['revenue'] = 0;
                                                        $data[1]['commission'] = 0;
                                                        $data[2]['commission'] = 0;
                                                        $data[3]['commission'] = 0;
                                                        $data[1]['count'] = 0;
                                                        $data[2]['count'] = 0;
                                                        $data[3]['count'] = 0;
                                                    ?>

                                                    @foreach($receipts_this as $r)

                                                        @foreach($centres as $c)

                                                            @if ($c->id == $r->centre_id)
                                                            <?php
                                                                // echo $c->id." ".$data[$c->id]['revenue']."+".$r->revenue."<br>";
                                                                $data[$c->id]['name'] = ucfirst($c->name);
                                                                $data[$c->id]['revenue'] += $r->revenue;
                                                                $data[$c->id]['rate'] = $r->rate;
                                                                $data[$c->id]['commission'] += $r->commission;
                                                                $data[$c->id]['count'] += 1;
                                                            ?>

                                                            @else 
                                                            <?php
                                                                $data[$c->id]['name'] = ucfirst($c->name);
                                                                $data[$c->id]['revenue'] += 0;
                                                                $data[$c->id]['rate'] = $c->rate;
                                                                $data[$c->id]['commission'] += 0;
                                                            ?>
                                                            @endif                                     

                                                        @endforeach
                                                            <?php
                                                                $amount_total += $r->revenue;
                                                                $comm_total += $r->commission;
                                                            ?>
                                                    @endforeach

                                                    @foreach($centres as $c)
                                                        <tr>
                                                        <td scope="row">{{ ++$count }}</td>
                                                        <td class="centre-name">{{ $data[$c->id]['name'] }}</td>
                                                        <td>£ {{ number_format($data[$c->id]['revenue'], 2, '.', '') }}</td> 

                                                        @if ($data[$c->id]['count'] > 1)                  
                                                        <td>{{ $data[$c->id]['rate'] }}% <strong class="success-color">*</strong></td>
                                                        @else                   
                                                        <td>{{ $data[$c->id]['rate'] }}%</td>
                                                        @endif                   
                                                        <td>£ {{ number_format($data[$c->id]['commission'], 2, '.', '') }}</td>  
                                                        </tr>

                                                    @endforeach

                                                    @endif
                                                    <tr class="total pantone-pms122">
                                                        <td></td>
                                                        <td>TOTAL</td>
                                                        <td>£ {{ number_format($amount_total, 2, '.', '') }}</td>
                                                        <td></td>
                                                        <td>£ {{ number_format($comm_total, 2, '.', '') }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                        <hr>
                                            <h2 class="danger-color">Running Totals &nbsp; &nbsp; {{ date('Y') }}</h2>
                                            <table class="table table-striped table-bordered smaller-font">
                                                <thead style="background-color: #F35333; color: #FFF">
                                                    <th>#</th>
                                                    <th>Centre</th>
                                                    <th>Revenue</th>
                                                    <th>Rate</th>
                                                    <th>Commission</th>
                                                    <th>Amount Paid</th>
                                                    <th>Amount Outstanding</th>
                                                </thead>

                                                <?php
                                                    $data[1]['revenue'] = 0;
                                                    $data[2]['revenue'] = 0;
                                                    $data[3]['revenue'] = 0;
                                                    $data[1]['commission'] = 0;
                                                    $data[2]['commission'] = 0;
                                                    $data[3]['commission'] = 0;
                                                    $data[1]['count'] = 0;
                                                    $data[2]['count'] = 0;
                                                    $data[3]['count'] = 0;

                                                    $count = 0;

                                                    $amount_total = 0;
                                                    $comm_total = 0;


                                                    $data[1]['amount_paid'] = 0;
                                                    $data[2]['amount_paid'] = 0;
                                                    $data[3]['amount_paid'] = 0;

                                                    $data[1]['amount_outstanding'] = 0;
                                                    $data[2]['amount_outstanding'] = 0;
                                                    $data[3]['amount_outstanding'] = 0;
                                                ?>

                                                @foreach($approved_commissions as $r)
                                                    
                                                    @foreach($centres as $c)

                                                        @if ($c->id == $r->centre_id)
                                                            <?php
                                                                $data[$c->id]['amount_paid'] += $r->amount_requested;
                                                            ?> 

                                                        @endif

                                                    @endforeach
                                                @endforeach

                                                @foreach($receipts_all as $r)

                                                    @foreach($centres as $c)

                                                        @if ($c->id == $r->centre_id)
                                                        <?php
                                                            // echo $c->id." ".$data[$c->id]['revenue']."+".$r->revenue."<br>";
                                                            $data[$c->id]['name'] = ucfirst($c->name);
                                                            $data[$c->id]['revenue'] += $r->revenue;
                                                            $data[$c->id]['rate'] = $c->rate;
                                                            $data[$c->id]['commission'] += $r->commission;
                                                            
                                                            $data[$c->id]['count'] += 1;
                                                        ?>

                                                        @else 
                                                        <?php
                                                            $data[$c->id]['amount_outstanding'] += 0;
                                                            $data[$c->id]['name'] = ucfirst($c->name);
                                                            $data[$c->id]['revenue'] += 0;
                                                            $data[$c->id]['rate'] = $c->rate;
                                                            $data[$c->id]['commission'] += 0;
                                                        ?>
                                                        @endif                                     

                                                    @endforeach

                                                    <?php
                                                        $amount_total += $r->revenue;
                                                        $comm_total += $r->commission;
                                                    ?>
                                                @endforeach

                                                <?php $outstanding = 0; ?>

                                                @foreach($centres as $row)
                                                @if (isset($data[$row->id]['name']))
                                                <tr>
                                                    <td>{{ ++$count }}</td>
                                                    <td class="centre-name">{{ $data[$row->id]['name'] }}</td>
                                                    <td>£ {{ number_format($data[$row->id]['revenue'], 2, '.', '') }}</td>

                                                    <td style="white-space: nowrap;">
                                                    @if ($data[$row->id]['count'] > 1)                  
                                                        {{ $data[$row->id]['rate'] }}% <strong class="success-color">*</strong>
                                                    @else                   
                                                        {{ $data[$row->id]['rate'] }}%
                                                    @endif  
                                                    </td>

                                                    <td>£ {{ number_format($data[$row->id]['commission'], 2, '.', '') }}</td>
                                                    <td>£ {{ number_format($data[$row->id]['amount_paid'], 2, '.', '') }}</td>
                                                    <?php $temp = $data[$row->id]['amount_paid'] - $data[$row->id]['commission'] ?>

                                                    <?php
                                                        if ($temp <= 0)
                                                            $temp = 0;
                                                    ?>
                                                    <td>£ {{ number_format($temp, 2, '.', '') }}</td>

                                                    <?php $outstanding += $temp; ?>
                                                </tr>
                                                @endif
                                                @endforeach

                                                <tr style="background-color: #F9BAA9">   
                                                    <td colspan="2"><strong>{{ date('Y') }} TOTALS</strong></td>
                                                    <td>£ {{ number_format($amount_total, 2, '.', '') }}</td>
                                                    <td></td>
                                                    <td>£ {{ number_format($comm_total, 2, '.', '') }}</td>


                                                    <?php
                                                        $total_paid_comm = $data[1]['amount_paid'] + $data[2]['amount_paid'] +$data[3]['amount_paid'];
                                                    ?>
                                                    <td>£ {{ number_format($total_paid_comm, 2, '.', '') }}</td>

                                                    <?php
                                                        if ($outstanding <= 0)
                                                            $outstanding = 0;
                                                    ?>
                                                    <td>£ {{ number_format($outstanding, 2, '.', '') }}</td>
                                                </tr>

                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                        <hr>
                                            <h2 style="color: #00D465">Latest Payments <small><a href="/commissions/view-approved"> View Approved Commissions </a></small></h2>
                                            <table class="table table-striped smaller-font">
                                                <thead style="background-color: #00D465; color: #FFF">
                                                    <th>#</th>
                                                    <th>Centre</th>
                                                    <th>Last Payments</th>
                                                    <th>Paid By</th>
                                                    <th>Date</th>
                                                    <th>Method</th>
                                                    <th>Approved</th>
                                                </thead>
                                                <?php $count = 0; ?>
                                                @foreach($current_commissions as $row)
                                                <tr>
                                                    <td>{{ ++$count }}</td>
                                                    @foreach($centres as $cen)
                                                        @if ($cen->id == $row->centre_id)
                                                            <td><strong>{{ ucfirst($cen->name) }}</strong></td>        
                                                            <td>£ {{ number_format($row->amount_requested, 2, '.', '') }}</td>
                                                            <td>{{ $row->paid_by }}</td>
                                                            <td>{{ date('M d, Y', $row->date_requested) }}</td>
                                                            <td>{{ $row->method }}</td>

                                                            @if ($row->approved == "no")
                                                            <td style="width: 20%"><a href="/commissions/approve/{{ $row->id }}/{{ $cen->name }}" class="btn btn-info btn-sm" style="width: 100%">No, approve this payment</a></td>
                                                            @else
                                                            <td>Approved</td>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if ($current_centre > 0)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Finance <small>Monthly Report</small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        @if (isset($message))
                                        <div class="alert alert-success" role="alert"><i class="fa fa-check"></i> <strong>Success!</strong> {{ $message }} </div>
                                        @endif

                                        <div class="col-md-4 tile">
                                            
                                            <style type="text/css">
                                                .total {
                                                    font-weight: bold;
                                                }
                                            </style>

                                            <?php
                                                $temp = date('m')-2;
                                                $prev_month_first = strtotime(date('Y')."-".$temp."-01");
                                                $prev_month_last = strtotime(date('Y-m-t', $prev_month_first));
                                            ?>
                                            <h2 class="warning-color">{{ date('M j', $prev_month_first) }} - {{ date('M j', $prev_month_last) }}, {{ date('Y') }}</h2>
                                            <table class="table table-bordered table-striped smaller-font">
                                                <thead class="pantone-yellow">
                                                    <tr>
                                                        <th>Revenue</th>
                                                        <th>Rate</th>
                                                        <th>Commission</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                        $count = 0;
                                                        $amount_total = 0;
                                                        $comm_total = 0;
                                                    ?>


                                                    @if (empty($receipts_prev2))
                                                        <tr>
                                                            <td>£ 0.00</td>
                                                            <td>£ 0.00</td>
                                                            <td>£ 0.00</td>
                                                        </tr>
                                                    @else
                                                        <?php
                                                            $data[1]['revenue'] = 0;
                                                            $data[2]['revenue'] = 0;
                                                            $data[3]['revenue'] = 0;
                                                            $data[1]['commission'] = 0;
                                                            $data[2]['commission'] = 0;
                                                            $data[3]['commission'] = 0;
                                                            $data[1]['count'] = 0;
                                                            $data[2]['count'] = 0;
                                                            $data[3]['count'] = 0;
                                                        ?>

                                                        @foreach($receipts_prev as $r)

                                                            @foreach($centres as $c)

                                                                @if ($c->id == $r->centre_id)
                                                                <?php

                                                                    $data[$c->id]['name'] = ucfirst($c->name);
                                                                    $data[$c->id]['revenue'] += $r->revenue;
                                                                    $data[$c->id]['rate'] = $c->rate;
                                                                    $data[$c->id]['commission'] += $r->commission;
                                                                    $data[$c->id]['count'] += 1;
                                                                ?>

                                                                @else 
                                                                <?php
                                                                    $data[$c->id]['name'] = ucfirst($c->name);
                                                                    $data[$c->id]['revenue'] += 0;
                                                                    $data[$c->id]['rate'] = $c->rate;
                                                                    $data[$c->id]['commission'] += 0;
                                                                
                                                                    $amount_total += $r->revenue;
                                                                    $comm_total += $r->commission;
                                                                ?>
                                                                @endif                                     

                                                            @endforeach

                                                        @endforeach

                                                        @foreach($centres as $c)
                                                            @if ($c->id == $current_centre)
                                                                <tr>
                                                                
                                                                <td>£ {{ number_format($data[$c->id]['revenue'], 2, '.', '') }}</td> 

                                                                @if ($data[$c->id]['count'] > 1)                  
                                                                <td>{{ $data[$c->id]['rate'] }}% <strong class="success-color">*</strong></td>
                                                                @else                   
                                                                <td>{{ $data[$c->id]['rate'] }}%</td>
                                                                @endif                   
                                                                <td>£ {{ number_format($data[$c->id]['commission'], 2, '.', '') }}</td>  
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4 tile">
                                            <?php
                                                $temp = date('m')-1;
                                                $this_month_first = strtotime(date('Y')."-".$temp."-01");
                                                $this_month_last = strtotime(date('Y-m-t', $this_month_first));
                                            ?>
                                            <h2 class="warning-color">{{ date('M j', $this_month_first) }} - {{ date('M j', $this_month_last) }}, {{ date('Y') }}</h2>
                                            <table class="table table-bordered table-striped smaller-font">
                                                <thead class="pantone-yellow">
                                                    <tr>
                                                        <th>Revenue</th>
                                                        <th>Rate</th>
                                                        <th>Commission</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $count = 0;
                                                        $amount_total = 0;
                                                        $comm_total = 0;
                                                    ?>


                                                    @if (empty($receipts_prev2))
                                                        <tr>
                                                            <td>£ 0.00</td>
                                                            <td>£ 0.00</td>
                                                            <td>£ 0.00</td>
                                                        </tr>
                                                    @else
                                                        <?php
                                                            $data[1]['revenue'] = 0;
                                                            $data[2]['revenue'] = 0;
                                                            $data[3]['revenue'] = 0;
                                                            $data[1]['commission'] = 0;
                                                            $data[2]['commission'] = 0;
                                                            $data[3]['commission'] = 0;
                                                            $data[1]['count'] = 0;
                                                            $data[2]['count'] = 0;
                                                            $data[3]['count'] = 0;
                                                        ?>

                                                        @foreach($receipts_prev as $r)

                                                            @foreach($centres as $c)

                                                                @if ($c->id == $r->centre_id)
                                                                <?php

                                                                    $data[$c->id]['name'] = ucfirst($c->name);
                                                                    $data[$c->id]['revenue'] += $r->revenue;
                                                                    $data[$c->id]['rate'] = $c->rate;
                                                                    $data[$c->id]['commission'] += $r->commission;
                                                                    $data[$c->id]['count'] += 1;
                                                                ?>

                                                                @else 
                                                                <?php
                                                                    $data[$c->id]['name'] = ucfirst($c->name);
                                                                    $data[$c->id]['revenue'] += 0;
                                                                    $data[$c->id]['rate'] = $c->rate;
                                                                    $data[$c->id]['commission'] += 0;
                                                                
                                                                    $amount_total += $r->revenue;
                                                                    $comm_total += $r->commission;
                                                                ?>
                                                                @endif                                     

                                                            @endforeach

                                                        @endforeach

                                                        @foreach($centres as $c)
                                                            @if ($c->id == $current_centre)
                                                                <tr>
                                                                
                                                                <td>£ {{ number_format($data[$c->id]['revenue'], 2, '.', '') }}</td> 

                                                                @if ($data[$c->id]['count'] > 1)                  
                                                                <td>{{ $data[$c->id]['rate'] }}% <strong class="success-color">*</strong></td>
                                                                @else                   
                                                                <td>{{ $data[$c->id]['rate'] }}%</td>
                                                                @endif                   
                                                                <td>£ {{ number_format($data[$c->id]['commission'], 2, '.', '') }}</td>  
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-4 tile">
                                            <?php
                                                $temp = date('m');
                                                $next_month_first = strtotime(date('Y')."-".$temp."-01");
                                                $next_month_last = strtotime(date('Y-m-t', $next_month_first));
                                            ?>
                                            <h2 class="warning-color">{{ date('M j', $next_month_first) }} - {{ date('M j', $next_month_last) }}, {{ date('Y') }}</h2>
                                            <table class="table table-bordered table-striped smaller-font">
                                                <thead class="pantone-yellow">
                                                    <tr>
                                                        <th>Revenue</th>
                                                        <th>Rate</th>
                                                        <th>Commission</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $count = 0;
                                                        $amount_total = 0;
                                                        $comm_total = 0;
                                                    ?>


                                                    @if (empty($receipts_this))
                                                        <tr>
                                                            <td>£ 0.00</td>
                                                            <td>£ 0.00</td>
                                                            <td>£ 0.00</td>
                                                        </tr>
                                                    @else
                                                        <?php
                                                            $data[1]['revenue'] = 0;
                                                            $data[2]['revenue'] = 0;
                                                            $data[3]['revenue'] = 0;
                                                            $data[1]['commission'] = 0;
                                                            $data[2]['commission'] = 0;
                                                            $data[3]['commission'] = 0;
                                                            $data[1]['count'] = 0;
                                                            $data[2]['count'] = 0;
                                                            $data[3]['count'] = 0;
                                                        ?>

                                                        @foreach($receipts_this as $r)

                                                            @foreach($centres as $c)

                                                                @if ($c->id == $r->centre_id)
                                                                <?php

                                                                    $data[$c->id]['name'] = ucfirst($c->name);
                                                                    $data[$c->id]['revenue'] += $r->revenue;
                                                                    $data[$c->id]['rate'] = $c->rate;
                                                                    $data[$c->id]['commission'] += $r->commission;
                                                                    $data[$c->id]['count'] += 1;
                                                                ?>

                                                                @else 
                                                                    <?php
                                                                        $data[$c->id]['name'] = ucfirst($c->name);
                                                                        $data[$c->id]['revenue'] += 0;
                                                                        $data[$c->id]['rate'] = $c->rate;
                                                                        $data[$c->id]['commission'] += 0;
                                                                    
                                                                        $amount_total += $r->revenue;
                                                                        $comm_total += $r->commission;
                                                                    ?>
                                                                @endif                                     

                                                            @endforeach

                                                        @endforeach

                                                        @foreach($centres as $c)
                                                            @if ($c->id == $current_centre)
                                                                <tr>
                                                                
                                                                <td>£ {{ number_format($data[$c->id]['revenue'], 2, '.', '') }}</td> 

                                                                @if ($data[$c->id]['count'] > 1)                  
                                                                <td>{{ $data[$c->id]['rate'] }}% <strong class="success-color">*</strong></td>
                                                                @else                   
                                                                <td>{{ $data[$c->id]['rate'] }}%</td>
                                                                @endif                   
                                                                <td>£ {{ number_format($data[$c->id]['commission'], 2, '.', '') }}</td>  
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                        <hr>
                                            <h2 class="danger-color">Running Totals &nbsp; &nbsp; {{ date('Y') }}</h2>
                                            <table class="table table-striped table-bordered smaller-font">
                                                <thead style="background-color: #F35333; color: #FFF">
                                                    <th>Revenue</th>
                                                    <th>Rate</th>
                                                    <th>Commission</th>
                                                    <th>Amount Paid</th>
                                                    <th>Amount Outstanding</th>
                                                </thead>

                                                <?php
                                                    $data[1]['revenue'] = 0;
                                                    $data[2]['revenue'] = 0;
                                                    $data[3]['revenue'] = 0;
                                                    $data[1]['commission'] = 0;
                                                    $data[2]['commission'] = 0;
                                                    $data[3]['commission'] = 0;
                                                    $data[1]['count'] = 0;
                                                    $data[2]['count'] = 0;
                                                    $data[3]['count'] = 0;
                                                ?>

                                                @foreach($receipts_all as $r)

                                                    @if ($r->centre_id == $current_centre)
                                                    <?php
                                                        // echo $c->id." ".$data[$c->id]['revenue']."+".$r->revenue."<br>";
                                                        $data[$current_centre]['name'] = ucfirst($c->name);
                                                        $data[$current_centre]['revenue'] += $r->revenue;
                                                        $data[$current_centre]['rate'] = $r->rate;
                                                        $data[$current_centre]['commission'] += $r->commission;
                                                        $data[$current_centre]['count'] += 1;
                                                    ?>
                                                    @endif                                     

                                                    <?php
                                                        $amount_total += $r->revenue;
                                                        $comm_total += $r->commission;
                                                    ?>
                                                @endforeach

                                                <tr>
                                                    <td>£ {{ number_format($data[$current_centre]['revenue'], 2, '.', '') }}</td>
                                                    @if (isset($data[$current_centre]['name']))
                                                    <td style="white-space: nowrap;">
                                                    @if ($data[$current_centre]['count'] > 1)                  
                                                        {{ $data[$current_centre]['rate'] }}% <strong class="success-color">*</strong>
                                                    @else                   
                                                        {{ $data[$current_centre]['rate'] }}%
                                                    @endif  
                                                    </td>
                                                    @endif  

                                                    <td>£ {{ number_format($data[$current_centre]['commission'], 2, '.', '') }}</td>
                                                    <td>£ 0.00</td>
                                                    <td>£ 0.00</td>
                                                    <td>£ 0.00</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                        <hr>

                                            <h2 style="color: #00D465">Add Payment <small><a href="/commission-history">Payment History</a></small></h2>
                                            <table class="table table-striped smaller-font">
                                                <thead style="background-color: #00D465; color: #FFF">
                                                    <th>Make a Payment</th>
                                                    <th>Paid By</th>
                                                    <th>Date</th>
                                                    <th>Method</th>
                                                    <th>Send for Approval</th>
                                                </thead>
                                                <form class="form-control" method="post" action="/commissions/add">
                                                    <tr>
                                                        <td><input type="text" name="amount_requested" class="form-control" value="" placeholder="£ 0.00" required></td>
                                                        <td><input type="text" name="paid_by" class="form-control"  value="{{ $initials }}" readonly placeholder="Enter initials here" required></td>
                                                        <td><input type="text" name="date_requested" class="form-control" value="{{ date('M d, Y') }}" readonly placeholder=""></td>
                                                        <td>
                                                            <select name="method" class="form-control">
                                                                <option value="cash">Cash</option>
                                                                <option value="BACS">BACS</option>
                                                                <option value="cheque">Cheque</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <button type="submit" class="btn btn-success" style="width: 100%">Go</button>
                                                        </td>
                                                    </tr>

                                                    @foreach($current_commissions as $row)
                                                        @if ($row->centre_id == $current_centre)
                                                            <tr>
                                                                <td>£ {{ number_format($row->amount_requested, 2, '.', '') }}</td>
                                                                <td>{{ $row->paid_by }}</td>
                                                                <td>{{ date('M d, Y', $row->date_requested) }}</td>
                                                                <td>{{ $row->method }}</td>

                                                                @if ($row->approved == "no")
                                                                <td>Sent for Approval</td>
                                                                @else
                                                                <td>Approved</td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                    <input type="hidden" name="centre_id" value="{{ $current_centre }}">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                </form>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endif
                </div>

@stop

@section('additional_script')
    <!-- daterangepicker -->
    <script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
    <!-- sparkline -->
    <script src="{{ asset('js/sparkline/jquery.sparkline.min.js') }}"></script>
        <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="{{ asset('js/excanvas.min.js') }}"></script><![endif]-->
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.pie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.orderBars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.time.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/date.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.spline.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.stack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/curvedLines.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flot/jquery.flot.resize.js') }}"></script>

    <!-- flot -->
    <script type="text/javascript">
        $(document).ready(function(){


            //define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
            var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

            //generate random number for charts
            randNum = function () {
                return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
            }

            $(function () {
                var d1 = [];
                //var d2 = [];

                //here we generate data for chart
                for (var i = 0; i < 30; i++) {
                    d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
                    //    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
                }

                var chartMinDate = d1[0][0]; //first day
                var chartMaxDate = d1[20][0]; //last day

                var tickSize = [1, "day"];
                var tformat = "%d/%m/%y";

                //graph options
                var options = {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 10,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor: null,
                        minBorderMargin: 5,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 100
                    },
                    series: {
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 2,
                            steps: false
                        },
                        points: {
                            show: true,
                            radius: 4.5,
                            symbol: "circle",
                            lineWidth: 3.0
                        }
                    },
                    legend: {
                        position: "ne",
                        margin: [0, -25],
                        noColumns: 0,
                        labelBoxBorderColor: null,
                        labelFormatter: function (label, series) {
                            // just add some space to labes
                            return label + '&nbsp;&nbsp;';
                        },
                        width: 40,
                        height: 1
                    },
                    colors: chartColours,
                    shadowSize: 0,
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s: %y.0",
                        xDateFormat: "%d/%m",
                        shifts: {
                            x: -30,
                            y: -50
                        },
                        defaultTheme: false
                    },
                    yaxis: {
                        min: 0
                    },
                    xaxis: {
                        mode: "time",
                        minTickSize: tickSize,
                        timeformat: tformat,
                        min: chartMinDate,
                        max: chartMaxDate
                    }
                };
                var plot = $.plot($("#placeholder33x"), [{
                    label: "Email Sent",
                    data: d1,
                    lines: {
                        fillColor: "rgba(150, 202, 89, 0.12)"
                    }, //#96CA59 rgba(150, 202, 89, 0.42)
                    points: {
                        fillColor: "#fff"
                    }
                }], options);

            });
        });
    </script>

    <!-- -->
    <!-- datepicker -->
    <script type="text/javascript">
        $(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });
            $('#options1').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            $('#options2').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            $('#destroy').click(function () {
                $('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>
    <!-- /datepicker -->

    <!-- chart js -->
    <script src="/js/chartjs/chart.min.js"></script>

    <script type="text/javascript">


        var lineChartData1 = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [
                {
                    label: "Hythe",
                    fillColor: "rgba(255, 255, 255, 0.2)", //rgba(220,220,220,0.2)
                    // fillColor: "rgba(213, 79, 24, 0.2)", //rgba(220,220,220,0.2)
                    strokeColor: "rgba(213, 79, 24, 0.6)", //rgba(220,220,220,1)
                    pointColor: "rgba(213, 79, 24, 1)", //rgba(220,220,220,1)
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [
                        {{ $month_report[1][1] }},
                        {{ $month_report[1][2] }},
                        {{ $month_report[1][3] }},
                        {{ $month_report[1][4] }},
                        {{ $month_report[1][5] }},
                        {{ $month_report[1][6] }},
                        {{ $month_report[1][7] }},
                        {{ $month_report[1][8] }},
                        {{ $month_report[1][9] }},
                        {{ $month_report[1][10] }},
                        {{ $month_report[1][11] }},
                        {{ $month_report[1][12] }},

                        ]
                },

        ]

        }   

        var lineChartData2 = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [
                
                {
                    label: "Sevenoaks",
                    fillColor: "rgba(255, 255, 255, 0.2)", //rgba(220,220,220,0.2)
                    // fillColor: "rgba(26, 187, 156, 0.2)", //rgba(151,187,205,0.2)
                    strokeColor: "rgba(26, 187, 156, 0.6)", //rgba(151,187,205,1)
                    pointColor: "rgba(26, 187, 156, 1)", //rgba(151,187,205,1)
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [
                        {{ $month_report[2][1] }},
                        {{ $month_report[2][2] }},
                        {{ $month_report[2][3] }},
                        {{ $month_report[2][4] }},
                        {{ $month_report[2][5] }},
                        {{ $month_report[2][6] }},
                        {{ $month_report[2][7] }},
                        {{ $month_report[2][8] }},
                        {{ $month_report[2][9] }},
                        {{ $month_report[2][10] }},
                        {{ $month_report[2][11] }},
                        {{ $month_report[2][12] }},

                        ]
                },

        ]

        }

        var lineChartData3 = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [
                
                {
                    label: "Tonbridge",
                    fillColor: "rgba(255, 255, 255, 0.2)", //rgba(220,220,220,0.2)
                    // fillColor: "rgba(52, 152, 219, 0.2)", //rgba(151,187,205,0.2)
                    strokeColor: "rgba(52, 152, 219, 0.6)", //rgba(151,187,205,1)
                    pointColor: "rgba(52, 152, 219, 1)", //rgba(151,187,205,1)
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [
                        {{ $month_report[3][1] }},
                        {{ $month_report[3][2] }},
                        {{ $month_report[3][3] }},
                        {{ $month_report[3][4] }},
                        {{ $month_report[3][5] }},
                        {{ $month_report[3][6] }},
                        {{ $month_report[3][7] }},
                        {{ $month_report[3][8] }},
                        {{ $month_report[3][9] }},
                        {{ $month_report[3][10] }},
                        {{ $month_report[3][11] }},
                        {{ $month_report[3][12] }},

                        ]
                },

        ]

        }

        $(document).ready(function () {
            new Chart(document.getElementById("canvas1").getContext("2d")).Line(lineChartData1, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)"
            });

            new Chart(document.getElementById("canvas2").getContext("2d")).Line(lineChartData2, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)"
            });

            new Chart(document.getElementById("canvas3").getContext("2d")).Line(lineChartData3, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)"
            });
        });

    </script>




    
    <!-- bootstrap progress js -->
    <script src="/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="/js/icheck/icheck.min.js"></script>

@stop