@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ $title }} @if($for_all_view['current_usertype'] == "1")<small><a href="/settings#centre_info">Back to Centre Information List</a></small>@endif</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    @if (isset($message))
                        <div class="alert alert-success" role="alert">
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            {{ $message }}
                        </div>
                    @endif
                    <form id="settings-edit-centre" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="/settings/edit-centre/{{ $centres['id'] }}" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cname">Name
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="cname" name="cname" class="form-control col-md-7 col-xs-12" value="{{ $centres['name'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" rows="4" style="line-height: 24px;">{{ $centres['address'] }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{ $centres['email'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Telephone Number
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="phone" name="phone" class="form-control col-md-7 col-xs-12" value="{{ $centres['phone'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile Number <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="mobile" name="mobile" required="required" class="form-control col-md-7 col-xs-12" value="{{ $centres['mobile'] }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fax">Fax Number
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="fax" name="fax" class="form-control col-md-7 col-xs-12" value="{{ $centres['fax'] }}">
                            </div>
                        </div>
                        <div class="ln_solid"></div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="reg">Regular Price
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="reg" name="price" class="form-control col-md-7 col-xs-12" value="{{ $centres['price'] }}" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dis">Discounted Price
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="dis" name="discounted_price" class="form-control col-md-7 col-xs-12" value="{{ $centres['discounted_price'] }}" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cus">Custom Price
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="cus" name="custom_price" class="form-control col-md-7 col-xs-12" value="{{ $centres['custom_price'] }}" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="com">Commission Rate
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="com" name="rate" class="form-control col-md-7 col-xs-12" value="{{ $centres['rate'] }}" autocomplete="off" required>
                            </div>
                        </div>
                        
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a id="cancel" href="/settings#centre_info" class="btn btn-primary">Cancel</a>
                                <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form><!--/#user-add-form-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>
    </div>
</div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#user-add-form .btn').on('click', function () {
        $('#user-add-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#user-add-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});

$('#cancel').on('click', function(){
    $('#user-add-form').hide();
});
</script>
@stop