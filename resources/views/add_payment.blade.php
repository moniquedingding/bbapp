@extends('admin.admin_layout')

@section('title', $title)

@section('additional_head')
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Payment FARTS Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    <form id="invoice-form" method="post" action="/receipt/add-payment/{{ $invoice_id }}" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                    $customer = $customers_invoices[0];
                                ?>

                                <table class="table table-bordered">

                                    <style type="text/css">
                                        .thirty{
                                            width: 30%;
                                            font-weight: bold;
                                        }
                                    </style>
                                    
                                    <tr>
                                        <td class="thirty">Date</td>
                                        <td>{{ date("D, M j, Y", time()) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="thirty">Receipt # </td>
                                        <td>{{ sprintf( '%07d', $receipt_id) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="thirty">Customer Name</td>
                                        <td>{{ ucfirst($customer->firstname)." ".ucfirst($customer->lastname) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="thirty">Description</td>
                                        <td>
                                            Payment for invoice # {{ sprintf( '%07d', $invoice_id) }}<br>
                                            {{ ucfirst($customer->name) }} enrolled in class {{ ucfirst($customer->title) }}
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="thirty">Total Amount Due</td>
                                        <td>£ {{ number_format($customer->amount_due, 2, '.', '') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="thirty">Remaining Balance</td>
                                        <td>£ {{ number_format($customer->amount_rem, 2, '.', '') }}</td>
                                    </tr>

                                    <tr>
                                        <td class="thirty">Credit</td>

                                        <td class="credit_amount"> <span style='color: #2A3F54'>£ {{ number_format($credits_amount, 2, '.', '') }} </span></td>
                                    </tr>
                                    <tr>
                                        <td class="thirty">
                                            <span style="color: #26B99A">Payment Method </span> <br>
                                        </td>
                                        <td>
                                            <select class="form-control" style="width:30%" name="payment_method">
                                                <option value="cash" class="cash">Cash</option>
                                                <option value="credit" class="credit" data-customerid="{{ $customer_id }}">Credit</option>
                                                <option value="cheque" class="cheque">Cheque</option>
                                            </select>
                                            <span style="color: #D9534F;" class="credit_msg">£ 0.00 credits left. Automatically reverted back to cash. </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="thirty" style="color: #26B99A">Amount (in £)</td>
                                        <td>
                                            <?php
                                                $temp = number_format($customer->amount_rem, 2, '.', '');
                                            ?>


                                            <input type="text" name="amount" class="form-control" id="amountrem" value="" style="width:80%" required>
                                            <input type="hidden" name="description" value="Payment for invoice # {{ sprintf( '%07d', $invoice_id).' enrolled in class '.ucfirst($customer->title) }} ">
                                            <input type="hidden" name="invoice_id" value="{{ $invoice_id }}">
                                            <input type="hidden" name="receipt_id" value="{{ $receipt_id }}">
                                            <input type="hidden" name="customer_id" value="{{ $customer_id }}">
                                            <input type="hidden" name="centre_id" value="{{ $customer->centre_id }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div><!--row-->
                        

                        <div class="clearfix"></div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="/invoice" class="btn btn-primary">Cancel</a>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                    </form><!--/#invoice-form-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>


        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Payment Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div><!--/.x_title-->
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Receipt #</th>
                                    <th>Date Paid</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($payment_history as $row)
                                <tr>
                                    <td><a href="/receipt/view/{{ $row->id }}">{{ sprintf( '%07d', $row->id) }}</a></td>
                                    <td>{{ date("D, M j, Y", $row->date_paid) }}</td>
                                    <td>£ {{ number_format($row->amount_paid, 2, '.', '') }}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div><!--/.row-->
                </div><!--/.x_content-->
            </div><!--/.x_panel-->
        </div>
    </div>
@stop

@section('additional_script')
<!-- form validation -->
<script type="text/javascript" src="{{ asset('js/parsley/parsley.min.js') }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ asset('js/moment.min2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">
$(".credit_msg").hide();
$("#amountrem").attr("value","<?php echo $temp ?>");   

$("body").on("click", ".cash", function(e) {
    e.preventDefault();

    $("#amountrem").val("<?php echo $temp ?>");   
    $("#amountrem").attr("value","<?php echo $temp ?>");
    $("#amountrem").prop("disabled", false);
});

$("body").on("click", ".cheque", function(e) {
    e.preventDefault();

    $("#amountrem").val("<?php echo $temp ?>");
    $("#amountrem").attr("value","<?php echo $temp ?>");   
    $("#amountrem").prop("disabled", false);
});

$("body").on("click", ".credit", function() {
    $.ajax({
    method: "POST",
    url: "/credits/has-credits",
    dataType: "json",
    data: {
        customer_id: $(this).attr('data-customerid'),
        _token: "{{ csrf_token() }}"
        }
    })
    .done(function(res) {
        // alert(res['amount']);

        if (res['amount'] == "0.00") {
            $(".credit_msg").show();
            $("#amountrem").attr("value", "<?php echo $temp ?>");
            $("#amountrem").prop("disabled", false);
            $(".cash").prop("selected", true);
        } else {
            $(".credit_amount").html("<span style='color: #2A3F54'> £ " + res['amount'] + "</span>");
            $("#amountrem").val(res['amount']); 
            $("#amountrem").attr("value",res['amount']);
            // $("#amountrem").attr("value","<?php echo $temp ?>"); 
            $("#amountrem").prop('readonly', true);
        }

    });
});

</script>

<script>
$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('#invoice-form .btn').on('click', function () {
        $('#invoice-form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('#invoice-form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.date-picker').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#date-range').daterangepicker({
        calender_style: "picker_2",
        "startDate": moment().startOf('month'),
        "endDate": moment().endOf('month')
    });
}); 
</script>
@stop