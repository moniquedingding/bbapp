@extends('admin.admin_layout')

@section('title', $title)

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>{{ $title }}</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ ucwords($dog['name']) }} Class Schedule <a href="/dogs"><small>Back to list</small></a></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    @if (isset($message))
                        @if (isset($is_delete)) 
                            <div class="alert alert-danger" role="alert">
                        @else
                            <div class="alert alert-success" role="alert">
                        @endif

                            {{ $message }}
                            <br><a href="/"><strong>Go back to home</strong></a>
                        </div>
                    @endif

                    <table class="table table-striped table-highlight-links">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Details</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($classes as $row)
                            <tr>
                                <td><a href="/classes/view/{{ $row->id }}">{{ $row->title }}</a></td>
                                <td>
                                    <p>{{ date("M j, Y", $row->start_date) }} to {{ date("M j, Y", $row->end_date) }}</p>
                                    <p>{{ date("h:i A", $row->time_start) }} to {{ date("h:i A", strtotime("+".$row->duration." minutes", $row->time_start)) }}</p>
                                    <p>
                                        <?php 

                                            if ($row->frequency == "ALL") {
                                                $str = "Everyday";
                                            } else {
                                                $temp = explode(",", $row->frequency);
                                                $str = "";

                                                foreach($temp as $t) {
                                                    $str .= substr(ucfirst($t), 0, 3)."-";
                                                }

                                                $str = substr($str, 0, strlen($str)-2);
                                            }

                                        echo $str;
                                        ?>
                                    </p>
                                </td>
                                <td>
                                    <a href="/dogs/complete-schedule/{{ $dog_id }}/{{ $row->id }}">View all scheduled dates</a>
                                    <br>
                                    <?php $flag = TRUE; ?>
                                    @foreach($customers_invoices as $ci)

                                        @if ($ci->class_id == $row->id && $ci->status != "new")
                                            <?php $flag = FALSE; ?>
                                        @endif
                                    @endforeach

                                    @if ($flag)
                                        <a href="/dogs/remove-schedule/{{ $dog_id }}/{{ $row->id }}" onclick="return confirm('Permanently remove {{ ucwords($dog['name']) }} from {{ $row->title }} class? This action is NOT reversible once completed.');"> Cancel this class </a>
                                        <br>
                                        <a href="/dogs/complete-schedule/{{ $dog_id }}/{{ $row->id }}">Cancel only on selected dates</a>
                                    @endif

                                </td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />

    </div>
</div>

@stop

@section('additional_script')
        
@stop