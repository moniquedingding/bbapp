@extends('admin.admin_layout')
@section('title', $title)

@section('additional_head')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
 <link href="{{ asset('css/icheck/flat/green.css') }}" rel="stylesheet">
 <link href="{{ asset('css/floatexamples.css') }}" rel="stylesheet" />
 <link href="{{ asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                {{ $title }}
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    @if (isset($is_all_invoice) && $is_all_invoice == TRUE)
                    <h2>Paid Invoices List</h2>
                    @else
                    <h2>{{ $title }} as of {{ date('M Y') }}</h2>
                    @endif

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    @if (!isset($is_all_invoice))
                    <a href="/invoice/view-all" class="btn btn-info">View Paid Invoices</a>
                    <a href="/invoice/view-unpaid" class="btn btn-primary">View Unpaid Invoices</a>
                    @else
                    <a href="/invoice/" class="btn btn-primary">Go back to invoices list</a>
                    @endif
                    <!-- <a href="/invoice/add" class="btn btn-warning"><i class="fa fa-plus"></i> Add Manual Invoice</a> -->
                    @if (isset($message))
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    @endif
                    <table id="example" class="table table-striped responsive-utilities jambo_table table-highlight-links" style="font-size: 11px;">
                        <thead>
                            <tr class="headings">
                                <th>Receipt # </th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Description</th>
                                <th>Payment Method</th>
                                <th>Amount Due</th>
                                <th>Amount Paid</th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count = 0;
                                $due_sum = 0;
                                $paid_sum = 0;
                            ?>
                            @foreach ($receipts as $row)
                                <tr>
                                    <td><a href="/receipt/view/{{ $row->id }}">{{ sprintf('%07d', $row->id) }}</a></td>
                                    <td style="white-space: nowrap;">{{ date("M j, Y", $row->date_paid) }}</td>

                                    <td style="white-space: nowrap;">{{ $customers[$row->customers_invoices_id] }}</td>

                                    <td>{{ $row->description }}</td>
                                    <td>{{ $row->payment_method  }}</td>

                                    @foreach($customers_invoices as $c)
                                        @if ($c->id == $row->customers_invoices_id)
                                            <td>£ {{ number_format($c->amount_due, 2, '.', '') }}</td>
                                        @endif
                                    @endforeach

                                    <td>£ {{ number_format($row->amount_paid, 2, '.', '') }}</td>
                                    <td><a href="/receipt/view/{{ $row->id }}">View</a></td>
                                </tr>
                                
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="col-md-12">
                <hr>
                    <div class="pull-right">
                         <h4>
                            Total amount due: <strong>£ {{ number_format($due_sum, 2, '.', '') }}</strong> &nbsp;&nbsp;|&nbsp;&nbsp;
                            Total amount paid: <strong>£ {{ number_format($paid_sum, 2, '.', '') }}</strong>

                         </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('additional_script')

    <script type="text/javascript">
        /*$(".description").click(function() {
            var desc = $('.description').attr('data-desc'); 
            var id = $('.description').attr('data-id'); 

            $(".desc-td"+id).html(desc);
        });*/   
         $(document).ready(function() {
              // Configure/customize these variables.
              var showChar = 60;  // How many characters are shown by default
              var ellipsestext = "...";
              var moretext = "More <i class='fa fa-angle-double-right'></i>";
              var lesstext = "<i class='fa fa-angle-double-left'></i> Less";
            

            $('.class-description').each(function() {
                var content = $(this).html();
         
                if(content.length > showChar) {
         
                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);
         
                    var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
         
                    $(this).html(html);
                }
         
            });
         
            $(".morelink").click(function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>

    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
        ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "{{ asset('js/datatables/tools/swf/copy_csv_xls_pdf.swf') }}"
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop