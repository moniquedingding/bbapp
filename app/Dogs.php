<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dogs extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'dogs';
    public $timestamps = TRUE;
    protected $fillable = array('customer_id', 'breed', 'color', 'name', 'age', 'image', 'sex', 'price_type', 'new_custom_price', 'updated_at', 'created_at');
}