<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centres extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    public $timestamps = TRUE;
    protected $fillable = array('name', 'address', 'phone', 'mobile', 'email', 'fax', 'price', 'discounted_price', 'custom_price', 'new_price', 'new_discounted_price', 'rate');
}