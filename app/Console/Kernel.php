<?php

namespace App\Console;

use DB;
use Mail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){

            $customers_invoices = DB::table('customers_invoices')
                        ->where('customers_invoices.sent_date', '=', '0')
                        ->where('customers_invoices.start', '>', strtotime("first day of this month"))
                        ->where('customers_invoices.start', '<', strtotime("last day of this month"))/*for testing purpose only*/
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->get();

            $invoice = DB::table('customers_invoices')
                        ->where('customers_invoices.sent_date', '=', '0')
                        ->where('customers_invoices.start', '>', strtotime("first day of this month"))
                        ->where('customers_invoices.start', '<', strtotime("last day of this month"))/*for testing purpose only*/
                        ->get();

            $count = count($invoice);

            $days_cancelled = array();
            $days_abs = DB::table('dogs_days_absent')
                        ->get();

            $days_absent = array();
            foreach($invoice as $key) {
                $days_cancelled[$key->id] = DB::table('dogs_days_cancelled')
                        ->where('class_id', '=', $key->class_id)
                        ->where('dog_id', '=', $key->dog_id)
                        ->get();
            }

            if (isset($days_abs)) {
                foreach($invoice as $row) {
                    foreach($days_abs as $d) {
                        if( ($d->class_id == $row->class_id)  && ($d->dog_id == $row->dog_id) ) {
                            $days_absent[$row->id] = $d->schedule;
                        }
                    }
                }
            }

            for($i=0; $i<$count; $i++){
                $data[$i]['invoice_date'] = $invoice[$i]->created_at;
                $data[$i]['invoice_id'] = $invoice[$i]->id;
                $data[$i]['days_cancelled'] = $days_cancelled[$invoice[$i]->id];
                $data[$i]['days_absent'] = $days_absent;
                $data[$i]['customers_invoices'] = $customers_invoices[$i];
            }

            for($i=0; $i<$count; $i++){
                $temp = [$i, $customers_invoices, $data[$i]['invoice_date'], $data[$i]['invoice_id']];
                Mail::send('email', $data[$i], function ($message) use ($temp) {
                    $message->to($temp[1][$temp[0]]->email, $temp[1][$temp[0]]->firstname.' '.$temp[1][$temp[0]]->lastname);
                    $message->subject('Best Behaviour Invoice for the Month of '.date('F',$temp[2]).' (Invoice #: '.sprintf( '%07d', $temp[3]).')');
                });
            }
        
        })->everyMinute();
    }
}
