<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('login', 'LoginController@login');
Route::post('login/process', 'LoginController@loginCheck');
Route::get('logout', 'LoginController@logout');

Route::get('/', 'HomeController@index');
Route::get('/view-as/{id}', 'HomeController@view_as');

// PROFILE ROUTES
Route::get('profile', 'HomeController@profile');
Route::post('profile', 'HomeController@profile');
Route::get('profile/remove-image/{id}', 'HomeController@remove_profile_image');

// CUSTOMER ROUTES
Route::get('customers', 'HomeController@view_customers');
Route::post('customers/add', 'HomeController@add_customers');
Route::get('customers/add', 'HomeController@add_customers');
Route::get('customers/view-all', 'HomeController@view_all_customers');
Route::get('customers/view/{id}', 'HomeController@view_a_customer');
Route::get('customers/edit/{id}', 'HomeController@edit_customer');
Route::post('customers/edit/{id}', 'HomeController@edit_customer');
Route::get('customers/delete/{id}', 'HomeController@delete_customer');
Route::post('customers/ajax-view', 'HomeController@ajax_view_customers');

// CLASSES ROUTES
Route::get('classes/add', 'HomeController@add_classes');
Route::post('classes/add', 'HomeController@add_classes');
Route::get('classes/edit/{id}', 'HomeController@edit_classes');
Route::post('classes/edit/{id}', 'HomeController@edit_classes');
Route::get('classes/view/{id}', 'HomeController@view_a_class');
Route::get('classes/delete/{id}', 'HomeController@delete_class');
Route::get('classes', 'HomeController@view_all_classes');
Route::post('classes/ajax-view', 'HomeController@ajax_view_classes');
Route::post('classes/get-details', 'HomeController@get_class_details');
Route::get('classes/delete-schedules/{option}/{id}/{timestamp}', 'HomeController@delete_class_with_option');

Route::get('wassap', 'HomeController@wassap');

// DOGS ROUTES
Route::get('dogs', 'HomeController@view_dogs');
Route::post('dogs/ajax-view', 'HomeController@ajax_view_dogs');
Route::get('dogs/delete/{id}', 'HomeController@delete_dog');
Route::get('dogs/edit/{id}', 'HomeController@edit_dog');
Route::post('dogs/edit/{id}', 'HomeController@edit_dog');
Route::get('dogs/add', 'HomeController@add_dogs');
Route::post('dogs/add', 'HomeController@add_dogs');
Route::get('dogs/add/{dog_id}', 'HomeController@add_dogs');
Route::post('dogs/add/{dog_id}', 'HomeController@add_dogs');
Route::get('dogs/view/{dog_id}', 'HomeController@view_a_dog');
Route::get('dogs/enroll/{class_id}', 'HomeController@enroll_dogs');
Route::post('dogs/enroll/{class_id}', 'HomeController@view_dogs');
Route::post('dogs/ajax-enroll-dog', 'HomeController@ajax_enroll_dog');
Route::post('dogs/ajax-search-dog', 'HomeController@ajax_search_dog');
Route::get('dogs/view-schedule/{id}', 'HomeController@view_dog_schedule');
Route::get('dogs/view-schedule/{id}/{sched_type}', 'HomeController@view_dog_schedule');
Route::get('dogs/remove-schedule/{id}/{class_id}', 'HomeController@remove_class_dog');
Route::get('dogs/remove-image/{id}', 'HomeController@remove_dog_image');
Route::get('dogs/complete-schedule/{id}/{class_id}', 'HomeController@view_complete_schedule');
Route::get('dogs/complete-schedule/{id}/{class_id}/{start_sched}', 'HomeController@view_complete_schedule');
Route::get('dogs/remove-selected/{id}/{class_id}/{timestamp}', 'HomeController@remove_selected_class_dog');
Route::get('dogs/mark-as-absent/{dog_id}/{class_id}/{timestamp}', 'HomeController@mark_as_absent_dog');

// CALENDAR ROUTE
Route::get('calendar', 'HomeController@calendar');
Route::get('calendar/get-schedule', 'HomeController@get_schedule');

// INVOICES ROUTES
Route::get('invoice', 'FinanceController@view_all_invoices');
Route::get('invoice/add', 'FinanceController@add_invoice');
Route::get('invoice/view/{invoice_id}', 'FinanceController@view_individual_invoice');
Route::get('invoice/view-all', 'FinanceController@view_invoices_all_dates');
Route::get('invoice/view-unpaid', 'FinanceController@view_invoices_unpaid');
Route::get('invoice/mark-as-paid/{invoice_id}', 'FinanceController@invoice_mark_as_paid');
Route::get('invoice/view-customer/{customer_id}', 'FinanceController@view_invoices_by_customer');
Route::get('invoice/view-past', 'FinanceController@view_past_invoices');
Route::get('invoice/view-future', 'FinanceController@view_future_invoices');
Route::get('invoice/resend-email/{invoice_id}', 'FinanceController@resend_email');

// RECEIPTS ROUTES
Route::get('receipt/add-payment/{invoice_id}', 'FinanceController@add_payment');
Route::post('receipt/add-payment/{invoice_id}', 'FinanceController@add_payment');
Route::get('receipt/view/{id}', 'FinanceController@view_individual_receipt');
Route::get('receipt', 'FinanceController@view_receipts');

// CREDITS ROUTES
Route::get('credits', 'FinanceController@view_credits');
Route::post('credits/has-credits', 'FinanceController@ajax_check_has_credits');

// COMMISSIONS ROUTES
Route::post('commissions/add', 'FinanceController@add_commission');
Route::get('commissions/approve/{id}/{centre_name}', 'FinanceController@approve_commission');
Route::get('commissions/view-approved', 'FinanceController@view_approved_commissions');


// SETTING ROUTE
Route::get('settings', 'HomeController@settings');
Route::post('settings', 'HomeController@settings');
Route::get('settings/edit-centre/{id}', 'HomeController@settings_edit_centre');
Route::post('settings/edit-centre/{id}', 'HomeController@settings_edit_centre');
Route::get('settings/edit-email/{id}', 'HomeController@settings_edit_email');
Route::post('settings/edit-email/{id}', 'HomeController@settings_edit_email');
Route::get('settings/remove-image/{id}', 'HomeController@settings_remove_image');

// USERS ROUTE
Route::get('users', 'HomeController@view_users');
Route::get('users/add', 'HomeController@add_users');
Route::post('users/add', 'HomeController@add_users');
Route::get('users/edit/{id}', 'HomeController@edit_users');
Route::post('users/edit/{id}', 'HomeController@edit_users');
Route::get('users/delete/{id}', 'HomeController@delete_users');

// CRON ROUTES
Route::get('cron/auto-send-email', 'CronController@autoSendEmail');

//TEST ROUTES
Route::get('get-amount/{class_id}/{dog_id}', 'HomeController@get_amount_due');
