<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Mail;
use App\Centres;
use App\Staff;
use App\Customers;
use App\CustomersInvoices;
use App\CustomersReceipts;
use App\CustomersCredits;
use App\Classes;
use App\ClassesCentres;
use App\CancelledClasses;
use App\ClassesDogs;
use App\DogsDaysCancelled;
use App\FinanceCommissionsPaid;
use App\FinanceReports;
use App\User;
use App\Dogs;
use Config;
use DB;
use View;

class FinanceController extends Controller {

	public function __construct() {
        // date_default_timezone_set("Europe/London");
        // Config::set('app.timezone', "Europe/London");

        $is_login = Session::get('login');

        if (empty($is_login)) {
            return Redirect::to('login')->send();
        }

        /*Share View to all Pages (includes App Name & User currently log-in)*/
        $settings = DB::table('settings')
                    ->where('id', '=', '1')
                    ->get();
        $current_user_info = DB::table('users')->select('*')
                    ->where('id', '=', Session::get('user_id'))->get();
        $centres_info_for_all = DB::table('centres')->limit(3)->get();

        $for_all_view['app_name'] = $settings[0]->app_name;
        $for_all_view['app_logo'] = $settings[0]->app_logo;
        $for_all_view['current_user_info'] = $current_user_info;
        $for_all_view['current_usertype'] = Session::get('current_usertype');
        $for_all_view['current_centre'] = Session::get('current_centre');
        $for_all_view['centres_info_for_all'] = $centres_info_for_all;
        $for_all_view['view_as'] = Session::get('view_as');
        View::share('for_all_view', $for_all_view);
	}


/* --------------- INVOICES -------------------- */
    
    public function view_invoices_unpaid() {
        $customers = Customers::all();

        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('centres', 'centres.id', '=', 'customers.centre_id')
                        ->where('status', '=', 'new')
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orwhere('status', '=', 'partial')
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orderBy('customers_invoices.created_at', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id',
                            'customers.id as customer_id',
                            'centres.id as centre_id')
                        ->get();
        } else {
            $customers_invoices = DB::table('customers_invoices')
                        ->where('status', '=', 'new')
                        ->orwhere('status', '=', 'partial')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->orderBy('customers_invoices.created_at', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id')
                        ->get();
        }

        $centres = Centres::all();
        // echo "<pre>";
        // print_r($customers_invoices);
        // exit;

        $data['customers'] = $customers;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['is_active_link'] = 3;
        $data['title'] = "Unpaid & Partial Invoices";
        $data['message'] = Session::get('message');

        return view('view_all_invoices', $data);
    }

    public function view_invoices_all_dates() {
        $customers = Customers::all();

        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('centres', 'centres.id', '=', 'customers.centre_id')
                        ->where('status', '=', 'paid')
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orwhere('status', '=', 'partial')
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orderBy('customers_invoices.created_at', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id',
                            'customers.id as customer_id',
                            'centres.id as centre_id')
                        ->get();
        } else {
            $customers_invoices = DB::table('customers_invoices')
                        ->where('status', '=', 'paid')
                        ->orwhere('status', '=', 'partial')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->orderBy('customers_invoices.created_at', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id')
                        ->get();
        }

        $centres = Centres::all();
        // echo "<pre>";
        // print_r($customers_invoices);
        // exit;

        $data['customers'] = $customers;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['title'] = "All Paid Invoices";
        $data['is_all_invoice'] = TRUE;
        $data['is_active_link'] = 2;
        $data['message'] = Session::get('message');

        return view('view_all_invoices', $data);
    }

    public function view_future_invoices() {
        $customers = Customers::all();

        $this_month = strtotime(date('M Y'));
        $next_month = strtotime("+ 1 month");

        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('centres', 'centres.id', '=', 'customers.centre_id')
                        // ->where('customers_invoices.created_at', '>=', $this_month)
                        ->where('customers_invoices.start', '>=', $next_month)
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orwhere('customers_invoices.end', '>=', $next_month)
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id',
                            'customers.id as customer_id',
                            'centres.id as centre_id')
                        ->get();
        } else {
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        // ->where('customers_invoices.created_at', '>=', $this_month)
                        ->where('customers_invoices.start', '>=', $next_month)
                        ->orwhere('customers_invoices.end', '>=', $next_month)
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id')
                        ->get();
        }

        $centres = Centres::all();
        // echo "<pre>";
        // print_r($customers_invoices);
        // exit;

        $data['customers'] = $customers;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['is_active_link'] = 5;
        $data['title'] = "Future Invoices";
        $data['message'] = Session::get('message');

        return view('view_all_invoices', $data);
    }

    public function view_past_invoices() {

        $customers = Customers::all();

        $this_month = strtotime(date('M Y'));
        $next_month = strtotime("+ 1 month");

        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('centres', 'centres.id', '=', 'customers.centre_id')
                        ->where('customers_invoices.end', '<=', $this_month)
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id',
                            'customers.id as customer_id',
                            'centres.id as centre_id')
                        ->get();
        } else {
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        // ->where('customers_invoices.created_at', '>=', $this_month)
                        ->where('customers_invoices.end', '<=', $this_month)
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id')
                        ->get();
        }

        $centres = Centres::all();
        // echo "<pre>";
        // print_r($customers_invoices);
        // exit;

        $data['customers'] = $customers;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['is_active_link'] = 4;
        $data['title'] = "Past Invoices";
        $data['message'] = Session::get('message');

        return view('view_all_invoices', $data);
    }

    public function view_all_invoices() {

        $customers = Customers::all();

        $this_month = strtotime(date('M Y'));
        $next_month = strtotime("+ 1 month");

        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('centres', 'centres.id', '=', 'customers.centre_id')
                        ->where('customers_invoices.created_at', '>=', $this_month)
                        ->where('customers_invoices.end', '<', $next_month)
                        ->where('centres.id', '=', Session::get('current_centre'))
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id',
                            'customers.id as customer_id',
                            'centres.id as centre_id')
                        ->get();
        } else {
            $customers_invoices = DB::table('customers_invoices')
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('centres', 'centres.id', '=', 'customers.centre_id')
                        ->where('customers_invoices.created_at', '>=', $this_month)
                        ->where('customers_invoices.end', '<', $next_month)
                        ->orderBy('customers_invoices.start', 'DESC')
                        ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                            'customers_invoices.id as id',
                            'customers.id as customer_id',
                            'centres.id as centre_id')
                        ->get();
        }

        $centres = Centres::all();
        // echo "<pre>";
        // print_r($customers_invoices);
        // exit;

        $data['customers'] = $customers;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['title'] = "Invoices";
        $data['is_active_link'] = 1;
        $data['message'] = Session::get('message');

        return view('view_all_invoices', $data);
    }

    public function invoice_mark_as_paid($invoice_id = NULL) {
        $customers_invoices = CustomersInvoices::find($invoice_id);
        $amount_due = $customers_invoices['amount_due'];
        // echo $amount_due; exit;

        $db_data = array(
            'amount_due' => "0.00",
            'amount_paid' => $amount_due,
            'status' => 'paid',
            'paid_date' => time()
        );

        CustomersInvoices::where('id', '=', $invoice_id)->update($db_data);

        $message = "Success! Invoice #".sprintf('%07d', $invoice_id)." is now successfully marked as paid.";

        Session::flash('message', $message);
        return Redirect::to('/invoice');

    }

    public function view_individual_invoice($invoice_id = NULL) {
        $customers_invoices = DB::table('customers_invoices')
                    ->where('customers_invoices.id', '=', $invoice_id)
                    ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                    ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                    ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                    ->orderBy('customers_invoices.start', 'DESC')
                    ->get();


        $invoice = DB::table('customers_invoices')
                    ->where('customers_invoices.id', '=', $invoice_id)
                    ->get();

        $classes_dogs = DB::table('classes_dogs')
                    ->where('class_id', '=', $invoice[0]->class_id)
                    ->get();

        // echo "<pre>";
        // print_r(date('Y-m-d', $customers_invoices[0]->start_date));
        // exit;
           
        $days_cancelled = DB::table('dogs_days_cancelled')
                    ->where('class_id', '=', $customers_invoices[0]->class_id)
                    ->where('dog_id', '=', $customers_invoices[0]->dog_id)
                    ->get();   

        $temp = DB::table('dogs_days_absent')
                    ->where('class_id', '=', $customers_invoices[0]->class_id)
                    ->where('dog_id', '=', $customers_invoices[0]->dog_id)
                    ->get();

        $days_absent = array();
        foreach($temp as $row) {
            $days_absent[] = date("M j, Y", $row->schedule);
        }

        $centres = Centres::all();

        $data['invoice_date'] = $invoice[0]->created_at;
        $data['end_sched'] = $classes_dogs[0]->end_sched;
        $data['invoice_id'] = $invoice_id;
        $data['invoice_start'] = $invoice[0]->start;
        $data['invoice_end'] = $invoice[0]->end;
        $data['days_cancelled'] = $days_cancelled;
        $data['centres'] = $centres;
        $data['days_absent'] = $days_absent;
        $data['customers_invoices'] = $customers_invoices;
        $data['title'] = "View Invoice";
        return view('view_invoice', $data);


    }

    public function add_invoice(){
        $data['title'] = "Add Manual Invoice";
        return view('add_invoice', $data);
    }

    public function view_invoices_by_customer($customer_id) {

        $customers_invoices = DB::table('customers_invoices')
                    ->where('customers_invoices.customer_id', '=', $customer_id)
                    ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                    ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                    ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                    ->join('centres', 'centres.id', '=', 'customers.centre_id')
                    ->orderBy('customers_invoices.created_at', 'DESC')
                    ->select('customers_invoices.*', 'classes.*', 'dogs.*',
                        'customers_invoices.id as id',
                        'customers.id as customer_id',
                        'centres.id as centre_id')
                    ->get();

        $centres = Centres::all();
        
        $customers = DB::table('customers')
                ->where('id', '=', $customer_id)
                ->get();

        $data['customers'] = $customers;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['title'] = ucfirst($customers[0]->firstname)." ".ucfirst($customers[0]->lastname)." Invoice List";
        $data['is_all_invoice_customer'] = TRUE;
        $data['message'] = Session::get('message');

        return view('view_all_invoices', $data);
        
    }


    /* ------------------------------ RECEIPTS ----------------------------- */

    public function view_individual_receipt($receipt_id = NULL) {
        $receipts = CustomersReceipts::find($receipt_id);

        $customers_invoices = DB::table('customers_invoices')
                    ->where('customers_invoices.id', '=', $receipts['customers_invoices_id'])
                    ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                    ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                    ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                    ->orderBy('customers_invoices.created_at', 'DESC')
                    ->get();

        $payment_history = CustomersReceipts::where('customers_invoices_id', '=', $receipts['customers_invoices_id'])
                            ->where('id', '!=', $receipt_id)
                            ->get();

        // echo "<pre>";
        // print_r($payment_history);
        // exit;

        $data['title'] = "Receipt # ".sprintf( '%07d', $receipt_id);
        $data['receipt_id'] = $receipt_id;
        $data['customers_invoices'] = $customers_invoices;
        $data['payment_history'] = $payment_history;
        $data['receipts'] = $receipts;
        $data['invoice_id'] = $receipts['customers_invoices_id'];

        return view('view_a_payment', $data);
    }

    public function view_receipts() {
        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $receipts = DB::table('customers_receipts')
                ->join('customers_invoices', 'customers_invoices.id', '=', 'customers_receipts.customers_invoices_id')
                ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                ->join('centres', 'centres.id', '=', 'customers.centre_id')
                ->where('customers_invoices_id', '!=', '0')
                ->where('centres.id', '=', Session::get('current_centre'))
                ->orderBy('customers_receipts.created_at', 'DESC')
                ->select('customers_receipts.*',
                    'customers_receipts.id as id',
                    'customers_invoices.id as customers_invoices_id',
                    'customers.id as customer_id',
                    'centres.id as centre_id')
                ->get();
        } else {
            $receipts = DB::table('customers_receipts')
                ->where('customers_invoices_id', '!=', '0')
                ->orderBy('customers_receipts.created_at', 'DESC')
                ->get();
        }

        $customers_invoices = CustomersInvoices::all();

        $temp = Customers::all();

        $customers = array();
        foreach($temp as $row) {
            foreach($customers_invoices as $c) {

                if ($c->customer_id == $row->id) {
                    $customers[$c->id] = $row->firstname." ".$row->lastname;
                }
            }

        }

        // echo "<pre>";
        // print_r($receipts);
        // exit;

        $data['title'] = "View All Receipts";
        $data['receipts'] = $receipts;
        $data['customers_invoices'] = $customers_invoices;
        $data['customers'] = $customers;
        // $data['credits'] = $credits;

        return view('view_payments', $data);
    }

    public function add_payment($invoice_id = NULL, Request $request) {

        $post = $request->all();



        if (empty($post)) {
            $receipt_id = Session::get('receipt_id');

            if (empty($receipt_id)) {
                $receipt = CustomersReceipts::create();
                Session::put('receipt_id', $receipt['id']);

                $receipt_id = $receipt['id'];
            }

            $temp = CustomersInvoices::find($invoice_id);

            $credits = DB::table('customers_credits')
                ->where('customer_id', '=', $temp['customer_id'])
                ->get();

            $customers_invoices = DB::table('customers_invoices')
                        ->where('customers_invoices.id', '=', $invoice_id)
                        ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                        ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                        ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                        ->orderBy('customers_invoices.created_at', 'DESC')
                        ->get();

            $payment_history = CustomersReceipts::where('customers_invoices_id', '=', $invoice_id)->get();


            $data['title'] = "Add Payment";
            $data['receipt_id'] = $receipt_id;
            $data['customers_invoices'] = $customers_invoices;
            $data['payment_history'] = $payment_history;
            $data['invoice_id'] = $invoice_id;
            $data['customer_id'] = $temp['customer_id'];
            $data['credits_amount'] = (isset($credits[0]->amount))? $credits[0]->amount: "0.00";
            
            return view('add_payment', $data);
        } else {

        
            $db_data = array(
                'amount_paid' => $post['amount'],
                'description' => $post['description'],
                'customers_invoices_id' => $post['invoice_id'],
                'date_paid' => time(),
                'payment_method' => $post['payment_method'],
            );


            if ($post['payment_method'] == "credit") {
                $db_data['has_credit'] = "yes";
            }

            CustomersReceipts::where('id', '=', $post['receipt_id'])->update($db_data);


            if ($post['payment_method'] == "credit") {
                $credits = CustomersCredits::find($post['customer_id']);

                $excess = $post['amount'] - $credits['amount'];
                $excess = abs($excess);

                if ($excess > 0) {
                    $db_data = array(
                        'amount' => $excess
                    );
                } else {
                    $db_data = array(
                        'amount' => "0.00"
                    );
                }

                CustomersCredits::where('customer_id', '=', $post['customer_id'])->update($db_data);
            }

            $customers_invoices = CustomersInvoices::find($invoice_id);
            $amount_rem = $customers_invoices['amount_rem'];

            $diff = $amount_rem - $post['amount'];
    
            $temp = CustomersCredits::where("customer_id", "=", $customers_invoices['customer_id'])->get();

            $flag = TRUE;
            if ($post['payment_method'] == "credit") {
                $amount = $temp[0]['amount'] - $customers_invoices['amount_rem'];
                
                if ($amount <= 0) {
                    $amount = 0;
                }

                $db_data = array(
                    'amount' => $amount
                );


                CustomersCredits::where('customer_id', '=', $customers_invoices['customer_id'])->update($db_data);

                $flag = FALSE;
            }


            if ($diff == 0) {
                $status = "paid";
            } else if ($diff > 0) {
                $status = "partial";

            } else if ($diff < 0) {             // naay credits
                $status = "paid";
                
                $diff = abs($diff);
                $temp = CustomersCredits::where("customer_id", "=", $customers_invoices['customer_id'])->get();
                if (count($temp) == 0) {

                    $db_data = array(
                        'customer_id' => $customers_invoices['customer_id'],
                        'amount' => $diff
                    );

                    CustomersCredits::create($db_data);

                } else {

                    if ($flag) {
                        $amount = $temp[0]['amount'] + $diff;
                        
                        $db_data = array(
                            'amount' => $amount
                        );

                        CustomersCredits::where('customer_id', '=', $customers_invoices['customer_id'])->update($db_data);
                    }

                }

            }



            $db_data = array(
                'amount_rem' => $diff,
                'amount_paid' => $post['amount'] + $customers_invoices['amount_paid'],
                'status' => $status,
                'paid_date' => time(),
                'payment_method' => $post['payment_method'],
            );

            CustomersInvoices::where('id', '=', $post['invoice_id'])->update($db_data);


            $centres = Centres::find($post['centre_id']);

            Session::forget('receipt_id');

            $message = "Success! Invoice #".sprintf('%07d', $post['invoice_id'])." is now paid £".$post['amount']." with Receipt #".sprintf( '%07d', $post['receipt_id']) .".";

            Session::flash('message', $message);
            return Redirect::to('/invoice');

        }
    }

    /*----------------- CREDITS -----------------*/

    public function ajax_check_has_credits(Request $request) {
        $post = $request->all();

        $credits = DB::table('customers_credits')
                ->where('customer_id', '=', $post['customer_id'])
                ->get();


        if (empty($credits)) {
            $temp = "0.00";
        } else {
            $temp = $credits[0]->amount;
        }

        $data['amount'] = number_format($temp, 2, '.', '');

        echo json_encode($data);
    }

    public function view_credits() {
        $all_credits = CustomersCredits::all();

        if( (Session::get('current_usertype') != "1") || (Session::get('view_as') == TRUE) ){
            $credits = DB::table('customers_credits')
                    ->join('customers', 'customers.id', '=', 'customers_credits.customer_id')
                    ->join('centres', 'centres.id', '=', 'customers.centre_id')
                    ->where('centres.id', '=', Session::get('current_centre'))
                    ->select('customers_credits.*', 'customers.*')
                    ->get();
        } else {
            $credits = DB::table('customers_credits')
                    ->join('customers', 'customers.id', '=', 'customers_credits.customer_id')
                    ->get();
        }

        $customers_invoices = DB::table('customers_invoices')
                ->where('status','!=', 'paid')
                ->get();

        // echo "<pre>";
        // print_r($customers_invoices);
        // exit;

        $data['title'] = "View All Customer Credits";
        // $data['receipts'] = $receipts;
        $data['customers_invoices'] = $customers_invoices;
        $data['credits'] = $credits;

        return view('view_credits', $data);
    }

    /* ------------------------------ MAIL ----------------------------- */
    public function resend_email($invoice_id = NULL){
        $customers_invoices = DB::table('customers_invoices')
                    ->where('customers_invoices.id', '=', $invoice_id)
                    ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                    ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                    ->join('centres', 'centres.id', '=', 'customers.centre_id' )
                    ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                    ->select('*',
                        'customers_invoices.id as id',
                        'customers.phone as phone',
                        'customers.mobile as mobile',
                        'customers.email as email',
                        'customers.fax as fax',
                        'centres.name as centre_name',
                        'centres.phone as centre_phone',
                        'centres.mobile as centre_mobile',
                        'centres.email as centre_email',
                        'centres.fax as centre_fax')
                    ->get();

        $invoice = DB::table('customers_invoices')
                    ->where('customers_invoices.id', '=', $invoice_id)
                    ->get();
           
        $days_cancelled = DB::table('dogs_days_cancelled')
                    ->where('class_id', '=', $customers_invoices[0]->class_id)
                    ->where('dog_id', '=', $customers_invoices[0]->dog_id)
                    ->get();   

        $temp = DB::table('dogs_days_absent')
                    ->where('class_id', '=', $customers_invoices[0]->class_id)
                    ->where('dog_id', '=', $customers_invoices[0]->dog_id)
                    ->get();

        $app_name = DB::table('settings')
                    ->where('id', '=', '1')
                    ->pluck('app_name');

        $days_absent = array();
        foreach($temp as $row) {
            $days_absent[] = date("M j, Y", $row->schedule);
        }

        $data['invoice_date'] = $invoice[0]->created_at;
        $data['invoice_id'] = $invoice_id;
        $data['days_cancelled'] = $days_cancelled;
        $data['days_absent'] = $days_absent;
        $data['customers_invoices'] = $customers_invoices[0];
        $data['centres'] = Centres::all();
        $data['app_name'] = $app_name;

        Mail::send('email', $data, function ($message) use ($data){
            $message->to($data['customers_invoices']->email, $data['customers_invoices']->firstname.' '.$data['customers_invoices']->lastname);
            $message->subject('Best Behaviour Invoice for the Month of '.date('F',$data['invoice_date']).' (Invoice #: '.sprintf( '%07d', $data['invoice_id']).')');
        });

        //Update sent_date value
        $db_data = array( 'sent_date' => time() );
        CustomersInvoices::where('id', '=', $invoice_id)->update($db_data);

        $message = 'You have successfully sent an e-mail to customer.';
        Session::flash('message', $message);
        return Redirect::back();
        //return view('email_test', $data);
    }

    public function add_commission(Request $request) {

        $post = $request->all();

        $db_data = array(
            'centre_id' => $post['centre_id'],
            'amount_requested' => $post['amount_requested'],
            'date_requested' => strtotime($post['date_requested']),
            'method' => $post['method'],
            'paid_by' => $post['paid_by'],
            'approved' => 'no',
        );

        FinanceCommissionsPaid::create($db_data);

        $message = 'You have successfully requested a payment worth £ '.$post['amount_requested'].' to the admin.';
        Session::flash('message', $message);

        return Redirect::to('/');
    }

    public function approve_commission($id = NULL, $centre_name = NULL) {

        $db_data = array(
            'approved' => 'yes',
            'date_approved' => time(),
            'approved_by' => Session::get('initials'),
        );

        FinanceCommissionsPaid::where('id', '=', $id)->update($db_data);

        $message = 'You have successfully approved the commission of '.ucwords($centre_name).' Manager.';
        Session::flash('message', $message);

        return Redirect::to('/');
    }

    public function view_approved_commissions() {
        $data['all_commissions'] = DB::table('finance_commissions_paid')
                ->join('centres', 'centres.id', '=', 'finance_commissions_paid.centre_id')
                ->where('finance_commissions_paid.approved', '=', 'yes')
                ->orderBy('finance_commissions_paid.date_approved', 'DESC')
                ->get();

        $data['title'] = "Approved Commissions";

        return view('view_approved_commissions', $data);
    }

}