<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use View;
use Redirect;
use App\Centres;
use App\Staff;
use App\Customers;
use App\CustomersInvoices;
use App\CustomersCredits;
use App\Classes;
use App\ClassesCentres;
use App\CancelledClasses;
use App\ClassesDogs;
use App\DogsDaysCancelled;
use App\FinanceReports;
use App\FinanceCommissionsPaid;
use App\DogsDaysAbsent;
use App\User;
use App\Dogs;
use App\Settings;
use Config;
use DB;

class HomeController extends Controller {

    public function __construct() {
        // date_default_timezone_set("Europe/London");
        // Config::set('app.timezone', "Europe/London");

        /*For Login*/
        $is_login = Session::get('login');

        if (empty($is_login)) {
            return Redirect::to('login')->send();
        }

        /*Share View to all Pages (includes App Name & User currently log-in)*/
        $settings = DB::table('settings')
                    ->where('id', '=', '1')
                    ->get();
        $current_user_info = DB::table('users')->select('*')
                    ->where('id', '=', Session::get('user_id'))->get();
        $centres_info_for_all = DB::table('centres')->limit(3)->get();

        $for_all_view['app_name'] = $settings[0]->app_name;
        $for_all_view['app_logo'] = $settings[0]->app_logo;
        $for_all_view['current_user_info'] = $current_user_info;
        $for_all_view['current_usertype'] = Session::get('current_usertype');
        $for_all_view['current_centre'] = Session::get('current_centre');
        $for_all_view['centres_info_for_all'] = $centres_info_for_all;
        $for_all_view['view_as'] = Session::get('view_as');
        View::share('for_all_view', $for_all_view);
	}

    public function index() {

        $data['title'] = "Home";

        $temp = date('m')-1;
        $prev_month = strtotime(date('Y')."-".$temp."-30");

        $current_centre = Session::get('current_centre');

        if ($current_centre == 0) {
            
            $data['count_invoices'] =  DB::table('customers_invoices')
                    ->where('due_date', "<=", $prev_month)
                    ->where('status', "!=", "paid")
                    ->count();
        } else {
            $data['count_invoices'] =  DB::table('customers_invoices')
                    ->join('classes_centres', 'classes_centres.class_id', '=', 'customers_invoices.class_id')
                    ->where('classes_centres.centre_id', '=', $current_centre)
                    ->where('due_date', "<=", $prev_month)
                    ->where('status', "!=", "paid")
                    ->count();
        }  


        $this_month = strtotime(date('Y')."-".date('m')."-1");
        
        if ($current_centre == 0) {
            $data['count_classes'] = DB::table('classes')
                  ->count();
        } else {
            $data['count_classes'] = DB::table('classes')
                    ->join('classes_centres', 'classes_centres.class_id', '=', 'classes.id')
                    ->where('classes_centres.centre_id', '=', $current_centre)
                    ->count();
        }


        if ($current_centre == 0) {
            $data['count_customers'] =  DB::table('customers')
                    ->count();
        } else {
            $data['count_customers'] =  DB::table('customers')
                    ->where('centre_id', '=', $current_centre)
                    ->count();
        }

        if ($current_centre == 0) {
            $data['count_dogs'] =  DB::table('dogs')
                    ->count();
        } else {
            $data['count_dogs'] =  DB::table('dogs')
                    ->join('customers', 'customers.id', '=', 'dogs.customer_id')
                    ->where('customers.centre_id', '=', $current_centre)
                    ->count();
        }

        if ($current_centre == 0) {
            $data['centres'] = Centres::all();
        } else {
            $data['centres'] = DB::table('centres')
                    ->where('id', '=', $current_centre)
                    ->get();
        }

        /*-------------- MONTHLY REPORTS ---------------*/
        $year = date('Y');
        $month = date('m')-2;

        $data['receipts_prev2'] = DB::table('finance_reports')
                ->where('month', '=', $month)
                ->where('year', '=', $year)
                ->get();


        $month = date('m')-1;

        $data['receipts_prev'] = DB::table('finance_reports')
                ->where('month', '=', $month)
                ->where('year', '=', $year)
                ->get();
            
        $month = date('m');

        $data['receipts_this'] = DB::table('finance_reports')
                ->where('month', '=', $month)
                ->where('year', '=', $year)
                ->get();
        
        $data['current_centre'] = Session::get('current_centre');


        $data['receipts_all'] = DB::table('finance_reports')
                ->where('year', '=', $year)
                ->get();

        $data['initials'] = Session::get('initials');
        $data['message'] = Session::get('message');

        $data['current_commissions'] = DB::table('finance_commissions_paid')
                            ->where('date_requested', '>=', strtotime(date('Y-m-01')))
                            ->where('date_requested', '<=', strtotime(date('Y-m-t')))
                            ->limit(5)
                            ->get();

        $data['all_commissions'] = FinanceCommissionsPaid::where('approved', '=', 'no')->limit(5)->get();
        $data['approved_commissions'] = FinanceCommissionsPaid::where('approved', '=', 'yes')->get();

        $centres = Centres::all();

        foreach($centres as $cen) {
            for($i=1; $i<=12; $i++) {
                $month_report[$cen->id][$i] = 0;
            }
        }


        for($i=1; $i<=12; $i++) {
            foreach($data['receipts_all'] as $row) {
                if ($row->month == $i) {
                    $month_report[$row->centre_id][$i] += $row->revenue;
                }
            }
        }

        // echo "<pre>";
        // print_r($month_report);
        // exit;
        $data['month_report'] = $month_report;

        return view('dashboard', $data);

    }

    public function view_as($id = NULL){
        if($id != "0")
            Session::put('view_as', True);
        else
            Session::put('view_as', False);

        Session::put('current_centre',$id);

        return Redirect::to('/');
    }

/* --------------- PROFILE -------------------- */
    public function profile(Request $request) {
        $post = $request->all();
        $current_id = Session::get('user_id');

        if (empty($post)) {
            $user = User::find($current_id);
            $edit = FALSE;

            $data['title'] = "Profile";
            $data['message'] = Session::get('message');
            $data['user'] = $user;
            $data['edit'] = $edit;
            
            return view('profile', $data);
        } else {
            if ($request->hasFile('image'))
            {
                $imageName = $request->firstname.'-'.$current_id;
                $destinationPath = public_path().'/images/users/';
                $extension = $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move($destinationPath, $imageName.'.'.$extension);

                $db_data = array(
                    'username' => $post['username'],
                    'password' => (!empty($post['password']) ? $post['password'] : $post['cpassword']),
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'address' => $post['address'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'mobile' => $post['mobile'],
                    'image' => $imageName.'.'.$extension
                );
                $affectedRows = User::where('id', '=', $current_id)->update($db_data);
            } else {
                $db_data = array(
                    'username' => $post['username'],
                    'password' => (!empty($post['password']) ? $post['password'] : $post['cpassword']),
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'address' => $post['address'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'mobile' => $post['mobile']
                );
                $affectedRows = User::where('id', '=', $current_id)->update($db_data);
            }
            $message = "You have successfully updated your profile.";

            Session::flash('message', $message);
            return Redirect::to('/profile');
        }
    }

    public function remove_profile_image($id = NULL){
        $user = User::find($id);

        if (!empty($user['image'])) {

            $image_file = realpath('images/users/'.$user['image']);

            if (file_exists($image_file)) {
                if (is_writable($image_file)) {
                    unlink($image_file);
                }
            }
        } else echo "errer";

        $db_data = array(
            'image' => ""
        );
        $affectedRows = User::where('id', '=', $id)->update($db_data);

        return Redirect::back(); 
    }

/* --------------- CUSTOMER -------------------- */
    
    public function ajax_view_customers(Request $request) {

        $post = $request->all();


        $start = $post['start'];

        if((Session::get('current_centre') > 0) || (Session::get('view_as') > 0)) {
            // $centres = Centres::where('id', '=', Session::get('current_centre'))->get();
            $centre_id = Session::get('current_centre');
            if (!empty($post['search']['value'])) {
                 $customers = DB::table('customers')
                        ->where('centre_id', "=", $centre_id)
                        ->orwhere('firstname', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('middlename', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('lastname', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('state', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('city', 'like', '%'.$post['search']['value'].'%')
                        ->skip($start)
                        ->take($post['length'])
                        ->get();
            } else {
                $customers = DB::table('customers')
                        ->where('customers.centre_id', "=", Session::get('current_centre'))
                        ->skip($start)
                        ->take($post['length'])
                        ->get();


            }

        } else {
            if (isset($post['search']['value'])) {
                $customers = DB::table('customers')
                        ->where('firstname', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('middlename', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('lastname', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('state', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('city', 'like', '%'.$post['search']['value'].'%')
                        ->skip($start)
                        ->take($post['length'])
                        ->get();
            } else {
                $customers = DB::table('customers')
                        ->skip($start)
                        ->take($post['length'])
                        ->get();
            }
        }

        $centres = Centres::all();

        
        $dogs = Dogs::all();

        $i = 0;
        $count = $start;
        foreach($customers as $row) {
            $j = 0;
            $nestedData = array(); 

            $nestedData[$j++] = ++$count;
            $nestedData[$j++] = ucfirst($row->firstname)." ".ucfirst($row->lastname);

            foreach($centres as $cen) {
                if ($cen->id == $row->centre_id) {
                    $nestedData[$j++] = ucfirst($cen->name);
                }
            }

            $nestedData[$j++] = $row->address_1.", ".$row->address_2."...";
            $nestedData[$j++] = str_replace(' ', '', $row->phone);
            $nestedData[$j++] = str_replace(' ', '', $row->mobile);
            $nestedData[$j++] = $row->email;
            
            $nestedData[$j] = "";
            foreach($dogs as $d) {
                if ($d->customer_id == $row->id) {

                    $nestedData[$j] .= "<a href='/dogs/view/".$d->id."'>";
                    $nestedData[$j] .= ucfirst($d->name)."</a><br>";
                }
            }
            $j++;

            $message = 'Permanently delete'.ucfirst($row->firstname).' '.ucfirst($row->lastname).'? NOTE: Dogs that are under this customer will also be deleted.';
            $nestedData[$j++] = '<td class=" last" style="white-space: nowrap;">
                                        <a href="/dogs/add/'.$row->id.'">Add a dog</a> <br>
                                        <a href="/customers/view/'.$row->id.'">View</a> | 
                                        <a href="/customers/edit/'.$row->id.'">Edit</a> | 
                                        <a href="/customers/delete/'.$row->id.'" onclick="return confirm('.$message.');">Delete</a>
                                    </td>';
            
            $data[$i] = $nestedData;
            $i++;
            $j++;
        }


        if (empty($data)) {
            $data = array();
        }


        if((Session::get('current_centre') > 0) || (Session::get('view_as') > 0)) {
            // $centres = Centres::where('id', '=', Session::get('current_centre'))->get();
            $centre_id = Session::get('current_centre');
            $count_customers = DB::table('customers')
                    ->where('customers.centre_id', "=", Session::get('current_centre'))
                    ->get();
        } else {
            $count_customers = DB::table('customers')
                    ->get();
        }

        $json_data = array(
            'draw' => $post['draw'],
            'recordsTotal' =>count($count_customers),
            'recordsFiltered' =>count($count_customers),
            'data' => $data
        );

        // return $json_data;
        echo  json_encode($json_data);
    }

    public function view_customers() {
        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            $customers = DB::table('customers')->where('centre_id', "=", Session::get('current_centre'))->get();
        } else {
            $customers = Customers::all();
        }

        $centres = Centres::all();
        $dogs = Dogs::all();

        $data['title'] = "Customers";
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['dogs'] = $dogs;
        $data['message'] = Session::get('message');

        return view('view_customers', $data);
    }

    public function view_a_customer($id = NULL) {
        if (isset($id)) {
            $centres = Centres::all();
            $staff = Staff::all();
            $customer = Customers::find($id);
            $dogs = Dogs::where('customer_id', '=', $id)->get();
            $manager = DB::table('users')->select('*')->where('usertype', '=', '2')->get();

            $data['title'] = "View a Customer";
            $data['centres'] = $centres;
            $data['staff'] = $staff;
            $data['manager'] = $manager;
            $data['message'] = Session::get('message');
            $data['customer'] = $customer;
            $data['dogs'] = $dogs;
            $data['id'] = $id;

            return view('view_a_customer', $data);
        } else return Redirect::to('/customers');
    }

    public function view_all_customers() {

        $customers = Customers::all();
        $centres = Centres::all();

        $data['title'] = "Customers";
        $data['customers'] = $customers;
        $data['centres'] = $centres;

        return view('view_all_customers', $data);
    }

    public function add_customers(Request $request) {
        $post = $request->all();
        $manager = DB::table('users')->select('*')->where('usertype', '=', '2')->get();
        $staff = Staff::all();

        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            $centres = DB::table('centres')->where('id', '=', Session::get('current_centre'))->get();
        } else {
            $centres = Centres::all();
        }

        if (empty($post)) {

            $customer = array(
                'firstname' => "",
                'middlename' => "",
                'lastname' => "",
                'address_1' => "",
                'address_2' => "",
                'city' => "",
                'state' => "",
                'country' => "",
                'postcode' => "",
                'email' => "",
                'phone' => "",
                'mobile' => "",
                'fax' => "",
                'occupation' => "",
                'notes' => "",
                'is_patron' => "",
                'centre_id' => "",
                'manager_id' => "",
            );

            $data['title'] = "Add a Customer";
            $data['centres'] = $centres;
            $data['staff'] = $staff;
            $data['message'] = Session::get('message');
            $data['customer'] = $customer;
            $data['manager'] = $manager;
            $data['id'] = "";

            return view('add_customers', $data);
        } else {

            $db_data = array(
                'firstname' => $post['firstname'],
                'middlename' => $post['middlename'],
                'lastname' => $post['lastname'],
                'address_1' => $post['address_1'],
                'address_2' => $post['address_2'],
                'city' => $post['city'],
                'state' => $post['state'],
                'country' => $post['country'],
                'postcode' => $post['postcode'],
                'email' => $post['email'],
                'phone' => $post['phone'],
                'mobile' => $post['mobile'],
                'fax' => $post['fax'],
                'occupation' => $post['occupation'],
                'notes' => $post['notes'],
                'is_patron' => isset($post['is_patron']) ? $post['is_patron'] : "no",
                'centre_id' => $post['centre_id'],
                'manager_id' => $post['manager_id'],
            );

            DB::table('customers')->insert($db_data);

            $message = "You have successfully added ".ucfirst($post['firstname'])." ".ucfirst($post['lastname'])." as a customer.";

            Session::flash('message', $message);
            return Redirect::to('/customers/add');
        }
    }

    public function edit_customer($id = NULL, Request $request) {
        $post = $request->all();
        $manager = DB::table('users')->select('*')->where('usertype', '=', '2')->get();

        if (empty($post)) {
            if (isset($id)) {
                if((Session::get('current_usertype') != 1) || (Session::get('view_as')))
                    $centres = DB::table('centres')->where('id', '=', Session::get('current_centre'))->get();
                else
                    $centres = Centres::all();
                
                $staff = Staff::all();
                $customer = Customers::find($id);
                $data['manager'] = $manager;


                $data['title'] = "Update Customer";
                $data['centres'] = $centres;
                $data['staff'] = $staff;
                $data['message'] = Session::get('message');
                $data['customer'] = $customer;
                $data['id'] = $id;

                return view('add_customers', $data);
            } else return Redirect::to('/customers/add');
        } else {
            $db_data = array(
                'firstname' => $post['firstname'],
                'middlename' => $post['middlename'],
                'lastname' => $post['lastname'],
                'address_1' => $post['address_1'],
                'address_2' => $post['address_2'],
                'city' => $post['city'],
                'state' => $post['state'],
                'country' => $post['country'],
                'postcode' => $post['postcode'],
                'email' => $post['email'],
                'phone' => $post['phone'],
                'mobile' => $post['mobile'],
                'fax' => $post['fax'],
                'occupation' => $post['occupation'],
                'notes' => $post['notes'],
                'centre_id' => $post['centre_id'],
                'manager_id' => $post['manager_id'],
                'is_patron' => $post['is_patron']
            );

            // echo "<pre>";
            // print_r($db_data);
            // exit;

            $affectedRows = Customers::where('id', '=', $id)->update($db_data);
            
            $message = "You have successfully udpated the customer information of ".ucfirst($post['firstname'])." ".ucfirst($post['lastname']).".";

            Session::flash('message', $message);
            return Redirect::to('/customers/edit/'.$id);
        }
    }

    public function delete_customer($id = NULL) {
        if (isset($id)) {
            Customers::where('id', '=', $id)->delete();
            Dogs::where('customer_id', '=', $id)->delete();

            $message = "You have successfully deleted 1 row.";
            Session::flash('message', $message);
        }
        return Redirect::to('/customers');
    }


/* --------------- CLASSES -------------------- */
    
    public function delete_class_with_option($option = NULL, $id = NULL, $timestamp = NULL) {

        switch($option) {
            case "entire":
                Session::put('redirect_page', "/calendar");

                $invoices = DB::table('customers_invoices')
                        ->where('class_id', '=', $id)
                        ->where('status', '!=', 'new')
                        ->get();

                if (count($invoices) > 0) {
                    Session::flash('error_delete_modal', "yes");
                    return Redirect::to('/calendar');
                } else {

                    return Redirect::to('/classes/delete/'.$id);
                }

                break;
            case "future":
                
                $invoices = DB::table('customers_invoices')
                        ->where('class_id', '=', $id)
                        ->where('status', '!=', 'new')
                        ->get();

                if (count($invoices) > 0) {
                    Session::flash('error_delete_modal', "yes");
                    return Redirect::to('/calendar');
                } else {

                    DB::table('classes')
                        ->where('id', $id)
                        ->update(array('end_date' => $timestamp));
                    
                    $message = "You have successfully removed the future schedules of the class.";
                    Session::flash('message', $message);
                    
                    Session::flash('is_delete', "yes");
                    
                    return Redirect::to('/calendar');
                }

                break;
            case "selected":

                $invoices = DB::table('customers_invoices')
                        ->where('class_id', '=', $id)
                        ->where('status', '!=', 'new')
                        ->get();

                if (count($invoices) > 0) {
                    Session::flash('error_delete_modal', "yes");
                    return Redirect::to('/calendar');
                } else {

                    $db_data = array(
                        'class_id' => $id,
                        'schedule' => $timestamp
                    );

                    CancelledClasses::create($db_data);

                    $centres = DB::table('classes')
                            ->join('classes_centres', 'classes_centres.class_id', '=', 'classes.id')
                            ->join('centres', 'classes_centres.centre_id', '=', 'centres.id')
                            ->where('class_id', '=', $id)
                            ->get();

                    $customers_invoices = CustomersInvoices::where('class_id', $id)->get();
                    $new_amount = $customers_invoices[0]['amount_due'] - $centres[0]->discounted_price;
                    $new_days = $customers_invoices[0]->days_enrolled - 1;

                    $db_data = array(
                        'amount_due' => $new_amount,
                        'days_enrolled' => $new_days
                    );

                    DB::table('customers_invoices')
                        ->where('class_id', $id)
                        ->update($db_data);


                    $message = "You have successfully removed the future schedules of the class.";
                    Session::flash('message', $message);
                    
                    Session::flash('is_delete', "yes");

                    return Redirect::to('/calendar');
                }


            break;

        }
    }

    public function get_class_details(Request $request) {

        $post = $request->all();

        $data['class_details'] = DB::table('classes')
                                    ->where('title', '=', $post['title'])
                                    ->where('description', '=', $post['description'])
                                    ->limit(1)
                                    ->get();

        $data['enrolled_dogs'] = DB::table('classes_dogs')
                        ->join('dogs', 'dogs.id', '=', 'classes_dogs.dog_id')
                        ->where('class_id', '=', $data['class_details'][0]->id)
                        ->get();

        echo json_encode($data);
    }

    public function delete_class($id = NULL) {
        if (isset($id)) {

            $invoices = CustomersInvoices::where('class_id', '=', $id)->get();
            $classes_centres = ClassesCentres::where('class_id', '=', $id)->get();
            $c = $classes_centres[0];

            $centres = Centres::find($c->centre_id);

            $amount = 0;
            foreach ($invoices as $row) {
                $amount += $row->amount_due;
            }

            $finance = FinanceReports::where('centre_id', '=', $c->centre_id)->where('rate', '=', $centres['rate'])->where('year', '=', date('Y'))->where('month', '=', date('m'))->get();

            if (count($finance) > 0) {

                $new_revenue = $finance[0]->revenue - $amount;

                if ($new_revenue <= 0) {
                    FinanceReports::where('centre_id', '=', $c->centre_id)->where('rate', '=', $centres['rate'])->where('year', '=', date('Y'))->where('month', '=', date('m'))->delete();
                } else {
                    $new_commission = $new_revenue * ($centres['rate'] * 0.01);

                    $db_data = array(
                        'revenue' => $new_revenue,
                        'commission' => $new_commission
                    );

                    FinanceReports::where('id', '=', $finance[0]->id)->update($db_data);
                }
            }

            // echo "<pre>";
            // print_r($new_revenue);
            // // echo $amount."<br>";
            // exit;

            CustomersInvoices::where('class_id', '=', $id)->delete();

            Classes::where('id', '=', $id)->delete();

            ClassesCentres::where('class_id', '=', $id)->delete();
            ClassesDogs::where('class_id', '=', $id)->delete();
            CancelledClasses::where('class_id', '=', $id)->delete();
            DogsDaysCancelled::where('class_id', '=', $id)->delete();

            Session::flash('is_delete', "yes");

            $message = "You have successfully deleted an entire class.";
            Session::flash('message', $message);

        }

        $redirect_page = Session::get('redirect_page');
        Session::forget('redirect_page');

        if (isset($redirect_page)) {
            return Redirect::to($redirect_page);
        } else {
            return Redirect::to('/classes');
        }

    } 


    public function ajax_view_classes(Request $request) {

        $post = $request->all();
        // print_r($post);

        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            
            if (!empty($post['search']['value'])) {
                $classes = DB::table('classes')
                            ->join('classes_centres', 'classes.id', '=', 'classes_centres.class_id')
                            ->select('classes.*')
                            ->where('classes_centres.centre_id', '=', Session::get('current_centre'))
                            ->orwhere('classes.title', 'like', '%'.$post['search']['value'].'%')
                            ->orwhere('classes.description', 'like', '%'.$post['search']['value'].'%')
                            ->orderby('created_at', 'DESC')
                            ->skip($post['start'])
                            ->take($post['length'])
                            ->get();

                $temp = $classes;

                foreach ($temp as $key => $value) {
                    if ($value->centre_id != Session::get('current_centre')) {
                        unset($classes[$key]);
                    }
                }
                $classes = array_values($classes);

            } else {
                $classes = DB::table('classes')
                            ->join('classes_centres', 'classes.id', '=', 'classes_centres.class_id')
                            ->select('classes.*')
                            ->where('classes_centres.centre_id', '=', Session::get('current_centre'))
                            ->orderby('created_at', 'DESC')
                            ->skip($post['start'])
                            ->take($post['length'])
                            ->get();

            }
        } else {
            if (isset($post['search']['value'])) {
                $classes = DB::table('classes')
                            ->orderby('created_at', 'DESC')
                            ->orwhere('classes.title', 'like', '%'.$post['search']['value'].'%')
                            ->orwhere('classes.description', 'like', '%'.$post['search']['value'].'%')
                            ->skip($post['start'])
                            ->take($post['length'])
                            ->get();

            } else {

                $classes = DB::table('classes')
                            ->orderby('created_at', 'DESC')
                            ->skip($post['start'])
                            ->take($post['length'])
                            ->get();
            }
        }
        $count_dogs = $classes;

        foreach($count_dogs as $row) {
            $enrolled_dogs_count[$row->id] = ClassesDogs::where('class_id', '=', $row->id)->count();
        }

        $classes_centres = ClassesCentres::all();
        $centres = Centres::all();

        $class_names = array();
        foreach($centres as $row) {
            $class_names[$row->id] = ucfirst($row->name);
        }

        $i = 0;
        $count = $post['start'];
        foreach($classes as $row) {
            $j = 0;
            $nestedData = array();

            $nestedData[$j++] = ++$count;
            $nestedData[$j++] = ucwords($row->title);
            $nestedData[$j++] = substr(ucfirst($row->description), 0, 30)."...";

            foreach($classes_centres as $c) {
                if ($row->id == $c->class_id) {
                    $name = $class_names[$c->centre_id];
                }
            }
            $nestedData[$j++] = $name;
            

            $temp = floor($row->duration/60);

            $nestedData[$j] = "";
            $nestedData[$j] .= $temp;
            $nestedData[$j] .= " hrs";

            $mins = $row->duration%60;

            if ($mins > 0) {
                $nestedData[$j] .= " ".$mins." mins";
            }

            $j++;

            $nestedData[$j++] = date("D, M j, Y", $row->start_date);
            
            $nestedData[$j] = "";
            $nestedData[$j] = date("g:i A", $row->time_start)." to ".date("g:i A", strtotime("+".$row->duration." minutes", $row->time_start))." <br>";
            
            if ($row->frequency == "ALL") {
                $nestedData[$j] = "Everyday<br>";
            } else {
                $str = explode(",", $row->frequency);

                foreach($str as $s) {
                    $nestedData[$j] .= ucfirst(substr($s, 0, 3));
                }
            }

            $j++;

            if (!empty($enrolled_dogs_count[$row->id])) {   
                $nestedData[$j++] = $enrolled_dogs_count[$row->id];
            }
            else {   
                $nestedData[$j++] = "0";
            }

            $message = 'Permanently delete '.$row->title.' class? This action is NOT reversible once completed.';
            $nestedData[$j++] = '<td class=" last" style="white-space: nowrap;">
                                        <a href="/dogs/enroll/'.$row->id.'">Enroll Dogs</a>  <br>
                                        <a href="/classes/view/'.$row->id.'">View</a> | 
                                        <a href="/classes/edit/'.$row->id.'">Edit</a> | 
                                        <a href="/classes/delete/'.$row->id.'" onclick="return confirm('.$message.');">Delete</a>
                                    </td>';

            $data[$i++] = $nestedData;
            // $i++; 
        }

        if (empty($data)) {
            $data = array();
        }

        if((Session::get('current_centre') > 0) || (Session::get('view_as') > 0)) {
            // $centres = Centres::where('id', '=', Session::get('current_centre'))->get();
            $centre_id = Session::get('current_centre');
            $count_classes = DB::table('classes')
                            ->join('classes_centres', 'classes.id', '=', 'classes_centres.class_id')
                            ->where('classes_centres.centre_id', '=', Session::get('current_centre'))
                            ->get();
        } else {
            $count_classes = DB::table('classes')
                            ->join('classes_centres', 'classes.id', '=', 'classes_centres.class_id')
                            ->get();
        }

        $json_data = array(
            'draw' => $post['draw'],
            'recordsTotal' => count($count_classes),
            'recordsFiltered' => count($count_classes),
            'data' => $data
        );

        echo json_encode($json_data);
    }


    public function edit_classes(Request $request, $id = NULL) {
        if (empty($post)) {
            $centres = Centres::all();
            $centre_per_centre = DB::table('centres')->where('id', '=', Session::get('current_centre'))->get();
            $classes = Classes::find($id);

            $included_centres = DB::table('classes_centres')
                                    ->join('centres', 'centres.id', '=', 'classes_centres.centre_id')
                                    ->where('class_id', '=', $id)
                                    ->get();

            $data['title'] = "Update Class Information";
            $data['centres'] = $centres;
            $data['centre_per_centre'] = $centre_per_centre;
            $data['classes'] = $classes;
            $data['included_centres'] = $included_centres;
            $data['is_edit'] = "edit class";

            return view('add_classes', $data);
        } else {

            $db_data = array(
                'title' => ucwords($post['title']),
                'description' => $post['description'],
                'duration' => $post['duration'],
                'is_repeating' => $post['is_repeating'],
                'start_date' => strtotime($post['start_date']),
                'end_date' => strtotime($post['end_date']),
                'time_start' => strtotime($post['time_start']),
            );

            $result = Classes::create($db_data);

            foreach($post['centre_id'] as $c) {
                $db_data = array(
                    'class_id' => $result['id'],
                    'centre_id' => $c
                );
                
                ClassesCentres::create($db_data);
               
            }


            $message = "You have successfully created class ".ucwords($post['title'])." as a customer.";
            Session::flash('message', $message);

            return Redirect::to('/dogs/enroll/'.$result['id']);
        }
    }

    public function view_a_class($id) {
        $classes = Classes::find($id);
        $enrolled_dogs = DB::table('classes_dogs')
                        ->join('dogs', 'dogs.id', '=', 'classes_dogs.dog_id')
                        ->where('class_id', '=', $id)
                        ->get();

        $centres = DB::table('classes_centres')
                        ->join('centres', 'centres.id', '=', 'classes_centres.centre_id')
                        ->where('class_id', '=', $id)
                        ->get();

        $data['title'] = "View Class Information";
        $data['class'] = $classes;
        $data['enrolled_dogs'] = $enrolled_dogs;
        $data['centres'] = $centres;

        return view('view_a_class', $data);
    }

    public function view_all_classes() {
        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            $classes = DB::table('classes')
                            ->join('classes_centres', 'classes.id', '=', 'classes_centres.class_id')
                            ->select('classes.*')
                            ->where('classes_centres.centre_id', '=', SESSION::get('current_centre'))
                            ->orderby('created_at', 'DESC')
                            ->get();
        } else {
            $classes = DB::table('classes')
                            ->orderby('created_at', 'DESC')
                            ->get();
        }



        $enrolled_dogs_count = array();

        $count_dogs = $classes;

        foreach($count_dogs as $row) {
            $enrolled_dogs_count[$row->id] = ClassesDogs::where('class_id', '=', $row->id)->count();
        }

        // echo "<pre>";
        // print_r($classes);
        // exit;

        $classes_centres = DB::table('classes_centres')
                        ->join('centres', 'centres.id', '=', 'classes_centres.centre_id')
                        ->get();

        $data['title'] = "View All Classes";
        $data['classes'] = $classes;
        $data['classes_centres'] = $classes_centres;
        $data['enrolled_dogs_count'] = $enrolled_dogs_count;
        $data['message'] = Session::get('message');
        $data['is_delete'] = Session::get('is_delete');


        return view('view_all_classes', $data);
    }

    public function add_classes(Request $request) {
        $post = $request->all();

        if (empty($post)) {
            $centres = Centres::all();

            $classes = array(
                'title' => "",
                'description' => "",
                'duration' => "",
                'start_date' => "",
                'is_repeating' => "",
                'end_date' => "",
                'start_date' => "",
                'frequency' =>"",
                'time_start' =>""
            );

            $data['title'] = "Add a Class";
            $data['centres'] = $centres;
            $data['classes'] = $classes;

            return view('add_classes', $data);
        } else {

            if (!isset($post['is_edit'])) {

                if ($post['is_repeating'] == "forever") {
                    $year = date('Y') + 3;
                    $post['end_date'] = "December 31, ".$year;
                }

                if ($post['frequency'] == "ALL") {
                    $frequency = $post['frequency'];

                    $num_days = strtotime($post['end_date']) - strtotime($post['start_date']);
                    $num_days = $num_days/(60*60*24);

                } else {
                    $frequency = "";
                    foreach($post['frequency'] as $f) {
                        $frequency .= $f.",";
                    }


                    $num_days = 0;
                    $start_date = strtotime($post['start_date']);
                    // $start_date = strtotime("+ 1 day", $start_date);

                    while($start_date <= strtotime($post['end_date'])) {

                        $temp = date('l', $start_date);

                        $res = array_search(strtolower($temp), $post['frequency']);

                        if (is_numeric($res)) {   
                            $num_days++;
                        }

                        $start_date = strtotime("+ 1 day", $start_date);

                    }

                }
            }

            if (isset($post['is_edit']) && $post['is_edit'] == "edit class") {
                $db_data = array(
                    'title' => ucwords($post['title']),
                    'description' => $post['description'],
                    // 'duration' => $post['duration'],
                    // 'is_repeating' => $post['is_repeating'],
                    // 'start_date' => strtotime($post['start_date']),
                    // 'end_date' => strtotime($post['end_date']),
                    // 'time_start' => strtotime($post['time_start']),
                    // 'frequency' => $frequency,
                    // 'num_days' => $num_days
                );
                
                Classes::where('id', '=', $post['id'])->update($db_data);

                $message = "You have successfully updated class ".ucwords($post['title']).".";
                Session::flash('message', $message);

                return Redirect::to('/classes');

            } else {

                // $temp = strtotime($post['start_date']);
                // $start = strtotime("-1 day", $temp);

                $db_data = array(
                    'title' => ucwords($post['title']),
                    'description' => $post['description'],
                    'duration' => $post['duration'],
                    'is_repeating' => $post['is_repeating'],
                    'start_date' => strtotime($post['start_date']),
                    'end_date' => strtotime($post['end_date']),
                    'time_start' => strtotime($post['time_start']),
                    'frequency' => $frequency,
                    'num_days' => $num_days
                );

                $result = Classes::create($db_data);


                $centre_id = $post['centre_id'][0];
                    // var_dump($post['centre_id'][0]); exit;

                $centres = DB::table('centres')
                    ->where('id', '=', (int) $centre_id)
                    ->get();

                foreach($post['centre_id'] as $c) {
                    $db_data = array(
                        'class_id' => $result['id'],
                        'centre_id' => $c
                    );
                    
                    ClassesCentres::create($db_data);
                }
                
                $message = "You have successfully created class ".ucwords($post['title'])." as a customer.";
                Session::flash('message', $message);

                return Redirect::to('/dogs/enroll/'.$result['id']);
            }

        }
    }

/* --------------- DOGS -------------------- */
    
    
    public function mark_as_absent_dog($dog_id = NULL, $class_id = NULL, $timestamp = NULL) {

        $db_data = array(
            'dog_id' => $dog_id,
            'class_id' => $class_id,
            'schedule' => $timestamp
        );

        DogsDaysAbsent::create($db_data);

        $dog = Dogs::find($dog_id);

        $message = "You have successfully marked ".ucfirst($dog['name'])." absent on ".date('M d, Y', $timestamp).".";
        Session::flash('message', $message);

        return Redirect::to('/calendar');
    }


    public function view_a_dog($dog_id = NULL) {

        if (isset($dog_id)) {
            $dogs = Dogs::find($dog_id);
            $customers = Customers::all();  

            $data['title'] = "View Dog Information";
            $data['dog'] = $dogs;
            $data['customers'] = $customers;

            return view('view_a_dog', $data);
        } else return Redirect::to('/');
    }

    public function ajax_view_dogs(Request $request) {
        $post = $request->all();

        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {

            if (isset($post['search']['value'])) {
                $dogs = DB::table('dogs')
                        ->join('customers', 'dogs.customer_id', '=', 'customers.id')
                        ->where('customers.centre_id', '=', SESSION::get('current_centre'))
                        ->orwhere('name', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('breed', 'like', '%'.$post['search']['value'].'%')
                        ->orderBy('customers.created_at', 'DESC')
                        ->skip($post['start'])
                        ->take($post['length'])
                        ->get();

                $temp = $dogs;
                foreach ($temp as $key => $value) {
                    
                    if ($value->centre_id != Session::get('current_centre')) {
                        unset($dogs[$key]);
                    }
                }
                $dogs = array_values($dogs);

            } else {
                $dogs = DB::table('dogs')
                        ->join('customers', 'dogs.customer_id', '=', 'customers.id')
                        ->select('dogs.*')
                        ->where('customers.centre_id', '=', SESSION::get('current_centre'))
                        ->orderBy('created_at', 'DESC')
                        ->skip($post['start'])
                        ->take($post['length'])
                        ->get();
            }

        } else {
            if (isset($post['search']['value'])) {
                $dogs = DB::table('dogs')
                        ->orderBy('created_at', 'DESC')
                        ->orwhere('name', 'like', '%'.$post['search']['value'].'%')
                        ->orwhere('breed', 'like', '%'.$post['search']['value'].'%')
                        ->skip($post['start'])
                        ->take($post['length'])
                        ->get();
            } else {

                $dogs = DB::table('dogs')
                        ->orderBy('created_at', 'DESC')
                        ->skip($post['start'])
                        ->take($post['length'])
                        ->get();
            }
        }

        $customers = DB::table('customers')
                    ->orderBy('created_at', 'DESC')
                    ->get();


        $centres = Centres::all();

        $i = 0;
        $count = $post['start'];
        foreach($dogs as $row) {
            $j = 0;
            $nestedData = array();

            $nestedData[$j++] = ++$count;
            
            if(empty($row->image)) {
                $image_path = asset('/images/pets/no-image.jpg');
                $nestedData[$j++] = '<img src="'.$image_path.'" alt="" class="img-responsive" width="100" style="max-height:90px;">';
            } else {
                $image_path = asset('/images/pets/'.$row->image);
                $nestedData[$j++] = '<img src="'.$image_path.'" alt="" class="img-responsive" width="100" style="max-height:90px;">';
            }

            $nestedData[$j++] = '<a href="/dogs/view/'.$row->id.'">'.ucwords($row->name).'</a>';
            $nestedData[$j++] = ucwords($row->breed);
            $nestedData[$j++] = ucwords($row->color);
            $nestedData[$j++] = $row->age;

            foreach($customers as $c){

                if($row->customer_id == $c->id) {
                    
                    $nestedData[$j++] = ucfirst($c->firstname)." ".ucfirst($c->lastname);
                    foreach($centres as $cen) {
                        if ($c->centre_id == $cen->id) {
                            $nestedData[$j++] = ucfirst($cen->name);
                        }
                    }
                }
            }

            $message = 'Permanently delete'.ucwords($row->name).'? This action is NOT reversible once completed.';
            $nestedData[$j++] = '<td class="last" style="white-space: nowrap;">
                                    <a href="/dogs/view-schedule/'.$row->id.'">View schedules</a><br>
                                    <a href="/dogs/view/'.$row->id.'">View </a> |
                                    <a href="/dogs/edit/'.$row->id.'">Edit </a> |
                                    <a href="/dogs/delete/'.$row->id.'" onclick="return confirm('.$message.');">Delete</a>                                    
                                </td>';

            $data[$i++] = $nestedData;

        }


        if (empty($data)) {
            $data = array();
        }

        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            $count_dogs = DB::table('dogs')
                        ->join('customers', 'dogs.customer_id', '=', 'customers.id')
                        ->where('customers.centre_id', '=', SESSION::get('current_centre'))
                        ->get();
        } else {
            $count_dogs = DB::table('dogs')->get();
        }

        $json_data = array(
            'draw' => $post['draw'],
            'recordsTotal' => count($count_dogs),
            'recordsFiltered' => count($count_dogs),
            'data' => $data
        );

        // return $json_data;
        echo  json_encode($json_data);

    }

    public function view_dogs() {
        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            $dogs = DB::table('dogs')
                ->join('customers', 'dogs.customer_id', '=', 'customers.id')
                ->select('dogs.*')
                ->where('customers.centre_id', '=', SESSION::get('current_centre'))
                ->orderBy('created_at', 'DESC')
                ->get();
        } else {
            $dogs = DB::table('dogs')
                        ->orderBy('created_at', 'DESC')
                        ->get();
        }

        $customers = DB::table('customers')
                    ->orderBy('created_at', 'DESC')
                    ->get();


        $centres = Centres::all();

        $data['title'] = "View All Dogs";
        $data['dogs'] = $dogs;
        $data['customers'] = $customers;
        $data['centres'] = $centres;
        $data['message'] = Session::get('message');
        $data['is_delete'] = Session::get('is_delete');

        return view('view_dogs', $data);
    }

    public function add_dogs(Request $request, $customer_id = NULL) {
        $post = $request->all();
        $last_id = DB::table('dogs')->select('id')->orderBy('id', 'desc')->limit(1)->get();
        $new_image_id = $last_id == NULL ? "1" : ($last_id[0]->id + 1);

        if (empty($post)) {
            if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
                $customers = DB::table('customers')->where('centre_id', '=', SESSION::get('current_centre'))->orderBy('firstname', 'ASC')->get();
            } else {
                $customers = Customers::orderBy('firstname', 'ASC')->get();
            }

            $dogs = array(
                'customer_id' => $customer_id,
                'breed' => "",
                'color' => "",
                'age' => "",
                'name' => "",
                'image' => ""
            );

            $data['message'] = Session::get('message');
            $data['title'] = "Add Dog Information";
            $data['customers'] = $customers;
            $data['dogs'] = $dogs;

            return view('add_dogs', $data);
        } else {

            

            if ($request->hasFile('image'))
            {
                if($request->file('image')->getClientSize() >  300000){
                    $message = "Error in downloading the image, file size is too large. Please upload an image that is at most 640px.";
                    Session::flash('message', $message);

                    return redirect('/dogs/add');
                } else {
                    $imageName = $request->name.'-'.$new_image_id;
                    $destinationPath = public_path().'/images/pets/';
                    $extension = $request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($destinationPath, $imageName.'.'.$extension);

                    $db_data = array(
                        'customer_id' => $post['customer_id'],
                        'breed' => $post['breed'],
                        'color' => $post['color'],
                        'age' => $post['age'],
                        'name' => $post['name'],
                        'image' => $imageName.'.'.$extension,
                        'sex' => $post['sex'],
                        'price_type' => $post['price_type'],
                        'new_custom_price' => $post['new_custom_price']
                    );

                    Dogs::create($db_data);
                }
            } else {
                $db_data = array(
                    'customer_id' => $post['customer_id'],
                    'breed' => $post['breed'],
                    'color' => $post['color'],
                    'age' => $post['age'],
                    'name' => $post['name'],
                    'image' => ""
                );
                Dogs::create($db_data);
            }

            $message = "You have successfully added ".$post['name']." to the system.";
            Session::flash('message', $message);

            return Redirect::to('/dogs/');
        }
    }

    public function edit_dog($id = NULL, Request $request) {
        $post = $request->all();

        if (empty($id)) {
            return Redirect::to('/');
        }

        if (empty($post)) {
            $dogs = Dogs::find($id);   
            $customers = Customers::all();
            $customers_per_centre = DB::table('customers')->where('centre_id', '=', SESSION::get('current_centre'))->get();

            $data['message'] = Session::get('message');
            $data['title'] = "Update Dog Information";
            $data['dogs'] = $dogs;
            $data['customers'] = $customers;
            $data['customers_per_centre'] = $customers_per_centre;
            $data['id'] = $id;

            return view('add_dogs', $data);
        } else {
            if ($request->hasFile('image'))
            {
                if($request->file('image')->getClientSize() >  300000){
                    $message = "Large file size.";
                    Session::flash('message', $message);

                    return redirect('/dogs/edit/'.$id);
                } else {
                    $imageName = $request->name.'-'.$id;
                    $destinationPath = public_path().'/images/pets/';
                    $extension = $request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($destinationPath, $imageName.'.'.$extension);

                    $db_data = array(
                        'customer_id' => $post['customer_id'],
                        'breed' => $post['breed'],
                        'color' => $post['color'],
                        'age' => $post['age'],
                        'name' => $post['name'],
                        'image' => $imageName.'.'.$extension
                    );
                    $affectedRows = Dogs::where('id', '=', $post['id'])->update($db_data);
                }
            } else {
                $db_data = array(
                    'customer_id' => $post['customer_id'],
                    'breed' => $post['breed'],
                    'color' => $post['color'],
                    'age' => $post['age'],
                    'name' => $post['name']
                );
                $affectedRows = Dogs::where('id', '=', $post['id'])->update($db_data);
            }

            /*$db_data = array(
                'customer_id' => $post['customer_id'],
                'breed' => $post['breed'],
                'color' => $post['color'],
                'age' => $post['age'],
                'name' => $post['name'],
                'image' => $post['image']
            );

            $affectedRows = Dogs::where('id', '=', $post['id'])->update($db_data);*/
            
            $message = "You have successfully udpated the profile of ".ucwords($post['name']).".";

            Session::flash('message', $message);
            return Redirect::to('/dogs');
        }
    }

    public function view_complete_schedule($dog_id = NULL, $class_id = NULL, $timestamp = NULL) {


        $schedule = DB::table('classes_dogs')
                    ->where('dog_id', '=', $dog_id)
                    ->where('class_id', '=', $class_id)
                    ->join('classes', 'classes_dogs.class_id', '=', 'classes.id')
                    ->limit(1)
                    ->get();

        $dogs_days_cancelled =  DB::table('dogs_days_cancelled')
                    ->where('dog_id', '=', $dog_id)
                    ->where('class_id', '=', $class_id)
                    ->join('classes', 'dogs_days_cancelled.class_id', '=', 'classes.id')
                    ->get();

        $temp =  DB::table('dogs_days_absent')
                    ->where('dog_id', '=', $dog_id)
                    ->where('class_id', '=', $class_id)
                    ->join('classes', 'dogs_days_absent.class_id', '=', 'classes.id')
                    ->get();

        $dogs_days_absent = array();
        foreach ($temp as $t) {
            $dogs_days_absent[] = $t->schedule;
        }


        $customers_invoices = CustomersInvoices::where('dog_id', '=', $dog_id)->where('class_id', '=', $class_id)->limit(1)->get();

        $dog = Dogs::find($dog_id);


        $data['title'] = ucfirst($dog['name'])."'s List of Class Sessions";
        $data['schedule'] = $schedule;
        if (isset($customers_invoices[0])) {
            $data['status'] = $customers_invoices[0]['status'];
        } else {
            $data['status'] = "new";
        }

        $data['dogs_days_cancelled'] = $dogs_days_cancelled;
        $data['dogs_days_absent'] = $dogs_days_absent;
        $data['dog_id'] = $dog_id;
        $data['class_id'] = $class_id;
        $data['message'] = Session::get('message');
        $data['is_delete'] = Session::get('is_delete');

        if (!empty($timestamp)) {
            $data['timestamp'] = $timestamp;
        }

        $data['limit'] = 20;

        $data['page_count'] = Session::get('count');
        Session::forget('count');

        return view('dog_complete_schedule', $data);
    }

    public function remove_selected_class_dog($dog_id = NULL, $class_id = NULL, $timestamp = NULL) {
        
        $db_data = array(
            'dog_id' => $dog_id,
            'class_id' => $class_id,
            'schedule' => $timestamp
        );

        DogsDaysCancelled::create($db_data);

        $centres = DB::table('classes')
                        ->join('classes_centres', 'classes_centres.class_id', '=', 'classes.id')
                        ->join('centres', 'classes_centres.centre_id', '=', 'centres.id')
                        ->where('class_id', '=', $class_id)
                        ->get();

        $customers_invoices = CustomersInvoices::where('dog_id', '=', $dog_id)->where('class_id', '=', $class_id)->get();
        $new_amount = $customers_invoices[0]['amount_due'] - $centres[0]->discounted_price;
        $new_days = $customers_invoices[0]->days_enrolled - 1;


        $db_data = array(
            'amount_due' => $new_amount,
            'days_enrolled' => $new_days
        );

        // echo "<pre>";
        // print_r($db_data);
        // exit;

        CustomersInvoices::where('dog_id', '=', $dog_id)->where('class_id', '=', $class_id)->update($db_data);

        // DB::table('customers_invoices')
        //     ->where('class_id', $class_id)
        //     ->where('dog_id', $class_id)
        //     ->update($db_data);


        $message = "You have successfully removed the schedule in the list.";
        Session::flash('message', $message);

        $temp = "delete";
        Session::flash('is_delete', $temp);

        return Redirect::to('/dogs/complete-schedule/'.$dog_id.'/'.$class_id);
    }

    public function view_dog_schedule($id = NULL, $sched_type = NULL) {
            
            $dog = Dogs::find($id);
            $classes = DB::table('classes_dogs')
                    ->where('dog_id', '=', $id)
                    ->where('classes.start_date', '>=', time())
                    ->join('classes', 'classes_dogs.class_id', '=', 'classes.id')
                    ->get();

            $customers_invoices = CustomersInvoices::where('dog_id', '=', $id)->get();

            if ($sched_type == "absences") {
                $data['sched_type'] = $sched_type;
            }

            // echo "<pre>";
            // print_r($classes);
            // exit;
        
            $data['title'] = "Active Classes";
            $data['dog'] = $dog;
            $data['customers_invoices'] = $customers_invoices;
            $data['classes'] = $classes;
            $data['dog_id'] = $id;
            $data['message'] = Session::get('message');
            $data['is_delete'] = Session::get('is_delete');

            return view('dog_schedule', $data);
    } 

    public function delete_dog($id) {

        $dogs = Dogs::find($id);

        if (!empty($dogs['image'])) {

            $image_file = realpath('images/pets/'.$dogs['image']);

            if (file_exists($image_file)) {
                if (is_writable($image_file)) {
                    unlink($image_file);
                } else echo "why";
            }
        } else echo "errer";

        Dogs::where('id', '=', $id)->delete();

        $message = "You have successfully removed ".$dogs['name']." in the dogs list.";
        Session::flash('message', $message);

        $temp = "delete";
        Session::flash('is_delete', $temp);

        return Redirect::to('/dogs');
    }

    public function remove_class_dog($dog_id = NULL, $class_id = NULL) {
        echo $dog_id, " ", $class_id;

        DB::table('classes_dogs')
            ->where('class_id', '=', $class_id)
            ->where('dog_id', '=', $dog_id)
            ->delete();

        $message = "The class was successfully removed from the list.";
        Session::flash('message', $message);

        return Redirect::to('/dogs/view-schedule/'.$dog_id);
    }

    public function remove_dog_image($id) {

        $dogs = Dogs::find($id);

        if (!empty($dogs['image'])) {

            $image_file = realpath('images/pets/'.$dogs['image']);

            if (file_exists($image_file)) {
                if (is_writable($image_file)) {
                    unlink($image_file);
                }
            }
        } else echo "errer";

        $db_data = array(
                        'image' => ""
                    );
                    $affectedRows = Dogs::where('id', '=', $id)->update($db_data);

        return Redirect::back();        
    }

    public function ajax_search_dog(Request $request) {

        $post = $request->all();
        

        $classes_centres = ClassesCentres::where('class_id', '=', $post['class_id'])->get();

        $data['dogs'] = DB::table('dogs')
            ->select('dogs.*', 'customers.centre_id as centre_id')
            ->join('customers', 'customers.id', '=', 'dogs.customer_id')
            ->where('customers.centre_id', '=', $classes_centres[0]->centre_id)
            ->where('dogs.name', 'like', '%'.$post['keyword'].'%')
            ->get();

        $temp = Customers::where('centre_id', '=', $classes_centres[0]->centre_id)->get();

        $classes_dogs = DB::table('classes_dogs')
            ->join('classes_centres', 'classes_centres.class_id', '=', 'classes_dogs.class_id')
            ->where('classes_centres.centre_id', '=', $classes_centres[0]->centre_id)
            ->get();

        $data['enrolled_dogs'] = $classes_dogs;


        $customers = array();
        foreach ($temp as $key => $value) {
            $customers[$key] = ucfirst($value->firstname)." ".ucfirst($value->lastname);
        }

        $data['customers'] = $customers;

        echo json_encode($data);


    }

    public function enroll_dogs($class_id = NULL, Request $request) {
        $post = $request->all();

        if (empty($post)) {
            $classes = Classes::find($class_id);

            $classes_centres = DB::table('classes_centres')
                        ->where('class_id', '=', $class_id)
                        ->limit(1)
                        ->get();

            // $dogs = DB::table('dogs')
            //         ->orderBy('dogs.created_at', 'desc')
            //         ->get();

            $dogs = DB::table('dogs')
                    ->select('dogs.*')
                    ->join('customers', 'customers.id', '=', 'dogs.customer_id')
                    ->where('customers.centre_id', '=', $classes_centres[0]->centre_id)
                    ->orderBy('dogs.created_at', 'desc')
                    ->limit(20)
                    ->get();

            $temp_dogs = DB::table('dogs')
                    ->select('dogs.*')
                    ->join('customers', 'customers.id', '=', 'dogs.customer_id')
                    ->where('customers.centre_id', '=', $classes_centres[0]->centre_id)
                    ->orderBy('dogs.created_at', 'desc')
                    ->limit(20)
                    ->get();
            // echo count($dogs); exit;

            // echo "<pre>";
            // print_r($dogs);
            // exit;

            $customers = Customers::where('centre_id', '=', $classes_centres[0]->centre_id)->get();


            $enrolled_dogs = DB::table('classes_dogs')
                    ->where('class_id', '=', $class_id)
                    ->get();

            $data['title'] = "Enroll Dogs on a Class";
            $data['dogs'] = $dogs;
            $data['temp_dogs'] = $temp_dogs;
            $data['classes'] = $classes;
            $data['customers'] = $customers;
            $data['enrolled_dogs'] = $enrolled_dogs;
            $data['class_id'] = $class_id;
            $data['centre_id'] = $classes_centres[0]->centre_id;

            return view('enroll_dogs', $data);
        }
    }

    public function wassap() {
        echo "<pre>";
        print_r(Session::all());

        // $classes = DB::table('classes')
        //     ->skip(0)
        //     ->take(10)
        //     ->get();

        // print_r($classes);
    }

    public function get_amount_due($class_id = NULL, $dog_id = NULL, $days_enrolled) {
        
        $classes = DB::table('classes')
            ->join('classes_dogs', 'classes_dogs.class_id', '=', 'classes.id')
            ->join('dogs', 'classes_dogs.dog_id', '=', 'dogs.id')
            ->where('classes.id', '=', $class_id)
            ->where('dogs.id', '=', $dog_id)
            ->get();

        $centres = DB::table('centres')
            ->join('classes_centres', 'classes_centres.centre_id', '=', 'centres.id')
            ->where('classes_centres.class_id', '=', $class_id)
            ->get();

        $dog = Dogs::where('id', '=', $dog_id)->get();


        $start = $classes[0]->start_date;
        $end = $classes[0]->end_sched;

        $amount_due = 0;

        if ($days_enrolled == 1) {
            if ($dog[0]->price_type == "custom") {
                $amount_due += $dog[0]->new_custom_price;
            } else if ($dog[0]->price_type == "new" || $dog[0]->price_type == "existing"){
                $amount_due += $centres[0]->price;
            }
        } else if ($days_enrolled > 1) {
            if ($dog[0]->price_type == "custom") {
                $amount_due += $dog[0]->new_custom_price * $days_enrolled;

            } else if ($dog[0]->price_type == "new") {
                /*
                    HYTHE, SEVENOAKS & TONBRIDGE
                    £20/session and £15/session for any extra ones. 
                */ 
            
                $new_count = $days_enrolled;

                $amount_due = 0;
                $week = 0;
                $i = 1;
                

                while($i <= $new_count) {
                    $week++;
                    
                    $sunday = strtotime("this sunday", $start);
                    
                    $days_enrolled = 0;
                    
                    $frequency = $classes[0]->frequency;

                    if ($frequency == "ALL") {
                        $frequency = "monday,tuesday,wednesday,thursday,friday,saturday,sunday,";
                    }

                    $frequency = explode(",", $frequency);
                    $frequency = array_filter($frequency);

                    while(($start <= $sunday) && ($start <= $end)) {
                        $day = strtolower(date('l', $start));
                        $res = array_search($day, $frequency);

                        if (is_numeric($res)) {
                            ++$days_enrolled;
                            ++$i;
                        }
                        
                        $start = strtotime("+1 day", $start);
                    }

                    if ($days_enrolled == 1) {
                        $amount_due += $centres[0]->price;
                    } else {

                        $discounted_price = $centres[0]->discounted_price * $days_enrolled;

                        $amount_due += $centres[0]->price + $discounted_price;

                    }

                }


            } else if ($dog[0]->price_type == "existing") {

                if (($centres[0]->centre_id == 1) || ($centres[0]->centre_id == 2)) {       // Hythe or Sevenoaks
                    /*
                        HYTHE & SEVENOAKS
                        £20/session or £15/session if dog attends more than one.
                    */ 

                    $amount_due += $centres[0]->discounted_price * $days_enrolled;
                } else {
                    
                    $new_count = $days_enrolled;

                    $amount_due = 0;
                    $week = 0;
                    $i = 1;
                    

                    while($i <= $new_count) {
                        $week++;
                        
                        $sunday = strtotime("this sunday", $start);
                        
                        $days_enrolled = 0;
                        
                        $frequency = $classes[0]->frequency;

                        if ($frequency == "ALL") {
                            $frequency = "monday,tuesday,wednesday,thursday,friday,saturday,sunday,";
                        }

                        $frequency = explode(",", $frequency);
                        $frequency = array_filter($frequency);

                        while(($start <= $sunday) && ($start <= $end)) {
                            $day = strtolower(date('l', $start));
                            $res = array_search($day, $frequency);

                            if (is_numeric($res)) {
                                ++$days_enrolled;
                                ++$i;
                            }
                            
                            $start = strtotime("+1 day", $start);
                        }

                        if ($days_enrolled <= 1) {
                            $amount_due += $centres[0]->price;
                        } else {

                            $discounted_price = $centres[0]->discounted_price * $days_enrolled;

                            if ($centres[0]->centre_id == 3) {
                                /*
                                    TON BRIDGE
                                    £20/session and £17.50 for any extra sessions so the same as the new
                                    Sevenoaks & Hythe pricing rule but with a higher discounted price.
                                */

                                $amount_due += $centres[0]->price + $discounted_price;

                            }
                        }

                    }

                }  

            }
        }

        return $amount_due;
       
    }



    public function ajax_enroll_dog(Request $request) {
        
        $post = $request->all();
        $end = strtotime($post['end_date']);
        $spec = strtotime($post['end_sched']);
        
        $start = strtotime($post['start_date']);

        if ( (($spec - $start) >= 0) && (($spec - $end) <= 0) ) {
            
            $specified_end = strtotime($post['end_sched']);


            $db_data = array(
                'class_id' => $post['class_id'],
                'dog_id' => $post['dog_id'],
                'end_sched' => $specified_end
            );

            ClassesDogs::create($db_data);

            $dog = DB::table('dogs')
                    ->select('*')
                    ->where('dogs.id', '=', $post['dog_id'])
                    ->join('customers', 'customers.id', '=', 'dogs.customer_id')
                    ->limit(1)
                    ->get();

            $price_type = $dog[0]->price_type;
            $new_custom_price = $dog[0]->new_custom_price;
            // print_r($new_custom_price);
            // exit;

            $classes = DB::table('classes')
                    ->select('*')
                    ->where('classes.id', '=', $post['class_id'])
                    ->limit(1)
                    ->get();

            $centres = DB::table('centres')
                    ->select('*')
                    ->join('classes_centres', 'classes_centres.centre_id', '=', 'centres.id')
                    ->where('classes_centres.class_id', '=', $post['class_id'])
                    ->limit(1)
                    ->get();


            $start = date('m', $classes[0]->start_date);
            $end = date('m', $specified_end);


            $diff = (int) $end - (int) $start;

            // echo $diff;
            // exit;

            if ($diff == 0) {

                $freq = $classes[0]->frequency;

                if ($freq == "ALL") {
                    $freq = "monday,tuesday,wednesday,thursday,friday,saturday,sunday,";
                }

                $freq = explode(",", $freq);
                $count = 0;

                $start_t = $classes[0]->start_date;
                $end_t = strtotime(date("Y-m-t", $start_t));
                $j = $start_t;

                while($j <= $end_t &&  $j <= $specified_end && $j <= $classes[0]->end_date) {
                    $day = strtolower(date('l', $j));
                    $res = array_search($day, $freq);
                    
                    if (is_numeric($res)) {
                        ++$count;
                    }

                    $j = strtotime("+1 day", $j);

                }

                $cancelled_classes = DB::table('cancelled_classes')
                            ->select('*')
                            ->where('class_id', '=', $post['class_id'])
                            ->get();


                $dogs_days_cancelled = DB::table('dogs_days_cancelled')
                            ->select('*')
                            ->where('class_id', '=', $post['class_id'])
                            ->where('dog_id', '=', $post['dog_id'])
                            ->get();

                            
                $days_enrolled = $count - count($cancelled_classes) - count($dogs_days_cancelled);
                if ($days_enrolled > 1) {  
                    $amount_due = $this->get_amount_due($post['class_id'], $post['dog_id'], $days_enrolled);
                } else {
                    if ($price_type == "custom") {
                        $amount_due = $new_custom_price;
                    } else {
                        $amount_due = $centres[0]->price;
                    }
                }


                $db_data = array(
                    'customer_id' => $dog[0]->customer_id,
                    'class_id' => $post['class_id'],
                    'dog_id' => $post['dog_id'],
                    'days_enrolled' => $days_enrolled,
                    'amount_due' => $amount_due,
                    'amount_rem' => $amount_due,
                    'start' => $classes[0]->start_date,
                    'end' => $specified_end,
                    'amount_paid' => "0.00",
                    'payment_option' => "none",
                    'due_date' => strtotime(date("Y-m-t", $classes[0]->start_date))
                );
        
                CustomersInvoices::create($db_data);
            } else if ($diff > 0) {

                $start_t = $classes[0]->start_date;
                $end_t = strtotime(date("Y-m-t", $start_t));

                for($i=0; $i<=$diff; $i++) {

                    $count = 0;

                    $freq = $classes[0]->frequency;

                    if ($freq == "ALL") {
                        $freq = "monday,tuesday,wednesday,thursday,friday,saturday,sunday,";
                    }

                    $freq = explode(",", $freq);
                    $count = 0;

                    $j = $start_t;


                    while($j <= $end_t &&  $j <= $specified_end && $j <= $classes[0]->end_date) {
                        $day = strtolower(date('l', $j));
                        $res = array_search($day, $freq);
                        
                        if (is_numeric($res)) {
                            ++$count;
                        }

                        $j = strtotime("+1 day", $j);

                    }

                    $num_days = $count;

                    $cancelled_classes = DB::table('cancelled_classes')
                                ->select('*')
                                ->where('class_id', '=', $post['class_id'])
                                ->where('schedule', '>=', $start_t)
                                ->where('schedule', '<=', $end_t)
                                ->get();


                    $dogs_days_cancelled = DB::table('dogs_days_cancelled')
                                ->select('*')
                                ->where('class_id', '=', $post['class_id'])
                                ->where('dog_id', '=', $post['dog_id'])
                                ->where('schedule', '>=', $start_t)
                                ->where('schedule', '<=', $end_t)
                                ->get();

                                
                    $days_enrolled = $num_days - count($cancelled_classes) - count($dogs_days_cancelled);

                    if ($days_enrolled > 1) {
                        $amount_due = $this->get_amount_due($post['class_id'], $post['dog_id'], $days_enrolled);
                    } else {
                        if ($price_type == "custom") {
                            $amount_due = $new_custom_price;
                        } else {
                            $amount_due = $centres[0]->price;
                        }
                    }

                    $db_data = array(
                        'customer_id' => $dog[0]->customer_id,
                        'class_id' => $post['class_id'],
                        'dog_id' => $post['dog_id'],
                        'days_enrolled' => $days_enrolled,
                        'amount_due' => $amount_due,
                        'amount_rem' => $amount_due,
                        'start' => $start_t,
                        'end' => $end_t,
                        'amount_paid' => "0.00",
                        'payment_option' => "none",
                        'due_date' => $end_t
                    );
            
                    CustomersInvoices::create($db_data);

                    $start_t = strtotime("+1 day", $end_t);
                    $end_t = strtotime(date("Y-m-t", $start_t));
                }

            }


            $finance_reports = DB::table('finance_reports')
                ->where('centre_id', '=', $centres[0]->centre_id)
                ->where('year', '=', date('Y'))
                ->where('month', '=', date('m'))
                ->where('rate', '=', $centres[0]->rate)
                ->get();

            if (count($finance_reports) == 0) {
                $db_data = array(
                    'centre_id' => $centres[0]->centre_id,
                    'year' => date('Y'),
                    'month' => date('m'),
                    'rate' => $centres[0]->rate,
                    'revenue' => $amount_due,
                    'commission' => $amount_due * ($centres[0]->rate * 0.01),
                );

                FinanceReports::create($db_data);
            } else {

                $new_revenue = $finance_reports[0]->revenue + $amount_due;
                $new_commission = $new_revenue * ($finance_reports[0]->rate * 0.01);

                $db_data = array(
                    'revenue' => $new_revenue,
                    'commission' => $new_commission
                );

                FinanceReports::where('id', '=', $finance_reports[0]->id)->update($db_data);
            }

            echo "true";

        } else echo "false";
    }

/* --------------- CALENDAR -------------------- */

    public function get_schedule() {
        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        $current_centre = Session::get('current_centre');

        $start_temp = strtotime(date('Y-01-01', $start));
        $end_temp = strtotime(date('Y-m-t', $start));

        $classes = DB::table('classes')
            ->join('classes_centres', 'classes_centres.class_id', '=', 'classes.id')
            ->join('centres', 'centres.id', '=', 'classes_centres.centre_id')
            ->where('classes.start_date', '>=', $start_temp)
            ->orwhere('end_date', '<=', $end)
            ->get();

        $current_year = strtotime(date('Y')."-".date('m')."-01");
        $cancelled_classes = DB::table('cancelled_classes')
            ->where('schedule', '>=', $start)
            ->orwhere('schedule', '<=', $end)
            ->get();

        $cc_days = array();

        if (isset($cancelled_classes)) {

            foreach($cancelled_classes as $row) {
                $cc_days[$row->class_id] = $row->schedule;
            }
        }


        $json_data = array();
        
        foreach($classes as $row) {

            if ($row->start_date > strtotime($_GET['start'])) {
                $start = $row->start_date;
            } else  {
                $start = strtotime($_GET['start']);
            }

            $text_color = "#fff !important";

            if($row->name == "Hythe") {
                $bg_color = "#D54F18";
            } else if($row->name == "Sevenoaks") {
                $bg_color = "#1ABB9C";
            } else if($row->name == "Tonbridge") {
                $bg_color = "#3498DB";
            }

            if ($row->frequency == "ALL") {
                $frequency = "monday,tuesday,wednesday,thursday,friday,saturday,sunday";
            } else {
                $frequency = $row->frequency;
            }

            $frequency = explode(",", $frequency);


            $flag = FALSE;

            if (count($cc_days) > 0) {
                $res = array_search($start, $cc_days);

                if (is_numeric($res) && $res == $row->class_id) {}
                else {
                    $flag = TRUE;
                }
            }

            $centre_flag = FALSE;
            if ($current_centre == 0) {
                $centre_flag = FALSE;
            } else {
                if ($current_centre == $row->centre_id) {
                    $centre_flag = FALSE;
                } else $centre_flag = TRUE;
            }

            while(($start <= $end) && ($start <= $row->end_date)) {

                $start_date = date('Y-m-d', $start);
                $start_time = date('H:i', $row->time_start);
                $start_day = strtolower(date("l", $start));

                $temp = strtotime("+ ".$row->duration." minutes", $row->time_start);
                $end_time = date('H:i', $temp);

                $res = array_search($start_day, $frequency);

                if (is_numeric($res) && !$flag && !$centre_flag) {


                    $array_data = array(
                        'title' => $row->title,
                        'start' => $start_date." ".$start_time,
                        'end' => $start_date." ".$end_time,
                        'description' => $row->description,
                        'editable' => false,
                        'backgroundColor' => $bg_color,
                        'textColor' => $text_color,
                    );

                    $json_data[] = $array_data;
                }

                $start = strtotime("+ 1 day", $start);
            }

            

        }

        echo json_encode($json_data);
        // var_dump(count($json_data));
    }

    public function calendar() {
        if((Session::get('current_usertype') != 1) || (Session::get('view_as'))) {
            $centres = DB::table('centres')->where('id', '=', Session::get('current_centre'))->get();
            $classes = DB::table('classes')
                    ->select('*')
                    ->join('classes_centres', 'classes_centres.class_id', '=', 'classes.id')
                    ->join('centres', 'centres.id', '=', 'classes_centres.centre_id')
                    ->where('start_date', '>', 0)
                    ->where('end_date', '>', 0)
                    ->where('centres.id', '=', Session::get('current_centre'))
                    ->get();
        } else {
            $centres = Centres::all();
            $classes = DB::table('classes')
                    ->select('*')
                    ->join('classes_centres', 'classes_centres.class_id', '=', 'classes.id')
                    ->join('centres', 'centres.id', '=', 'classes_centres.centre_id')
                    ->where('start_date', '>', 0)
                    ->where('end_date', '>', 0)
                    ->get();
        }
        
        $cancelled_classes = CancelledClasses::all();
        $absent_dogs = DogsDaysAbsent::all();

        $customers_invoices = DB::table('customers_invoices')
                ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                ->where('customers_invoices.status', '=', 'new')
                ->get();

        $invoices = array();
        foreach ($customers_invoices as $row) {
            $invoices[$row->class_id] = $row->class_id;
        }
        // CustomersInvoices::where('status', '=', 'new')->get();

        // echo "<pre>";
        // print_r($invoices);
        // exit;

        $data['invoices'] = $invoices;

        $data['title'] = "Schedule of Classes";
        $data['classes'] = $classes;
        $data['cancelled_classes'] = $cancelled_classes;
        $data['absent_dogs'] = $absent_dogs;
        $data['centres'] = $centres;
        $data['message'] = Session::get('message');
        $data['is_delete'] = Session::get('is_delete');
        $data['error_delete_modal'] = Session::get('error_delete_modal');

        return view('calendar', $data);    
    }

/* --------------- SETTINGS -------------------- */
    public function settings(Request $request){
        $post = $request->all();
        if(Session::get('current_usertype') == '1')
            $centres = DB::table('centres')->limit(3)->get();
        else
            $centres = DB::table('centres')->limit(1)->where('id', '=', Session::get('current_centre'))->get();

        if (empty($post)) {
            $settings = Settings::find(1);

            $data['title'] = "Settings";
            $data['message'] = Session::get('message');
            $data['settings'] = $settings;
            $data['centres'] = $centres;
            return view('settings', $data);
        } else {

            if ($request->hasFile('image'))
            {
                if($request->file('image')->getClientSize() >  300000){
                    $message = "Error in downloading the image, file size is too large. Please upload an image that is at most 640px.";
                    Session::flash('message', $message);

                    return Redirect::back();
                } else {
                    $imageName = 'bblogo';
                    $destinationPath = public_path().'/images/users/';
                    $extension = $request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($destinationPath, $imageName.'.'.$extension);

                    $db_data = array(
                        'app_name' => $post['app_name'],
                        'app_logo' => $imageName.'.'.$extension
                    );

                    $affectedRows = Settings::where('id', '=', '1')->update($db_data);
                }
            } else {
                $db_data = array(
                    'app_name' => $post['app_name']
                );
                $affectedRows = Settings::where('id', '=', '1')->update($db_data);
            }

            $message = "You have successfully updated the Settings.";

            Session::flash('message', $message);
            return Redirect::to('/settings');
        }
    }

    public function settings_remove_image($id = NULL){
        $settings = Settings::find($id);

        if (!empty($settings['app_logo'])) {

            $image_file = realpath('images/users/'.$settings['app_logo']);

            if (file_exists($image_file)) {
                if (is_writable($image_file)) {
                    unlink($image_file);
                }
            }
        } else echo "errer";

        $db_data = array(
            'app_logo' => ""
        );
        $affectedRows = Settings::where('id', '=', '1')->update($db_data);

        return Redirect::back();
    }

    public function settings_edit_centre($id = NULL, Request $request){
        $post = $request->all();
        $centres = Centres::find($id);

        if (empty($post)) {

            $data['title'] = "Edit Centre Information";
            $data['message'] = Session::get('message');
            $data['centres'] = $centres;

            return view('settings_edit_centre', $data);
        } else {
            
            $db_data = array(
                'address' => $post['address'],
                'email' => $post['email'],
                'phone' => $post['phone'],
                'mobile' => $post['mobile'],
                'fax' => $post['fax'],
                'price' => $post['price'],
                'discounted_price' => $post['discounted_price'],
                'custom_price' => $post['custom_price'],
                'rate' => $post['rate']
            );
            
            $affectedRows = Centres::where('id', '=', $id)->update($db_data);

            $message = "You have successfully updated the information for ".$post['cname']." Centre.";

            Session::flash('message', $message);
            return Redirect::to('/settings/edit-centre/'.$id);
        }        
    }

    public function settings_edit_email($id = NULL, Request $request){
        $post = $request->all();
        $centres = Centres::find($id);

        if (empty($post)) {
            $data['title'] = "Edit Email Footnote";
            $data['message'] = Session::get('message');
            $data['centres'] = $centres;

            return view('settings_edit_email', $data);
        } else {
            
            $db_data = array(
                'email_footnote' => $post['descr']
            );
            
            $affectedRows = Centres::where('id', '=', $id)->update($db_data);

            $message = "You have successfully updated the information for e-mail.";

            Session::flash('message', $message);
            return Redirect::to('/settings/edit-email/'.$id);
        }   
    }

/* ----------------- USERS --------------------- */
    public function view_users(){

        $current_centre = Session::get('current_centre');

        if ($current_centre == 0) {
            $users = DB::table('users')
                ->orderBy('firstname', 'asc')
                ->get();
        } else {
            $users = DB::table('users')
                ->orderBy('firstname', 'asc')
                ->where('centre_id', '=', $current_centre)
                ->get();
        }


        $centres = DB::table('centres')->select('id', 'name')->get();

        $data['title'] = "Users";
        $data['message'] = Session::get('message');
        $data['users'] = $users;
        $data['centres'] = $centres;
        $data['current_centre'] = $current_centre;

        return view('view_users', $data);
    }

    public function add_users(Request $request){
        $centres = Centres::all();
        $post = $request->all();
        $last_id = DB::table('users')->select('id')->orderBy('id', 'desc')->limit(1)->get();
        $new_image_id = $last_id == NULL ? "1" : ($last_id[0]->id + 1);

        if (!empty($post)) {
            $check_username = DB::table('users')->where('username', '=', $post['username'])->limit(1)->count();
            if($check_username > 0) {
                $message = "Username is already been taken.";

                Session::flash('message', $message);
                return Redirect::to('/users/add');
            } else {
                if ($request->hasFile('image'))
                {
                    $imageName = $request->firstname.'-'.$new_image_id;
                    $destinationPath = public_path().'/images/users/';
                    $extension = $request->file('image')->getClientOriginalExtension();
                    $request->file('image')->move($destinationPath, $imageName.'.'.$extension);

                    $db_data = array(
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'address' => $post['address'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'mobile' => $post['mobile'],
                    'usertype' => $post['usertype'],
                    'centre_id' => ($post['usertype']=="1" ? "0" : $post['centre']),
                    'username' => $post['username'],
                    'password' => $post['password'],
                    'image' => $imageName.'.'.$extension
                    );
                    DB::table('users')->insert($db_data);

                } else {            
                    $db_data = array(
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'address' => $post['address'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'mobile' => $post['mobile'],
                    'usertype' => $post['usertype'],
                    'centre_id' => ($post['usertype']=="1" ? "0" : $post['centre']),
                    'username' => $post['username'],
                    'password' => $post['password']
                    );
                    DB::table('users')->insert($db_data);

                }

                $message = "You have successfully added a new user.";

                Session::flash('message', $message);
                return Redirect::to('/users');
            }
        }

        $data['title'] = "Add New User";
        $data['message'] = Session::get('message');
        $data['centres'] = $centres;

        return view('add_user', $data);
    }

    public function edit_users($id= NULL, Request $request){
        $centres = Centres::all();
        $post = $request->all();

        if (empty($post)) {
            $user = User::find($id);
            $edit = TRUE;

            $data['title'] = "Edit User Information";
            $data['message'] = Session::get('message');
            $data['user'] = $user;
            $data['centres'] = $centres;
            $data['edit'] = $edit;
            
            return view('profile', $data);
        } else {
            if ($request->hasFile('image'))
            {
                $imageName = $request->firstname.'-'.$id;
                $destinationPath = public_path().'/images/users/';
                $extension = $request->file('image')->getClientOriginalExtension();
                $request->file('image')->move($destinationPath, $imageName.'.'.$extension);

                $db_data = array(
                    'username' => $post['username'],
                    'password' => (!empty($post['password']) ? $post['password'] : $post['cpassword']),
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'address' => $post['address'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'mobile' => $post['mobile'],
                    'usertype' => $post['usertype'],
                    'centre_id' => ($post['usertype']=="1" ? "0" : $post['centre']),
                    'image' => $imageName.'.'.$extension
                );
                $affectedRows = User::where('id', '=', $id)->update($db_data);
            } else {
                $db_data = array(
                    'username' => $post['username'],
                    'password' => (!empty($post['password']) ? $post['password'] : $post['cpassword']),
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'address' => $post['address'],
                    'email' => $post['email'],
                    'phone' => $post['phone'],
                    'mobile' => $post['mobile'],'usertype' => $post['usertype'],
                    'centre_id' => ($post['usertype']=="1" ? "0" : $post['centre'])
                );
                $affectedRows = User::where('id', '=', $id)->update($db_data);
            }
            $message = "You have successfully updated the profile of ".$post['firstname']." ".$post['lastname'];

            Session::flash('message', $message);
            return Redirect::to('/users/edit/'.$id);
        }
    }

    public function delete_users($id = NULL) {
        $user = User::find($id);

        if (isset($id)) {
            User::where('id', '=', $id)->delete();

            $message = "You have successfully deleted ".$user['firstname']." ".$user['lastname']." in the list";
            Session::flash('message', $message);
        }
        return Redirect::to('/users');
    }
}
