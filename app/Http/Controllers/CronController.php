<?php
namespace App\Http\Controllers;

use DB;
use Mail;

class CronController extends Controller {

    public function autoSendEmail(){

        $customers_invoices = DB::table('customers_invoices')
                    ->where('customers_invoices.sent_date', '=', '0')
                    ->where('customers_invoices.start', '<', strtotime("first day of this month"))
                    ->join('classes', 'classes.id', '=', 'customers_invoices.class_id')
                    ->join('customers', 'customers.id', '=', 'customers_invoices.customer_id')
                    ->join('centres', 'centres.id', '=', 'customers.centre_id' )
                    ->join('dogs', 'dogs.id', '=', 'customers_invoices.dog_id')
                    ->orderBy('customers_invoices.id', 'asc')
                    ->select('*',
                        'customers_invoices.id as id',
                        'customers.phone as phone',
                        'customers.mobile as mobile',
                        'customers.email as email',
                        'customers.fax as fax',
                        'centres.name as centre_name',
                        'centres.phone as centre_phone',
                        'centres.mobile as centre_mobile',
                        'centres.email as centre_email',
                        'centres.fax as centre_fax')
                    ->get();

        $invoice = DB::table('customers_invoices')
                    ->where('customers_invoices.sent_date', '=', '0')
                    ->where('customers_invoices.start', '<', strtotime("first day of this month"))
                    ->orderBy('customers_invoices.id', 'asc')
                    ->get();

        $count = count($invoice);

        $app_name = DB::table('settings')
                    ->where('id', '=', '1')
                    ->pluck('app_name');

        $days_cancelled = array();
        $days_abs = DB::table('dogs_days_absent')
                    ->get();

        $days_absent = array();
        foreach($invoice as $key) {
            $days_cancelled[$key->id] = DB::table('dogs_days_cancelled')
                    ->where('class_id', '=', $key->class_id)
                    ->where('dog_id', '=', $key->dog_id)
                    ->get();
        }

        if (isset($days_abs)) {
            foreach($invoice as $row) {
                foreach($days_abs as $d) {
                    if( ($d->class_id == $row->class_id)  && ($d->dog_id == $row->dog_id) ) {
                        $days_absent[$row->id] = $d->schedule;
                    }
                }
            }
        }

        for($i=0; $i<$count; $i++){
            $data[$i]['invoice_date'] = $invoice[$i]->created_at;
            $data[$i]['invoice_id'] = $invoice[$i]->id;
            $data[$i]['days_cancelled'] = $days_cancelled[$invoice[$i]->id];
            $data[$i]['days_absent'] = $days_absent;
            $data[$i]['customers_invoices'] = $customers_invoices[$i];
            $data[$i]['centres'] = DB::table('centres')->get();
            $data[$i]['app_name'] = $app_name;
        }

        for($i=0; $i<$count; $i++){
            $temp = [$i, $customers_invoices, $data[$i]['invoice_date'], $data[$i]['invoice_id']];
            Mail::send('email', $data[$i], function ($message) use ($temp) {
                $message->to($temp[1][$temp[0]]->email, $temp[1][$temp[0]]->firstname.' '.$temp[1][$temp[0]]->lastname);
                $message->subject('Best Behaviour Invoice for the Month of '.date('F',$temp[2]).' (Invoice #: '.sprintf( '%07d', $temp[3]).')');
            });

            //Update sent_date value
            $db_data = array( 'sent_date' => time() );
            DB::table('customers_invoices')
                    ->where('id', $invoice[$i]->id)
                    ->update($db_data);
        }
    }

}