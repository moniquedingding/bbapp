<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Centres;
use App\Staff;
use App\Customers;
use App\Classes;
use App\ClassesCentres;
use App\ClassesDogs;
use App\User;
use App\Dogs;
use DB;
use View;

class LoginController extends Controller {

    public function __construct() {
		// $this->middleware('auth');
        $settings = DB::table('settings')
                    ->where('id', '=', '1')
                    ->pluck('app_name');
        $for_all_view['app_name'] = $settings;
        View::share('for_all_view', $for_all_view);
	}

    public function index() {
        return redirect('/');
    }

    public function wassap() {
        echo "<pre>";
        print_r(Session::all());
        exit;
    }
    
    public function login(Request $request) {
        if (Session::has('login')) {
            $data['title'] = "Home";
            return redirect('/');
        } else {
            $data['message'] = Session::get('message');
            return view('login', $data);
        }
    }

    public function loginCheck(Request $request){
        $login = TRUE;
        $is_valid = TRUE;
        $user = new User;

        do {

            $password = password_hash($request->password, PASSWORD_BCRYPT);
            $account = $user->doesUserExist($request->username);            

            if (empty($account))  {
                $is_valid = FALSE;
            } else {

                 if (password_verify($account[0]->password, $password)) {
                    
                    $initials = substr($account[0]->firstname, 0, 1)." ".substr(ucwords($account[0]->lastname), 0, 1);

                    Session::put('username', $account[0]->username);
                    Session::put('user_id', $account[0]->id);
                    Session::put('login', $login);
                    Session::put('current_usertype', $account[0]->usertype);
                    Session::put('current_centre',$account[0]->centre_id);
                    Session::put('initials', ucwords($initials));
                    Session::put('view_as', False);

                    return redirect('/');

                 } else $is_valid = FALSE;
            
            }

        } while($is_valid);

        $message = "Invalid username or password, please try again.";        
        Session::flash('message', $message);
        return redirect('login');

    }


    public function logout() {
        Session::flush();
        return redirect('login');
    }

}
