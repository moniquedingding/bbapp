<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersInvoices extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'customers_invoices';
    public $timestamps = TRUE;
    protected $fillable = array('invoice_id', 'customer_id', 'paid_date', 'class_id', 'dog_id', 'due_date', 'days_enrolled', 'amount_due', 'start', 'end', 'amount_paid', 'amount_rem', 'payment_method');
}