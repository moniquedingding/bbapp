<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelledClasses extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    public $timestamps = TRUE;
    protected $table = 'cancelled_classes';
    protected $fillable = array('class_id', 'schedule', 'updated_at', 'created_at');
}