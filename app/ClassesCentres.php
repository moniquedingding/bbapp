<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassesCentres extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'classes_centres';
    public $timestamps = TRUE;
    protected $fillable = array('class_id', 'centre_id');
}