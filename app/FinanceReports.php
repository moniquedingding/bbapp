<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinanceReports extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    public $timestamps = TRUE;
    protected $table = 'finance_reports';
    protected $fillable = array('centre_id', 'start_date', 'end_date', 'revenue', 'rate', 'commission', 'year', 'month', 'updated_at', 'created_at');
}