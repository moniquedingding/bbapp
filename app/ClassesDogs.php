<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassesDogs extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'classes_dogs';
    public $timestamps = TRUE;
    protected $fillable = array('class_id', 'dog_id', 'end_sched');
}