<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'customers';
    public $timestamps = TRUE;
    protected $fillable = array('firstname', 'is_patron', 'middlename', 'lastname', 'address_1', 'address_2', 'city', 'state', 'country', 'postcode', 'email', 'phone', 'mobile', 'fax', 'occupation', 'notes', 'centre_id', 'staff_id');
}