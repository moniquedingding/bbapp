<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $table = 'settings';
    public $timestamps = FALSE;
    protected $fillable = ['*'];
}