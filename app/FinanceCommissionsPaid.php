<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinanceCommissionsPaid extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'finance_commissions_paid';
    public $timestamps = TRUE;
    protected $fillable = array('centre_id', 'paid_by', 'rate', 'amount_requested', 'date_requested', 'approved', 'date_approved', 'approved_by', 'method');
}