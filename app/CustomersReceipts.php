<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersReceipts extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'customers_receipts';
    public $timestamps = TRUE;
    protected $fillable = array('customers_invoices_id', 'amount_paid', 'description', 'payment_method', 'has_credit', 'date_paid');
}