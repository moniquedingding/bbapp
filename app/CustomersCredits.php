<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersCredits extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'customers_credits';
    public $timestamps = TRUE;
    protected $fillable = array('customer_id', 'customers_invoices_id', 'amount');
}