<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    protected $table = 'classes';
    public $timestamps = TRUE;
    protected $fillable = array('title', 'description', 'duration', 'frequency', 'start_date', 'end_date', 'num_days', 'is_repeating', 'time_start');
}