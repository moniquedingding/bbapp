<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DogsDaysAbsent extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';
    public $timestamps = TRUE;
    protected $table = 'dogs_days_absent';
    protected $fillable = array('dog_id', 'class_id', 'schedule', 'updated_at', 'created_at');
}