-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2015 at 09:17 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bbapp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cancelled_classes`
--

CREATE TABLE IF NOT EXISTS `cancelled_classes` (
`id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `schedule` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancelled_classes`
--

INSERT INTO `cancelled_classes` (`id`, `class_id`, `schedule`, `updated_at`, `created_at`) VALUES
(5, 33, 1440090000, 1439962888, 1439962888),
(6, 30, 1440574200, 1440053849, 1440053849),
(7, 28, 1440504000, 1440061140, 1440061140),
(8, 30, 1440487800, 1440061801, 1440061801),
(9, 30, 1440660600, 1440064434, 1440064434),
(10, 28, 1440417600, 1440064475, 1440064475),
(11, 21, 1443697200, 1440348822, 1440348822),
(12, 21, 1443697200, 1440348903, 1440348903);

-- --------------------------------------------------------

--
-- Table structure for table `centres`
--

CREATE TABLE IF NOT EXISTS `centres` (
`id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `mobile` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `fax` varchar(25) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centres`
--

INSERT INTO `centres` (`id`, `name`, `phone`, `mobile`, `email`, `fax`, `updated_at`, `created`) VALUES
(1, 'Hythe', '01732764081', '07779 100204', 'pippa@bestbehaviourschoolfordo', 'pippa@bestbehaviourschool', 0, 0),
(2, 'Sevenoaks', '01732764081', '07779100204', 'pippa@bestbehaviourschoolfordo', 'pippa@bestbehaviourschool', 0, 0),
(3, 'Tonbridge', '01732764081', '07779100204', 'pippa@bestbehaviourschoolfordo', 'pippa@bestbehaviourschool', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
`id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `duration` varchar(25) NOT NULL,
  `is_repeating` varchar(30) NOT NULL,
  `frequency` varchar(255) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `time_start` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `title`, `description`, `duration`, `is_repeating`, `frequency`, `start_date`, `end_date`, `time_start`, `updated_at`, `created_at`) VALUES
(13, 'Exercise Dogs', 'There is no need to clean strings being passed as bindings.', '120', 'forever', 'wednesday,thursday,friday,', 1439942400, 1451520000, 1439996400, 1439971084, 1439194253),
(14, 'Behaviour Counselling', 'The Laravel query builder uses PDO parameter binding throughout to protect your application against SQL injection attacks.', '30', 'certain_date', 'wednesday,thursday,friday,', 1443657600, 1444608000, 1439985600, 1439970636, 1439195176),
(17, 'Running And Jogging', 'The Laravel query builder uses PDO parameter binding throughout to protect your application against SQL injection attacks.', '90', 'certain_date', 'monday,tuesday,wednesday,thursday,friday,', 1441152000, 1441929600, 1439989200, 1439970194, 1439263455),
(21, 'Singing Class', 'The checked attribute value does not change with the state of the checkbox', '60', 'forever', 'wednesday,thursday,friday,', 1443657600, 1451554019, 1439982000, 1439970498, 1439531349),
(22, 'Controlled Eating Class', 'The Unix epoch (or Unix time or POSIX time or Unix timestamp) is the number of seconds that have elapsed since January 1, 1970', '30', 'certain_date', 'monday,tuesday,', 1440374400, 1441324800, 1439991000, 1439971241, 1439532432),
(25, 'Graduation', 'Convert epoch to human readable date and vice versa', '60', 'forever', 'monday,wednesday,friday,', 1442102400, 1451554019, 1440003600, 1439970691, 1439780773),
(26, 'Doggy Dance', 'Shank fatback corned beef biltong prosciutto. Alcatra tri-tip shankle', '120', 'forever', 'ALL', 1440028800, 1440430200, 1440084600, 1440044402, 1440044402),
(28, 'Some Event', '$(''.delete-class'').attr(''data-time''$(''.delete-class'').attr(''data-time''', '90', 'certain_date', 'ALL', 1440111600, 1440716400, 1440072000, 1440051982, 1440051982),
(30, 'Browsing Privately', 'Firefox won''t remember any history for this window.', '120', 'certain_date', 'ALL', 1440115200, 1440720000, 1440055800, 1440053788, 1440053788),
(31, 'Some Event', '$timestamp = NULL', '120', 'certain_date', 'ALL', 1440547200, 1441324800, 1440316800, 1440355872, 1440355872);

-- --------------------------------------------------------

--
-- Table structure for table `classes_centres`
--

CREATE TABLE IF NOT EXISTS `classes_centres` (
`id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `centre_id` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes_centres`
--

INSERT INTO `classes_centres` (`id`, `class_id`, `centre_id`, `updated_at`, `created_at`) VALUES
(1, 11, 2, 1439193984, 1439193984),
(4, 12, 3, 1439194211, 1439194211),
(7, 14, 2, 1439195176, 1439195176),
(8, 14, 3, 1439195176, 1439195176),
(11, 17, 3, 1439263455, 1439263455),
(14, 19, 2, 1439531265, 1439531265),
(15, 20, 2, 1439531319, 1439531319),
(16, 21, 2, 1439531349, 1439531349),
(17, 22, 2, 1439532432, 1439532432),
(20, 25, 2, 1439780773, 1439780773),
(27, 32, 1, 1439868221, 1439868221),
(28, 26, 3, 1440044402, 1440044402),
(30, 28, 1, 1440051982, 1440051982),
(32, 30, 2, 1440053788, 1440053788),
(33, 31, 2, 1440355872, 1440355872);

-- --------------------------------------------------------

--
-- Table structure for table `classes_dogs`
--

CREATE TABLE IF NOT EXISTS `classes_dogs` (
`id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `dog_id` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes_dogs`
--

INSERT INTO `classes_dogs` (`id`, `class_id`, `dog_id`, `updated_at`, `created_at`) VALUES
(12, 14, 2, 1439198759, 1439198759),
(13, 17, 4, 1439264219, 1439264219),
(14, 0, 0, 1439273078, 1439273078),
(15, 21, 3, 1439531708, 1439531708),
(16, 21, 1, 1439531727, 1439531727),
(17, 21, 2, 1439532242, 1439532242),
(20, 25, 1, 1439780782, 1439780782),
(21, 25, 3, 1439780784, 1439780784),
(31, 22, 17, 1439806765, 1439806765),
(33, 30, 9, 1440053792, 1440053792),
(34, 31, 13, 1440355877, 1440355877),
(35, 31, 14, 1440355878, 1440355878),
(36, 31, 15, 1440355895, 1440355895),
(39, 14, 8, 1440356282, 1440356282);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
`id` int(11) NOT NULL,
  `centre_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `middlename` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(25) NOT NULL,
  `postcode` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `mobile` varchar(25) NOT NULL,
  `fax` varchar(25) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `centre_id`, `staff_id`, `firstname`, `middlename`, `lastname`, `address_1`, `address_2`, `city`, `state`, `country`, `postcode`, `email`, `phone`, `mobile`, `fax`, `occupation`, `notes`, `updated_at`, `created_at`) VALUES
(14, 1, 3, 'Maya', '', 'Hudson', '47 Layburn Court', '', 'Trecastle', 'Some State', 'United Kingdom', 'LD3 8WY ', 'MayaHodgson@dayrep.com', '5947300286', '070 8819 7062', '2641012398', '', '', 1439283660, 1432112745),
(15, 1, 3, 'Charlie ', '', 'Foster', '28 Great North Road', '', 'Alstonefield', '', 'United Kingdom', 'DE6 1QT', 'CharlieFoster@armyspy.com ', '', '258957442985', '', '', '', 1432190745, 1432190745),
(16, 1, 1, 'Sebastian ', '', 'Anderson', '41 South Street', '', 'Monrose', '', 'United Kingdom', 'DD10 7FY ', 'SebastianAnderson@teleworm.us', '', '     079 4504 4148', '', '', 'His wife will pick up the dogs after classes.', 1432122745, 1432122745),
(17, 1, 3, 'Samantha ', '', 'Moss', '19 South Western Terrace', '', 'Minnigaff', '', 'United Kingdom', 'DG8 1TS ', 'SamanthaMoss@teleworm.us ', '', '070 7046 7080', '', '', '', 1439122745, 1439122745),
(19, 3, 3, 'Oliver', '', 'Sykes', '73 Sutton Wick Lane', '', 'Bridge Trafford', '', 'United Kingdom', 'CH2 2GY', 'olisykes@bringme.tk', '5947300286', '07972143861', '', 'Singer', '', 1439177768, 1439177768),
(20, 2, 2, 'Stefan', 'Middle', 'Janoski', 'Keplerstraat 157', '', 'Apeldoorn ', 'JZ  ', 'Netherlands', '7316 ', 'sj7645@gmail.om', '', '0946952444', '', '', '', 1439806595, 1439806595);

-- --------------------------------------------------------

--
-- Table structure for table `dogs`
--

CREATE TABLE IF NOT EXISTS `dogs` (
`id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `breed` varchar(100) NOT NULL,
  `color` varchar(30) NOT NULL,
  `name` varchar(25) NOT NULL,
  `age` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dogs`
--

INSERT INTO `dogs` (`id`, `customer_id`, `breed`, `color`, `name`, `age`, `image`, `updated_at`, `created_at`) VALUES
(1, 14, 'half dachshund, half pomeranian', 'brown and black', 'Barfy', '5 years', 'barfy.jpg', 1439283964, 0),
(2, 16, 'yorkshire terrier', 'brown and black', 'Slater', '7 months', 'slater.jpg', 0, 0),
(3, 14, 'golden retriever', 'white', 'Rayne', '9 months', 'rayne.jpg', 1439284027, 0),
(4, 15, 'shih tzu', 'black', 'Amiel', '4 months', 'amiel.jpg', 0, 0),
(8, 14, 'Siberian Husky', 'gray and white', 'Fuffie', '2 years and 6 months', 'fuffie.jpg', 1439277145, 1439277145),
(9, 14, 'Pekingese', 'White', 'Munchy', '10 months', 'munchy.jpg', 1439285816, 1439285816),
(13, 14, 'Shiba Inu', 'orange and white', 'Doge', '10 years', 'Doge-081415061128.jpeg', 1439532688, 1439532688),
(14, 17, 'Corgi', 'light brown and white', 'Bubbles', '1 year', 'Bubbles-081415061618.jpg', 1439532979, 1439532979),
(15, 19, 'Mastiff', 'black and brown', 'Mashed Potty', '6 years', 'Doge-081415061943.jpg', 1439533183, 1439533183),
(16, 17, 'Shih Poo', 'White', 'Shishi', '4 months', 'Shishi-081715073802.jpg', 1439797082, 1439797082),
(17, 20, 'Siberian Husky and German Shepherd ', 'black, brown and white', 'Glacier', '5 years 3 months', 'Glacier-13958.jpg', 1439806735, 1439806735);

-- --------------------------------------------------------

--
-- Table structure for table `dogs_days_cancelled`
--

CREATE TABLE IF NOT EXISTS `dogs_days_cancelled` (
`id` int(11) NOT NULL,
  `dog_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `schedule` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dogs_days_cancelled`
--

INSERT INTO `dogs_days_cancelled` (`id`, `dog_id`, `class_id`, `schedule`, `updated_at`, `created_at`) VALUES
(2, 1, 21, 1443697200, 1440349001, 1440349001),
(3, 1, 21, 1443783600, 1440349427, 1440349427),
(4, 1, 21, 1451559600, 1440353869, 1440353869),
(5, 17, 22, 1441027800, 1440355798, 1440355798);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
`id` int(11) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `mobile` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `firstname`, `lastname`, `email`, `mobile`) VALUES
(1, 'Ellis', 'Midd', 'e.middleton@gmail.com', '07854998649'),
(2, 'Jethro', 'Parsons', 'japarsons91@yahoo.com', '07854934649'),
(3, 'Rowan', 'Ornsler', 'rowanjornsler@bbschool.com', '07854508649');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `usertype` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `usertype`) VALUES
(1, 'admin', 'admin', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cancelled_classes`
--
ALTER TABLE `cancelled_classes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `centres`
--
ALTER TABLE `centres`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes_centres`
--
ALTER TABLE `classes_centres`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes_dogs`
--
ALTER TABLE `classes_dogs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dogs`
--
ALTER TABLE `dogs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dogs_days_cancelled`
--
ALTER TABLE `dogs_days_cancelled`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cancelled_classes`
--
ALTER TABLE `cancelled_classes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `centres`
--
ALTER TABLE `centres`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `classes_centres`
--
ALTER TABLE `classes_centres`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `classes_dogs`
--
ALTER TABLE `classes_dogs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `dogs`
--
ALTER TABLE `dogs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `dogs_days_cancelled`
--
ALTER TABLE `dogs_days_cancelled`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
